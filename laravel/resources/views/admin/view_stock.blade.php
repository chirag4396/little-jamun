@extends('admin.layouts.master')

@section('title')
Products
@endsection
@push('header')
<!-- Datatables -->
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = 'product';
@endphp
<script>
  var ID = '{{ $ID }}';
</script>
@endpush

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Products</h3>
      </div>
      {{-- <div class="title_right">
        <button class="btn btn-primary pull-right" onclick="CRUD.add('{{ $ID }}','{{ route('admin.'.$ID.'.store') }}');">Add</button>
      </div> --}}
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">            
            <table id="{{ $ID }}Table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Quantity</th>
                  <th>Sold</th>
                  <th>Remaining</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($products as $k => $pro)
                @php
                  $stock = (isset($pro->stock[0]->total)) ? $pro->stock[0]->total : 0;
                @endphp
                <tr id = "tr-{{ $pro->pro_id }}">
                  <td>{{ ++$k }}</td>
                  <td><img src="{{ asset(isset($pro->display) ? $pro->display->img_thumb_path : '') }}" class="pro-img"></td>
                  <td>{{ str_limit($pro->pro_title, 30) }}</td>
                  <td>{{ $pro->pro_qty }}</td>
                  <td>{{ (isset($pro->stock[0]->total)) ? $pro->stock[0]->total : 0 }}</td>
                  <td>{{ $pro->pro_qty - $stock }}</td>
                  <td>                    
                    <button title = "Add more quantity to {{ $pro->pro_title }}" class="btn btn-info btn-xs" onclick="CRUD.edit({getUrl:'{{ route('admin.'.$ID.'.index') }}',fetchId : '{{ $pro->pro_id }}',id:'{{ $ID }}'});"><i class="fa fa-pencil"></i> Add More</button>
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan = "7" class="text-center">No {{ $ID }} Found <a class = "btn btn-primary" href="{{ route('admin.'.$ID.'.create') }}">Add some now</a></td>
                </tr>
                @endforelse
              </tbody>
{{--               <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Quantity</th>
                  <th>Sold</th>
                  <th>Remaining</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($products as $k => $pro)
                @php
                  $stock = (isset($pro->stock[0]->total)) ? $pro->stock[0]->total : 0;
                @endphp
                <tr id = "tr-{{ $pro->pro_id }}">
                  <td>{{ ++$k }}</td>
                  <td><img src="{{ asset($pro->display->img_thumb_path) }}" class="pro-img"></td>
                  <td>{{ str_limit($pro->pro_title, 30) }}</td>
                  <td>{{ $pro->pro_qty }}</td>
                  <td>{{ (isset($pro->stock[0]->total)) ? $pro->stock[0]->total : 0 }}</td>
                  <td>{{ $pro->pro_qty - $stock }}</td>
                  <td>                    
                    <button title = "Add more quantity to {{ $pro->pro_title }}" class="btn btn-info btn-xs" onclick="CRUD.edit({getUrl:'{{ route('admin.'.$ID.'.index') }}',fetchId : '{{ $pro->pro_id }}',id:'{{ $ID }}'});"><i class="fa fa-pencil"></i> Add More</button>
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan = "7" class="text-center">No {{ $ID }} Found <a class = "btn btn-primary" href="{{ route('admin.'.$ID.'.create') }}">Add some now</a></td>
                </tr>
                @endforelse
              </tbody> --}}
            </table>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="{{ $ID }}DModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Are You Sure?</h4>
            <br>
            <div>
              <form id = "{{ $ID }}DForm">                
                <button type = "button" class="btn btn-danger" id = "yes">Yes</button>
                <button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
              </form>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="{{ $ID }}Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="{{ $ID }}ModalLabel">Edit {{ $ID }}</h4>
      </div>
      <div class="modal-body">
        <form id = "{{ $ID }}Form" class="form-horizontal form-label-left">
          <input type="hidden" name="sid">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Available Quantity
            </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" class="form-control col-md-7 col-xs-12" name = "qty" readonly>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Add new Quantity
            </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" class="form-control col-md-7 col-xs-12" name = "new_quantity">
            </div>
          </div>
          <div class="ln_solid">
          </div>
          <div class="form-group text-center">              
            <button type="submit" class="btn btn-success">Update
            </button>             
          </div>          
        </form>            
      </div>          
    </div>
  </div>
</div>
@endsection

@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script> --}}
{{-- <script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script> --}}
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="text/javascript">  
  var update = "{{ url('admin/'.$ID) }}",
  deleteU = "{{ url('admin/'.$ID) }}",
  store = "{{ route('admin.'.$ID.'.store') }}";

  Table.init(ID);
</script>
@endpush