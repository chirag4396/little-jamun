@extends('admin.layouts.master')
@section('title')
Update Offer
@endsection

@php
$ID = 'offer';
$oID = 'occasion';
@endphp
@push('header')
<link href="{{ asset('admin-assets/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

<script>
	ID = '{{ $ID }}';
	oID = '{{ $oID }}';
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Upadte {{ $offer->offer_title }} Offer</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left">
						<input type="hidden" name="sid" value = "{{ $offer->offer_id }}">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Occasion</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<select name = "occasion" class="form-control" id = "occasion">
									<option value="-1-">--select--</option>
									@forelse (App\Models\Occasion::get() as $occ)
									<option value="{{ $occ->occ_id }}" {{ $offer->offer_occasion == $occ->occ_id ? 'selected' : ''}}>{{ $occ->occ_title }}</option>
									@empty
									@endforelse
									<option value="other" id = "occOther" onclick="addNew();">Other</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Title
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="text" value = "{{ $offer->offer_title }}" class="form-control col-md-7 col-xs-12" name = "title">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>
							<div class="col-md-6 col-sm-6 col-xs-12 margin-top">
								<input type="radio" value = "101" class="type" name = "type" {{ $offer->offer_type == 101 ? 'checked' : '' }}> Single
								<input type="radio" value = "102" class="type" name = "type" {{ $offer->offer_type == 102 ? 'checked' : '' }}> Combo	
								<input type="radio" value = "103" class="type" name = "type" {{ $offer->offer_type == 103 ? 'checked' : '' }}> Buy and Get Free	

							</div>
						</div>
						<div id = "buy" {!! $offer->offer_type == 103 ? '' : 'class="hidden"' !!}>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Product</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control col-md-7 col-xs-12 product" id = "buyPro" name="buy_product">
										<option value = "-1">--select--</option>
										@forelse (App\Models\Product::get() as $pro)
										<option value = "{{ $pro->pro_id }}" {{ isset($offer->buyOffer->bo_buy_product) ? ($offer->buyOffer->bo_buy_product == $pro->pro_id ? 'selected' : '') : '' }}>{{ $pro->pro_title }}</option>
										@empty
										@endforelse
									</select>									
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Buy Quantity</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="number" class="form-control col-md-7 col-xs-12" name = "buy_qty" id = "buyQty" value="{{ isset($offer->buyOffer->bo_buy_qty) ? $offer->buyOffer->bo_buy_qty : '' }}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Get Product</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control col-md-7 col-xs-12 product" name="get_product">
										<option value = "-1">--select--</option>
										@forelse (App\Models\Product::get() as $pro)
										<option value = "{{ $pro->pro_id }}" {{ isset($offer->buyOffer->bo_get_product) ? ($offer->buyOffer->bo_get_product == $pro->pro_id ? 'selected' : '') : '' }}>{{ $pro->pro_title }}</option>
										@empty
										@endforelse
									</select>									
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Get Quantity</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="number" class="form-control col-md-7 col-xs-12" name = "get_qty" value="{{ isset($offer->buyOffer->bo_get_qty) ? $offer->buyOffer->bo_get_qty : '' }}">
								</div>
							</div>
						</div>
						<div id = "single" {!! $offer->offer_type == 101 ? '' : 'class="hidden"' !!}>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Product</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control col-md-7 col-xs-12 product" name="product">
										<option>--select--</option>
										@forelse (App\Models\Product::get() as $pro)
										<option value = "{{ $pro->pro_id }}" {{ $offer->offer_products == $pro->pro_id ? 'selected' : '' }}>{{ $pro->pro_title }}</option>
										@empty
										@endforelse
									</select>									
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Discount(%)</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="number" class="form-control col-md-7 col-xs-12" name = "discount" value="{{ $offer->offer_cost }}">
								</div>
							</div>
						</div>
						<div id = "combo" {!! $offer->offer_type == 102 ? '' : 'class="hidden"' !!}>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Products</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control col-md-7 col-xs-12 product" name="products[]" multiple="multiple">
										<option>--select--</option>
										@php
										$pros = explode(',', $offer->offer_products);
										@endphp
										@forelse (App\Models\Product::get() as $pro)
										<option value = "{{ $pro->pro_id }}" {{ in_array($pro->pro_id,$pros) ? 'selected' : '' }}>{{ $pro->pro_title }}</option>
										@empty
										@endforelse
									</select>									
								</div>
							</div>
						</div>
						<div class="form-group" id ="cost">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Offer Price</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="number" class="form-control col-md-7 col-xs-12" name = "cost" value="{{ $offer->offer_cost }}">
							</div>
						</div>
						<input type="hidden" name = "fixcost" id = "fix" value="{{ $offer->offer_cost }}">

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Description
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<textarea rows="4" class="form-control col-md-7 col-xs-12" name = "description">{{ $offer->offer_description }}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Duration
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
									<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
									<span></span> <b class="caret"></b>
								</div>								
							</div>					
						</div>	
						<input type="hidden" name="start" id = "from" value = "{{ $offer->offer_start }}">
						<input type="hidden" name="end" id = "to" value = "{{ $offer->offer_end }}">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Upload Banner
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="file" class="form-control col-md-7 col-xs-12" name = "banner_img">
							</div>
						</div>
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Update
							</button>							
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>

	<div id="addModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add new Occasion</h4>
				</div>
				<div class="modal-body">
					<form id = "{{ $oID }}Form" class="form-horizontal form-label-left">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" class="form-control col-md-7 col-xs-12" name = "title">
							</div>
						</div>
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>							
						</div>					
					</form>
				</div>							
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ asset('admin-assets/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('admin-assets/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
	CRUD.formSubmission({
		url : "{{ route('admin.'.$ID.'.index') }}", 
		type : 2,
		id : '{{ $ID }}Form',
		proData : function(data){
			console.log(data);
		}
	});
	getProduct = function(id){
		$.post('{{ route('admin.get-product') }}', {id: id}, function(data) {
			$('#cost').find('input').val(data.price);
			$('#fix').val(data.price);
		});
	}
	$('.type').on({
		'click' : function(){
			$('#combo').addClass('hidden');
			$('#single').addClass('hidden');
			$('#buy').addClass('hidden');
			$('#cost').removeClass('hidden');

			if(this.value == 101){
				$('#single').removeClass('hidden');
					// $('#combo').addClass('hidden');
					$('#cost').addClass('hidden');				
				}else if(this.value == 102){
					$('#combo').removeClass('hidden');
					// $('#single').addClass('hidden');
				}else{
					$('#buy').removeClass('hidden');
				}
				$('.product').select2();				
			}
		});
	function addNew() {		
		$('#addModal').modal('toggle');
		CRUD.formSubmission({
			url:"{{ route('admin.'.$oID.'.store') }}",
			type : 0, 
			id : oID+'Form',
			proData : function (data) {
				if(data.msg == 'success'){					
					$('#occOther').before(data.option);					
					$('#occOther').parent().val(data.val);					
					$('#addModal').modal('toggle');				
				}
			}
		}); 
	}

	$('#occasion').on({
		'change' : function(){
			if(this.value == 'other'){
				addNew();
			}
		}
	});

	$('#buyPro').on({
		'change' : function(){
			var val = this.value;

			if(val != '-1'){
				getProduct(val);
			}else{
				$('#cost').find('input').val('');				
			}
		}
	});
	$('#buyQty').on({
		'keyup' : function(){
			var cost = parseInt($('#fix').val()),
			qty = parseInt(this.value),
			newCost = cost;
			console.log(cost, qty, newCost);
			if(qty){
				newCost = (cost * qty).toFixed(2);
			}
			$('#cost').find('input').val(newCost);
		}
	});
</script>
@if ($offer->offer_type)
<script type="text/javascript">
	$('.product').select2();
</script>
@endif
@endpush