@extends('admin.layouts.master')
@section('title')
Edit {{ $pro->pro_title }} Product
@endsection
@php
$ID = 'product';
@endphp
@push('header')
<style type="text/css">
#description{
	height: 150px;
}
img {
	object-fit: contain;
	width: 200px;
}
</style>
<script>
	ID = '{{ $ID }}';
</script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/quill.snow.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/dropzone/dist/min/dropzone.min.css') }}">
<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />

@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Edit {{ $pro->pro_title }} Product</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_content">

					<div class="col-xs-2">
						<!-- required for floating -->
						<!-- Nav tabs -->
						<ul class="nav nav-tabs tabs-left">
							<li class="active" id = "des-tab"><a href="#description-tab" data-toggle="tab">Description</a>
							</li>
							<li id = "img-tab"><a href="#images-tab" data-toggle="tab" >Images</a>
							</li>									
						</ul>
					</div>
					<div class="col-xs-10">
						<!-- Tab panes -->
						<div class="tab-content">
							<div class="tab-pane active" id="description-tab">
								<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/form-data">
									<input type="hidden" name="id" value="{{ $pro->pro_id }}">
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12">Product Title
										</label>
										<div class="col-md-10 col-sm-10 col-xs-12">								
											<input type="text" class="form-control col-md-7 col-xs-12" name = "title" value="{{ $pro->pro_title }}">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12">Short Description
										</label>
										<div class="col-md-10 col-sm-10 col-xs-12">
											<input type="text" class="form-control col-md-7 col-xs-12" name = "sub_title" value="{{ $pro->pro_sub_title }}">
										</div>
									</div>
									<div class="form-group">
										<label id = "priceLabel" class="control-label col-md-2 col-sm-2 col-xs-12">Price
										</label>
										<div class="col-md-4 col-sm-10 col-xs-12">
											<input type="text" class="form-control col-md-7 col-xs-12" name = "price" data-validate="empty" value="{{ $pro->pro_price }}">
										</div>
										
										<label id = "priceLabel" class="control-label col-md-2 col-sm-2 col-xs-12">Quantity
										</label>
										<div class="col-md-4 col-sm-10 col-xs-12">
											<input type="number" class="form-control col-md-7 col-xs-12" name = "qty"  value="{{ $pro->pro_qty }}">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12">Category
										</label>
										<div class="col-md-10 col-sm-10 col-xs-12">
											<input type="hidden" name = "category" id = "cat" value="{{ $pro->pro_category }}">
											<input type="hidden" id = "catSub" value="{{ $pro->category->cat_sub }}">
											<select class="form-control" id = "category">
												<option value = "-1">--Select--</option>
												@foreach ($categories as $category)
												<option value = "{{ $category->cat_id.'-'.$category->cat_sub }}" {{ $pro->pro_category == $category->cat_id ? 'selected' : '' }}>{{ $category->cat_name }}</option>
												@endforeach
											</select>								
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12">Sub Category
										</label>
										<div class="col-md-10 col-sm-10 col-xs-12">
											@if ($pro->pro_sub_category)
											<select class="form-control" id = "sub-category" name = "sub_category">
												<option value = "-1">--Select--</option>
												@foreach (App\Models\SubCategory::where('sc_cat_id',$pro->pro_category)->get() as $sc)
												<option value = "{{ $sc->sc_id }}" {{ $pro->pro_sub_category == $sc->sc_id ? 'selected' : '' }}>{{ $sc->sc_name }}</option>
												@endforeach
											</select>
											@else
											<select class="form-control" disabled id = "sub-category" name = "sub_category">
												<option value = "-1">--Select--</option>
											</select>
											@endif
										</div>
									</div>
									<div class="form-group" id = "types">
										@php
										function replace($v) {
											return str_replace(' ', '_', strtolower($v));
										}
										$vals = ($pro->category->cat_sub) ? ($pro->category->cat_types) : ($pro->subCategory->sc_types);
										
										$types = \App\Models\Type::whereIn('type_id', explode(',', $vals))->get();


										@endphp
										{{-- {{ $pro->pro_frames }} --}}
										@forelse ($types as $type)
										<div class="form-group">
											<label class="control-label col-md-2 col-sm-2 col-xs-12">{{ $type->type_title }}</label>
											<div class="col-md-10 col-sm-10 col-xs-12">
												<select class="form-control col-md-7 col-xs-12 {{ replace($type->type_title) }}" name="{{ replace($type->type_title) }}[]" multiple>
													<option value = "-1">--select--</option>
													@forelse (App\Models\TypeDetail::where('td_type', $type->type_id)->get() as $t)
													<option value = "{{ $t->td_id }}" {{ in_array($t->td_id , explode(',', $pro['pro_'.replace($type->type_title)])) ? 'selected' : '' }}>{{ $t->td_title }}</option>
													@empty
													@endforelse									
												</select>
											</div>
										</div>										
										@empty
										@endforelse

									</div>
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12">Discount % (optional)</label>
										<div class="col-md-10 col-sm-10 col-xs-12">
											<input type="number" step="0.01" class="form-control col-md-7 col-xs-12" name = "discount" value="{{ $pro->pro_discount or '' }}">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12">Description
										</label>
										<div class="col-md-10 col-sm-10 col-xs-12">
											<div id = "description">{!! $pro->pro_description !!}</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12">Product Image
										</label>
										<div class="col-md-3 col-sm-3 col-xs-12 text-center">			
											<div class="clearfix"></div>

											<img src="{{ asset(isset($pro->display) ? $pro->display->img_thumb_path : 'no-image.png') }}" width="300" height="150" id = "display_picturePreview">
											<div class="clearfix"></div>
											<input type = "file" id ="display_picture" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name = "display_picture">		
											<label class="btn btn-success" for = "display_picture">Choose Image</label>			
										</div>
									</div>				
									<div class="ln_solid">
									</div>
									<div class="form-group text-center">							
										<button type="submit" class="btn btn-success">Update
										</button>							
									</div>					
								</form>
							</div>
							<div class="tab-pane" id="images-tab">
								<div class="jumbotron how-to-create" >

									<h3>Images <span id="photoCounter"></span></h3>
									<br />

									{!! Form::open(['url' => route('upload-post'), 'class' => 'dropzone', 'files'=>true, 'id'=>'real-dropzone']) !!}

									<div class="dz-message">

									</div>

									<div class="fallback">
										<input name="file" type="file" multiple />
									</div>

									<div class="dropzone-previews" id="dropzonePreview"></div>

									<h4 style="text-align: center;color:#428bca;">Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4>
									<input type="hidden" name="product" id = "productId">
									{!! Form::close() !!}

								</div>

								<div id="preview-template" style="display: none;">

									<div class="dz-preview dz-file-preview">
										<div class="dz-image"><img data-dz-thumbnail="" class="drop-img"></div>

										<div class="dz-details">
											<div class="dz-size"><span data-dz-size=""></span></div>
											<div class="dz-filename"><span data-dz-name=""></span></div>
										</div>
										<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
										<div class="dz-error-message"><span data-dz-errormessage=""></span></div>

										<div class="dz-success-mark">
											<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
												<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
												<title>Check</title>
												<desc>Created with Sketch.</desc>
												<defs></defs>
												<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
													<path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
												</g>
											</svg>
										</div>

										<div class="dz-error-mark">
											<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
												<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
												<title>error</title>
												<desc>Created with Sketch.</desc>
												<defs></defs>
												<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
													<g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
														<path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
													</g>
												</g>
											</svg>
										</div>

									</div>
								</div>
								{!! Form::hidden('csrf-token', csrf_token(), ['id' => 'csrf-token']) !!}

							</div>	

							{{-- <button>Finish</button>						 --}}
						</div>
					</div>
					<div class="clearfix"></div>
				</div>				
				
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/quill.js') }}"></script>
<script>	
	var options = {	  	 
		placeholder: 'Add Product description...',	  
		theme: 'snow'
	};
	var description = new Quill('#description', options);

	getSelectValues('category','sub-category', '{{ route('admin.getSubCategories') }}');	


	$('#'+ID+'Form').CRUD({
		url : "{{ route('admin.'.$ID.'.index') }}", 
		type : 2,
		extraVariables : function(){
			return {'description' : document.querySelector(".ql-editor").innerHTML};
		},
		processResponse : function(data){
			
			if(data.msg == 'success'){
				window.setTimeout(function () {
					$('#images-tab').show();
					$('#img-tab, #images-tab').show().addClass('active');
					$('#des-tab, #description-tab').removeClass('active');
				},1500);
				$('#productId').val(data.pid);
			}
		}
	});	


</script>
<script type="text/javascript" src = "{{ asset('/admin-assets/dropzone/dist/min/dropzone.min.js') }}"></script>
<script type="text/javascript">
	var photo_counter = 0;
	Dropzone.options.realDropzone = {

		uploadMultiple: false,
		parallelUploads: 100,
		maxFilesize: 8,
		previewsContainer: '#dropzonePreview',
		previewTemplate: document.querySelector('#preview-template').innerHTML,
		addRemoveLinks: true,
		dictRemoveFile: 'Remove',
		dictFileTooBig: 'Image is bigger than 8MB',

        // The setting up of the dropzone
        init:function() {
        	var thisDropzone = this;

        	        $.getJSON('{{ route('admin.images',['id' => $pro->pro_id]) }}', function(data) { // get the json response

        	            $.each(data, function(key,value){ //loop through it
        	            	// console.log(value);
        	                var mockFile = { name: value.img_title, id : value.img_id}; // here we get the file name and size as response 

        	                thisDropzone.options.addedfile.call(thisDropzone, mockFile);

        	                thisDropzone.options.thumbnail.call(thisDropzone, mockFile, '/'+value.img_thumb_path);//uploadsfolder is the folder where you have all those uploaded files
        	                thisDropzone.options.complete.call(thisDropzone,mockFile);

        	            });

        	        });
        	        this.on("removedfile", function(file) {        		
        	        	$.ajax({
        	        		type: 'POST',
        	        		url: '{{ route('upload-remove') }}',
        	        		data: {id: file.id, _token: $('#csrf-token').val()},
        	        		dataType: 'html',
        	        		success: function(data){        				
        	        			var rep = JSON.parse(data);
        	        			if(rep.code == 200)
        	        			{
        	        				photo_counter--;
        	        				$("#photoCounter").text( "(" + photo_counter + ")");
        	        			}

        	        		}
        	        	});

        	        } );
        	    },
        	    error: function(file, response) {
        	    	if($.type(response) === "string")
                var message = response; //dropzone sends it's own error messages in string
            else
            	var message = response.message;
            file.previewElement.classList.add("dz-error");
            _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            	node = _ref[_i];
            	_results.push(node.textContent = message);
            }
            return _results;
        },
        success: function(file,done) {
        	photo_counter++;
        	$("#photoCounter").text( "(" + photo_counter + ")");
        }
    };

    $('#quantityType').on({
    	'change' : function(){
    		var val = this.value;
    		if(val == '-1'){
    			$('#qtyName').addClass('hidden');
    		}else{ 
    			$('#qtyName').removeClass('hidden');
    			var label = ( val == '0') ? 'Minimum Weight' : 'Minimum Piece';
    			var name = ( val == '0') ? 'minimum_weight' : 'minimum_qty';
    			var pl = ( val == '0') ? 'Price per KG' : 'Price per Piece';

    			$('#qtyName label').html(label);
    			$('#qtyName input').attr('name',name);
    			$('#priceLabel').html(pl);
    		}
    	}
    });
    function geturl(name) {
    	var link = '{{ route('admin.home') }}/'+name;
    	return link;
    }
    
    imageUpload('display_picture');

    $('#category').on({
    	'change' : function(){
    		var val = this.value.split('-');
    		$('#cat').val(val[0]);
    		$('#catSub').val(val[1]);
    		
    		if(val[1] == '1'){
    			fetchTypes(val[0], 'cat');
    		}else{
    			$('#types').html('');
    		}
    	}
    });

    $('#sub-category').on({
    	'change' : function(){
    		fetchTypes(this.value, 'sub');
    	}
    });

    function fetchTypes(id) {
    	
    	$.post('{{ route('admin.fetch-types') }}', {id: id}, function(data) {
    		$('#types').html(data);
    	},'html');
    }    
    @foreach ($types as $t)
    	multiSelect('.{{ replace($t->type_title) }}', 'type-detail', {{ $t->type_id }});
    @endforeach
</script>
@endpush