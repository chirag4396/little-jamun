@extends('admin.layouts.master')
@section('title')
Create Testimonial
@endsection

@php
	$ID = 'faq';
@endphp
@push('header')
<style type="text/css">
#question,#answer{
	height: 150px;
}
</style>
<script>
	ID = '{{ $ID }}';
</script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/quill.snow.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/dropzone/dist/min/dropzone.min.css') }}">
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Create New FAQ</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<br />
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/form-data">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Qusetion
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<textarea name="qus" class="form-control" rows="5"></textarea>
								{{-- <div id = "question"></div>								 --}}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Answer
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">	
								<textarea name="ans" class="form-control" rows="5"></textarea>

								{{-- <div id = "answer"></div> --}}
							</div>
						</div>						
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>							
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script type="text/javascript" src="{{ asset('js/quill.js') }}"></script>
<script>	
	
	new Quill('#question', {	  	 
		placeholder: 'Type Question here...',
		theme: 'snow'
	});
	new Quill('#answer', {	  	 
		placeholder: 'Type Answer here...',
		theme: 'snow'
	});

	CRUD.formSubmission({
		url : "{{ route('admin.'.$ID.'.store') }}", 
		type : 0,
		id : '{{ $ID }}Form',		
		proData : function(data){
			console.log(data);			
		}
	});		
</script>
@endpush