@extends('admin.layouts.master')
@section('title')
Create Flavors
@endsection

@php
	$ID = 'pincode';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Create New Flavor</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<br />
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Area Name:
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="text" class="form-control col-md-7 col-xs-12" name = "area">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Area Code:
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="number" class="form-control col-md-7 col-xs-12" name = "code">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Price
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="text" class="form-control col-md-7 col-xs-12" name = "price">
							</div>
						</div>						
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script>
	CRUD.formSubmission({
		url : "{{ route('admin.'.$ID.'.store') }}", 
		type : 0,
		id : '{{ $ID }}Form',
		proData : function(data){
			console.log(data);
		}
	});		
</script>
@endpush