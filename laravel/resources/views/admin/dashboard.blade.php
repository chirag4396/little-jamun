@extends('admin.layouts.master')

@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        {{-- <a href="{{ route('admin.order.index',['type' => 2]) }}"> --}}
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
            {{-- <div class="count">{{ App\Models\OrderDetail::where('order_status',2)->count() }}</div> --}}
            <h3>New Paid Orders</h3>          
          </div>
        </a>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        {{-- <a href="{{ route('admin.order.index',['type' => 3]) }}"> --}}
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-comments-o"></i></div>
            {{-- <div class="count">{{ App\Models\OrderDetail::where('order_status',3)->count() }}</div> --}}
            <h3>Cancelled Orders</h3>
          </div>
        </a>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        {{-- <a href="{{ route('admin.product.index') }}"> --}}
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
            {{-- <div class="count">{{ App\Models\Product::count() }}</div> --}}
            <h3>Total Products</h3>
          </div>
        </a>
      </div>
      {{-- <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-check-square-o"></i></div>
          <div class="count">179</div>
          <h3>New Sign ups</h3>
          <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
      </div> --}}
    </div>
  </div>
</div>
@endsection