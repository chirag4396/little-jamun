@php
	function replace($v) {
		return str_replace(' ', '_', strtolower($v));
	}
@endphp
@forelse ($types as $type)
<div class="form-group">
	<label class="control-label col-md-2 col-sm-2 col-xs-12">{{ $type->type_title }}</label>
	<div class="col-md-10 col-sm-10 col-xs-12">
		<select class="form-control col-md-7 col-xs-12 {{ replace($type->type_title) }}" name="{{ replace($type->type_title) }}[]" multiple>
			<option value = "-1">--select--</option>
			@forelse (App\Models\TypeDetail::where('td_type', $type->type_id)->get() as $t)
			<option value = "{{ $t->td_id }}">{{ $t->td_title }}</option>
			@empty
			@endforelse									
		</select>
	</div>
</div>
<script type="text/javascript">
    multiSelect('.{{ replace($type->type_title) }}', 'type-detail', {{ $type->type_id }});
</script>
@empty
@endforelse
