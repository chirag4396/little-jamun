@push('header')
@php
$ID = 'category';
@endphp
<script>
	ID = '{{ $ID }}';
</script>
<style type="text/css">
img {
	object-fit: contain;
	width: 200px;
}
</style>
<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />
@endpush

<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Category Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" id = "typeName" name = "name" data-validate = "empty">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Description (optional)
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control col-md-7 col-xs-12" name = "description"></textarea>								
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Category Banner
		</label>
		<div class="col-md-3 col-sm-3 col-xs-12 text-center">			
			<div class="clearfix"></div>
			<img src="{{ asset('images/no-image.png') }}" width="300" height="150" id = "icon_pathPreview">
			<div class="clearfix"></div>
			<input type = "file" id ="icon_path" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name = "icon_path">		
			<label class="btn btn-success" for = "icon_path">Choose Image</label>			
		</div>
	</div>
	<div class="form-group margin-top">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Custom (select Yes if there is customization under these category)</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="radio" name="custom" value="1"> Yes
			<input type="radio" name="custom" value="0" checked> No
		</div>
	</div>
	<div class="form-group margin-top">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Will these contains Sub Catrgories ?</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="radio" name="sub" value="1"> Yes
			<input type="radio" name="sub" value="0" checked> No
		</div>
	</div>
	<div class="form-group" id = "types">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Available options to these Category</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="form-control col-md-7 col-xs-12 types" name="types[]" multiple="multiple" >
				<option value = "-1">--select--</option>
				@forelse (App\Models\Type::get() as $t)
				<option value = "{{ $t->type_id }}">{{ $t->type_title }}</option>
				@empty
				@endforelse									
			</select>								
		</div>	
		<div class="clearfix"></div>
	</div>	
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">Add
		</button>							
	</div>					
</form>


@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>

<script>		
	imageUpload('icon_path');	

	$('.types').select2({
		placeholder: 'Start typing slowly'
	});

</script>
@endpush