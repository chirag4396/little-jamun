@push('header')
@php
	$ID = 'banner';
@endphp
<script>
	ID = '{{ $ID }}';
</script>
<style type="text/css">
img {
	object-fit: contain;
	width: 200px;
}
</style>
@endpush
<form id = "{{ $ID }}Form" class="form-horizontal form-label-left">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">								
			<input type="text" class="form-control col-md-7 col-xs-12" name = "title">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Sub Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">								
			<input type="text" class="form-control col-md-7 col-xs-12" name = "sub_title">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Sub Category Banner
		</label>
		<div class="col-md-3 col-sm-3 col-xs-12 text-center">			
			<div class="clearfix"></div>
			<img src="{{ asset('images/no-image.png') }}" width="300" height="150" id = "img_pathPreview">
			<div class="clearfix"></div>
			<input type = "file" id ="img_path" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name = "img_path">		
			<label class="btn btn-success" for = "img_path">Choose Image</label>			
		</div>
	</div>
	{{-- <div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Sub Banner
		</label>
		<div class="col-md-3 col-sm-3 col-xs-12 text-center">			
			<div class="clearfix"></div>
			<img src="{{ asset('images/no-image.png') }}" width="300" height="150" id = "banner2Preview">
			<div class="clearfix"></div>
			<input type = "file" id ="banner2" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name = "banner2">		
			<label class="btn btn-success" for = "banner2">Choose Image</label>			
		</div>
	</div> --}}
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">Add
		</button>							
	</div>					
</form>

@push('footer')
<script>	
	imageUpload('img_path');	
	// imageUpload('banner2');	

</script>
@endpush