@extends('admin.layouts.master')
@section('title')
Create Child Category
@endsection

@php
	$ID = 'child-category';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Create New Child Category</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<br />
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/form-data">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Select Sub Category
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="form-control" name = "sc_id">
									<option value = "-1">Select</option>
									@foreach (App\Models\SubCategory::get() as $sc)
									<option value = "{{ $sc->sc_id }}">{{ $sc->sc_name }}</option>
									@endforeach
								</select>								
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Child Category Name
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="text" class="form-control col-md-7 col-xs-12" name = "name">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Description
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<textarea class="form-control col-md-7 col-xs-12" name = "description"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Child Category Thumbnail
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="file" class="form-control col-md-7 col-xs-12" name = "picture">
							</div>
						</div>						
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>							
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script>
	CRUD.formSubmission({
		url : "{{ route('admin.'.$ID.'.store') }}", 
		type : 0,
		id : '{{ $ID }}Form'
	});		
</script>
@endpush