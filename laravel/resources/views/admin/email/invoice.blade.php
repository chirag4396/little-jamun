<html>
<head>
	<title>Little Jamun Invoice</title>
	<style type="text/css">

	html, body{
		font-size: 100%;
		font-family:'Arimo', sans-serif;
		background:#ffffff;
	}

	#page-wrap {
		width: 700px;
	}
	.center-justified {
		text-align: justify;
		margin: 0 auto;
		width: 30em;
	}
	table.outline-table {
		border: 1px solid #958f8f;
		border-spacing: 0;
	}
	tr.border-bottom td, td.border-bottom {
		border-bottom: 1px solid #958f8f;
	}
	tr.border-top td, td.border-top {
		border-top: 1px solid #958f8f;
	}
	tr.border-right td, td.border-right {
		border-right: 1px solid #958f8f;
	}
	tr.border-right td:last-child {
		border-right: 0px;
	}
	tr.center td, td.center {
		vertical-align: text-top;
		padding-left: 10px;
	}
	td.pad-left {
		padding-left: 10px;
	}
	tr.right-center td, td.right-center {
		text-align: right;
		padding-right: 50px;
	}
	tr.right td, td.right {
		text-align: right;
	}
	.grey {
		background:grey;
	}
	td
	{
		height: 33px;
		padding: 5px;
		line-height: 20px;
		font-size: 14px;
	}
	.invoice{
		font-weight: normal;		
	}
</style>
</head>
<body>
	<div>		
		<table width="100%">
			<tbody>
				<tr>
					<td width="30%">
						<img src="{{ asset('images/logo-black.png') }}" width="100%"> <!-- your logo here -->
					</td>
					<td width="40%" valign="top" align="center">
						<h1 class="invoice">Invoice </h1>
					</td>
					<td width="30%" align="right" valign="top">
						<strong>Date :</strong> {{ Carbon\Carbon::parse($order->order_created_at)->format('jS M, Y') }}<br>
						<strong>Order ID :</strong> {{ $order->order_d_id }}<br>						
					</td>
				</tr>	
			</tbody>
		</table>

		<table width="100%" class="outline-table">
			<tbody>
				
				<tr class="border-bottom border-right center">
					<td width="50%"><strong></strong></td>
					<td width="50%"><strong>Shipping Address</strong></td>
				</tr>
				
				<tr class="border-right">
					<td class="pad-left" valign="top">
						<strong>{{ config('app.name') }}</strong><br>
						Pushpanjali Enterprise,<br>
						Shop No. 6,<br>
						Sacred Heart,<br>
						Wnaowarie,<br>
						Pune - 411 040.<br>
						<strong>Email :</strong> {{ config('app.support_email') }}<br>
						<strong>Contact :</strong> {{ config('app.mobile') }}
					</td>
					<td class="pad-left" valign="top">
						<strong>{{ $order->userDetail->user->name }}</strong><br>
						@php
						$add = implode(',</br>', explode(',',$order->userDetail->ud_address));
						@endphp
					{!! $add !!}<br>
					<strong>Email :</strong> {{ $order->userDetail->user->email }}<br>
					<strong>Contact :</strong> {{ $order->userDetail->ud_mobile }}
				</td>
			</tr>

		</tbody>
	</table><br>

	{{-- <table width="100%" class="outline-table">
		<tbody>

			<tr class="border-bottom border-right center">
				<td width="100%" colspan="2"><strong>Order Details</strong></td>
			</tr>

			<tr class="border-right">
				<td width="50%" class="pad-left">
					<strong>Brownie Point</strong><br>
					303 Vardhaman Heights <br>
					1328/29 Shukrawar Peth<br>
					Telephone Bhavan<br>
					Pune - 411002
					<br><br>

					<strong>Email :</strong> prajaktprakashan1@gmail.com
					<strong>Contact :</strong> 02024469595 / 7588669595
					<strong>Website :</strong> http://prajaktprakashan.com<br>
				</td>
				<td width="50%" class="center">
					<strong>Date Added :</strong> 26/10/2017<br>
					<strong>Order ID :</strong> 92<br>
					<strong>Payment Method :</strong> PayUMoney<br>
					<strong>Shipping Method :</strong> Flat Shipping Rate
				</td>
			</tr>

		</tbody>
	</table><br> --}}


	<table width="100%" class="outline-table">
		<tbody>

			<tr class="border-bottom border-right center">
				<td width="20%"><strong>Product Name</strong></td>
				<td width="5%" align="center"><strong>Qty</strong></td>
				<td width="10%" align="center"><strong>Unit Price</strong></td>
				<td width="10%" align="right"><strong>Total</strong></td>
			</tr>
			@php
			$per = 0;
			@endphp
			@forelse ($order->products()->get() as $op)	
			@php
			$total = $op->op_quantity * $op->op_price;				
			@endphp
			<tr class="border-bottom border-right">
				<td>{{ (!empty($op->product->pro_title)) ? $op->product->pro_title : $op->offer->offer_title }}</td>
				<td width="5%" align="center">{{ $op->op_quantity }}</td>
				<td width="5%" align="right">{{ 'Rs.'.number_format($op->op_price,2) }}</td>
				<td width="10%" align="right">{{ 'Rs.'.number_format($total,2) }}</td>
			</tr>
			@empty

			@endforelse
			<tr class="border-right">
				<td colspan="3" style="height:30px; text-align: right;"><strong>Sub-Total</strong></td>
				<td class="center" style="height:30px;text-align: right">{{ 'Rs.'.number_format($order->order_total-$order->order_shipping,2) }}</td>
			</tr>
			<tr class="border-right">
				<td colspan="3" style="height:30px; text-align: right;"><strong>Flat Shipping Rate</strong></td>
				<td class="center" style="height:30px;text-align: right">{{ 'Rs.'.number_format($order->order_shipping,2) }}</td>
			</tr>
			<tr class="border-right">
				<td colspan="3" style="height:30px; text-align: right;"><strong>Total</strong></td>
				<td class="center" style="height:30px;text-align: right"><strong>{{ 'Rs.'.number_format($order->order_total,2) }}</strong></td>
			</tr>

		</tbody>
	</table>	

</div>
</body>
</html>
