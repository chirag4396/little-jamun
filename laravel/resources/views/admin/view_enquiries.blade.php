@extends('admin.layouts.master')

@section('title')
Enquiries
@endsection
@push('header')
<!-- Datatables -->
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = 'enquiry';
@endphp
<script>
  var ID = '{{ $ID }}';
  var type = {{ $type }};

  additional = function(data){
    var editForm = 'edit'+ID+'Form';
    if (type == 101){ 
      $('#enBtn').addClass('hidden');
    }else{
      $('#enBtn').removeClass('hidden');
      if($('#img').length){
        $('#img').attr('src','../'+data.img_thumb_path);

        $('#'+editForm+' button[type="submit"]').attr('type','button').attr('onclick','$(\'#sendQuote\').removeClass(\'hidden\');').html('SEND QUOTE');
        $('#enqProduct').val(data.pro_id);
        $('#enqMsg').val(data.enq_id);
      }else{
        $('#'+editForm+' button[type="submit"]').html('UPDATE');
      }
    }
  }
</script>
@endpush

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Enquiries</h3>
      </div>
      {{-- <div class="title_right">
        <button class="btn btn-primary pull-right" onclick="CRUD.add('{{ $ID }}','{{ route('admin.'.$ID.'.store') }}');">Add</button>
      </div> --}}
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">            
            <table id="{{ $ID }}Table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Client</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Query</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($enquiries as $k => $e)
                <tr id = "tr-{{ $e->enq_id }}">
                  <td>{{ ++$k }}</td>
                  <td>{{ $e->enq_name }}</td>
                  <td>{{ $e->enq_email }}</td>
                  <td>{{ $e->enq_mobile }}</td>                  
                  <td>{{ str_limit($e->enq_query, 20) }}</td>                  
                  <td>{{ Carbon\Carbon::parse($e->enq_created_at)->toDayDateTimeString() }}</td>
                  <td>                    
                    <a href="#" class="btn btn-info btn-xs" onclick = "CRUD.edit({id:'{{ $ID }}', getUrl:'{{ route('admin.'.$ID.'.index') }}' ,fetchId:'{{ $e->enq_id }}', proData : additional, extraVariables:{type: {{ $type }} }});"><i class="fa fa-pencil"></i> View</a>
                    <a onclick = "CRUD.delete('{{ $ID }}',{{ $e->enq_id }});" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>                    
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan = "4" class="text-center">No {{ $ID }} Found <a class = "btn btn-primary" href="{{ route('admin.'.$ID.'.create') }}">Add some now</a></td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="{{ $ID }}Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="{{ $ID }}ModalLabel">Edit {{ $ID }}</h4>
          </div>
          <div class="modal-body">
            <form id = "{{ $ID }}Form" class="form-horizontal form-label-left">
              <input type="hidden" name="sid" value="1">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Client:
                </label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12 no-borders" name = "enq_name" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile:
                </label>
                <div class="col-md-8 col-sm-6 col-xs-12">     
                  <input type="text" class="form-control col-md-7 col-xs-12 no-borders" name = "enq_mobile" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email:
                </label>
                <div class="col-md-8 col-sm-6 col-xs-12">     
                  <input type="text" class="form-control col-md-7 col-xs-12 no-borders" name = "enq_email" readonly>
                </div>
              </div>
              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Query:
                </label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <textarea class="form-control col-md-7 col-xs-12 no-borders" rows="5" name = "enq_query" readonly> </textarea>
                </div>
              </div>
              
              @if ($type == 102)
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Title:
                </label>
                <div class="col-md-8 col-sm-6 col-xs-12">     
                  <input type="text" class="form-control col-md-7 col-xs-12 no-borders" name = "pro_title" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Image:
                </label>
                <div class="col-md-8 col-sm-6 col-xs-12">     
                  <img class="pro-img-1" id = "img">
                </div>
              </div>
              @endif
              <div class="ln_solid">
              </div>
              <div class="form-group text-center" id = "enBtn">              
                <button type="submit" class="btn btn-success">Add
                </button>             
              </div> 
            </form>  

            <div id = "sendQuote" class="hidden">
              <div class="ln_solid">
              </div>
              <form class="form-horizontal" id = "sendMailForm">
                <input type="hidden" name="pro_id" id = "enqProduct">
                <input type="hidden" name="enq_id" id = "enqMsg">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Set Price:
                  </label>
                  <div class="col-md-8 col-sm-6 col-xs-12">     
                    <input type="text" class="form-control col-md-7 col-xs-12" name = "price" id = "enqPrice">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Message (Optional):
                  </label>
                  <div class="col-md-8 col-sm-6 col-xs-12">     
                    <textarea class="form-control col-md-7 col-xs-12" name = "query" id = "enqMsg"></textarea>
                  </div>
                </div>
                <div class="form-group text-center">              
                  <button type="submit" class="btn btn-success">Send
                  </button>             
                </div> 
              </form>

            </div>  
            <div class="clearfix"></div>          
          </div>          
        </div>
      </div>
    </div>

    <div class="modal fade" id="{{ $ID }}DModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Are You Sure?</h4>
            <br>
            <div>
              <form id = "{{ $ID }}DForm">                
                <button type = "button" class="btn btn-danger" id = "yes">Yes</button>
                <button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
              </form>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="text/javascript">  
  var update = "{{ url('admin/'.$ID) }}",
  deleteU = "{{ url('admin/'.$ID) }}",
  store = "{{ route('admin.'.$ID.'.store') }}",
  storeQuote = "{{ route('admin.'.$ID.'.send') }}";

  Table.init(ID);

  CRUD.formSubmission({
    url : storeQuote, 
    type : 0,
    id : 'sendMailForm',
    proData : function(data){
      console.log(data);
      if(data.msg == 'successSend'){
        Msg.txt(data.msg,'sendMailForm');
        Msg.timer(function(){
          $('#sendQuote').addClass('hidden');
          $('#enquiryModal').modal('toggle');
        });
      }
    }
  }); 
</script>
@endpush