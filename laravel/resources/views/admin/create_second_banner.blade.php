@extends('admin.layouts.master')
@section('title')
Create Banner
@endsection
@php
	$ID = 'second-banner';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Create New Banner</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">					
					@include('admin.forms.second_banner_form')
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
	<script type="text/javascript">
		$('#{{ $ID }}Form').CRUD({
			url : '{{ route('admin.'.$ID.'.store') }}',
			validation:false,	
			processResponse : function (data) {				
			}
		});
	</script>
@endpush
