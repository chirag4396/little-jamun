@extends('admin.layouts.master')
@section('title')
Create Offer
@endsection

@php
$ID = 'offer';
$oID = 'occasion';
@endphp
@push('header')

<script>
	ID = '{{ $ID }}';
	oID = '{{ $oID }}';
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Create New Offer</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<br />
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Occasion</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<select name = "occasion" class="form-control" id = "occasion">
									<option value="-1-">--select--</option>
									@forelse (App\Models\Occasion::get() as $occ)
									<option value="{{ $occ->occ_id }}">{{ $occ->occ_title }}</option>
									@empty
									@endforelse
									<option value="other" id = "occOther">Other</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Title
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="text" class="form-control col-md-7 col-xs-12" name = "title">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>
							<div class="col-md-6 col-sm-6 col-xs-12 margin-top">
								<input type="radio" value = "101" class="type" name = "type"> Single
								<input type="radio" value = "102" class="type" name = "type"> Combo	
								<input type="radio" value = "103" class="type" name = "type"> Buy and Get Free	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Eggs?</label>
							<div class="col-md-6 col-sm-6 col-xs-12 margin-top">
								<input type="radio" value = "0" name = "veg" checked> Yes
								<input type="radio" value = "1" name = "veg"> No	
							</div>
						</div>
						<div id = "buy" class="hidden">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Product</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control col-md-7 col-xs-12 product" id = "buyPro" name="buy_product">
										<option value = "-1">--select--</option>
										@forelse (App\Models\Product::get() as $pro)
										<option value = "{{ $pro->pro_id }}">{{ $pro->pro_title }}</option>
										@empty
										@endforelse
									</select>									
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Buy Quantity</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="number" class="form-control col-md-7 col-xs-12" name = "buy_qty" id = "buyQty">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Get Product</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control col-md-7 col-xs-12 product" name="get_product">
										<option value = "-1">--select--</option>
										@forelse (App\Models\Product::get() as $pro)
										<option value = "{{ $pro->pro_id }}">{{ $pro->pro_title }}</option>
										@empty
										@endforelse
									</select>									
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Get Quantity</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="number" class="form-control col-md-7 col-xs-12" name = "get_qty">
								</div>
							</div>
						</div>
						<div id = "single" class="hidden">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Product</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control col-md-7 col-xs-12 product" name="product">
										<option value = "-1">--select--</option>
										@forelse (App\Models\Product::get() as $pro)
										<option value = "{{ $pro->pro_id }}">{{ $pro->pro_title }}</option>
										@empty
										@endforelse
									</select>									
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Discount(%)</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="number" class="form-control col-md-7 col-xs-12" name = "discount">
								</div>
							</div>
						</div>
						<div id = "combo" class="hidden">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Products</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control col-md-7 col-xs-12 product" name="products[]" multiple="multiple">
										<option value = "-1">--select--</option>
										@forelse (App\Models\Product::get() as $pro)
										<option value = "{{ $pro->pro_id }}">{{ $pro->pro_title }}</option>
										@empty
										@endforelse
									</select>									
								</div>
							</div>
						</div>
						<div class="form-group" id ="cost">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Offer Price</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="number" class="form-control col-md-7 col-xs-12" name = "cost">
							</div>
						</div>
						<input type="hidden" name = "fixcost" id = "fix">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Description
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<textarea rows="4" class="form-control col-md-7 col-xs-12" name = "description"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Duration
							</label>
							{{-- <div class="control-group">
								<div class="controls">
									<div class="input-prepend input-group">
										<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
										<input name="reservation-time" id="reservation-time" class="form-control" value="01/01/2016 - 01/25/2016" type="text">
									</div>
								</div>
							</div> --}}
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
									<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
									<span></span> <b class="caret"></b>
								</div>								
							</div>					
						</div>	
						<input type="hidden" name="start" id = "from">
						<input type="hidden" name="end" id = "to">
						{{-- <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Start
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="datetime-local" class="form-control col-md-7 col-xs-12" name = "start">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Ends
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="datetime-local" class="form-control col-md-7 col-xs-12" name = "end">
							</div>
						</div> --}}
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Upload Banner
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="file" class="form-control col-md-7 col-xs-12" name = "banner_img">
							</div>
						</div>
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>							
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>

	<div id="addModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add new Occasion</h4>
				</div>
				<div class="modal-body">
					<form id = "{{ $oID }}Form" class="form-horizontal form-label-left">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" class="form-control col-md-7 col-xs-12" name = "title">
							</div>
						</div>
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>							
						</div>					
					</form>
				</div>							
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ asset('admin-assets/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('admin-assets/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>

	CRUD.formSubmission({
		url : "{{ route('admin.'.$ID.'.store') }}", 
		type : 0,
		id : '{{ $ID }}Form'
	});	
	getProduct = function(id){
		$.post('{{ route('admin.get-product') }}', {id: id}, function(data) {
			$('#cost').find('input').val(data.price);
			$('#fix').val(data.price);
		});
	}
	$('.type').on({
		'click' : function(){
			$('#combo').addClass('hidden');
			$('#single').addClass('hidden');
			$('#buy').addClass('hidden');
			$('#cost').removeClass('hidden');

			if(this.value == 101){
				$('#single').removeClass('hidden');
				// $('#combo').addClass('hidden');
				$('#cost').addClass('hidden');				
			}else if(this.value == 102){
				$('#combo').removeClass('hidden');
				// $('#single').addClass('hidden');
			}else{
				$('#buy').removeClass('hidden');
			}
			$('.product').select2();				
		}
	});
	function addNew() {		
		$('#addModal').modal('toggle');
		CRUD.formSubmission({
			url:"{{ route('admin.'.$oID.'.store') }}",
			type : 0, 
			id : oID+'Form',
			proData : function (data) {
				if(data.msg == 'success'){					
					$('#occOther').before(data.option);					
					$('#occOther').parent().val(data.val);					
					$('#addModal').modal('toggle');				
				}
			}
		}); 
	}

	$('#occasion').on({
		'change' : function(){
			if(this.value == 'other'){
				addNew();
			}
		}
	});

	$('#buyPro').on({
		'change' : function(){
			var val = this.value;

			if(val != '-1'){
				getProduct(val);
			}else{
				$('#cost').find('input').val('');				
			}
		}
	});
	$('#buyQty').on({
		'keyup' : function(){
			var cost = parseInt($('#fix').val()),
			qty = parseInt(this.value),
			newCost = cost;
			console.log(cost, qty, newCost);
			if(qty){
				newCost = (cost * qty).toFixed(2);
			}
			$('#cost').find('input').val(newCost);
		}
	});
</script>
@endpush