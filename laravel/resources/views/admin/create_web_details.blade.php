@extends('admin.layouts.master')
@section('title')
Web Details
@endsection

@php
$ID = 'flavor';
@endphp
@push('header')
<style type="text/css">
#about,#terms,#privacy{
	height: 200px;
	margin-bottom: 80px;
}
</style>
<script>
	ID = '{{ $ID }}';
</script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/quill.snow.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/dropzone/dist/min/dropzone.min.css') }}">
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3>Web Details</h3>
		</div>		
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<br />
					<div class="form-horizontal form-label-left">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">About Us
							</label>
							<div class="col-md-8 col-sm-6 col-xs-12">								
								<div id = "about">{!! $data->about !!}</div>
							</div>
						</div>						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Terms and Conditions
							</label>
							<div class="col-md-8 col-sm-6 col-xs-12">								
								<div id = "terms"></div>								
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Privacy Policies
							</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<div id = "privacy"></div>								
							</div>
						</div>
						<div class="ln_solid">
						</div>						
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script type="text/javascript" src="{{ asset('js/quill.js') }}"></script>

<script>
	function sendWData(data,id){
		Msg.txt('update',id);
		$.ajax({
			url : '{{ route('admin.web-detail') }}',
			type : 'post',
			data : {field: id,data : data},
			success : function (data){
				Msg.txt(data.msg,id);	
			}
		});
	}

	var Delta = Quill.import('delta');

	var getUrl = '{{ route('admin.web-detail') }}';

	var about = new Quill('#about', {	  	 
		placeholder: 'Type About here...',
		theme: 'snow'
	});

	var change = new Delta(),
	aboutText = '';

	about.on('text-change', function(delta) {
		change = change.compose(delta);  
		aboutText = document.querySelector(".ql-editor").innerHTML;
		aboutText = aboutText.replace(/"/g, '\\"');  

		sendWData(aboutText,'about');	  
	}); 

	var terms = new Quill('#terms', {	  	 
		placeholder: 'Type Terms and Conditions here...',
		theme: 'snow'
	});
	terms.on('text-change', function(delta) {
		change = change.compose(delta);  
		termsText = document.querySelector(".ql-editor").innerHTML;
		termsText = termsText.replace(/"/g, '\\"');  

		sendWData(termsText,'terms');	  
	}); 
	
	var privacy = new Quill('#privacy', {	  	 
		placeholder: 'Type Privacy here...',
		theme: 'snow'
	});
	privacy.on('text-change', function(delta) {
		change = change.compose(delta);  
		privacyText = document.querySelector(".ql-editor").innerHTML;
		privacyText = privacyText.replace(/"/g, '\\"');  

		sendWData(privacyText,'privacy');	  
	}); 	
</script>
@endpush