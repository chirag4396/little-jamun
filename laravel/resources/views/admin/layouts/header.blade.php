<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name') }} (Admin) | @yield('title')</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/fav.png') }}">   

  <!-- Bootstrap -->
  <link href="{{ asset('admin-assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{ asset('admin-assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{ asset('admin-assets/nprogress/nprogress.css') }}" rel="stylesheet">
  <!-- bootstrap-daterangepicker -->
  <link href="{{ asset('admin-assets/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="{{ asset('admin-assets/css/custom.css') }}" rel="stylesheet">
  <style type="text/css">
  .loader{
    display: none;
    color: white;  
    height: 1000px;
    font-family: initial;
    position: fixed;
    top: 0;
    font-size: 36px;
    z-index: 9999;
    background: rgba(0, 0, 0, 0.88);
    width: 100%;
    text-align: center;
    padding-top: 250px;
  }
  #loader2{
    display: block;            
    background: #2b0039e6 !important;
  }
</style>
@stack('header')
</head>

<body class="nav-md">
  <div class="loader" id = "loader2">
    <img src="{{ asset('images/logo.png') }}" width = "300">
  </div>  
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="{{ route('admin.home') }}" class="site_title"> <span>{{ config('app.name') }}</span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              <img src="{{ asset('admin-assets/images/img.jpg') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2>{{ Auth::user()->name }}</h2>
            </div>
          </div>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              {{-- <h3>General</h3> --}}
              <ul class="nav side-menu">
                <li><a><i class="fa fa-home"></i> Orders <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.order.index',['type' => 2]) }}">Paid Orders</a></li>
                    <li><a href="{{ route('admin.order.index',['type' => 3]) }}">Cancelled Orders</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Products <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.product.index') }}">Product List</a></li>
                    <li><a href="{{ route('admin.product.create') }}">Add Product</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Categories <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.category.index') }}">Category List</a></li>
                    <li><a href="{{ route('admin.category.create') }}">Add Category</a></li>
                    <li><a href="{{ route('admin.sub-category.index') }}">Sub Category List</a></li>
                    <li><a href="{{ route('admin.sub-category.create') }}">Add Sub Category</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Delivery Pincode <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.pincode.index') }}">Pincode List</a></li>
                    <li><a href="{{ route('admin.pincode.create') }}">Add Pincode</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Banner <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.banner.index') }}">Banner List</a></li>
                    <li><a href="{{ route('admin.banner.create') }}">Add Banner</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Second Banner <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.second-banner.index') }}">Second Banner List</a></li>
                    <li><a href="{{ route('admin.second-banner.create') }}">Add Second Banner</a></li>
                  </ul>
                </li>
                <li><a href="{{ route('admin.enquiry.index',['type' => 101]) }}"><i class="fa fa-home"></i> Enquiries</a></li>
                <li><a><i class="fa fa-home"></i> FAQ's <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.faq.index') }}">FAQ List</a></li>
                    <li><a href="{{ route('admin.faq.create') }}">Add FAQ</a></li>
                  </ul>
                </li>
                <li><a href="{{ route('admin.web-detail.index') }}"><i class="fa fa-home"></i> {{ config('app.name') }} Details </a></li>
                <li><a href="{{ route('admin.import-product') }}"><i class="fa fa-home"></i> Import Products </a></li>
                <li><a><i class="fa fa-home"></i> Report <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.stock') }}">Stock</a></li>
                  </ul>
                </li>
                {{-- <li><a><i class="fa fa-home"></i> Orders <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.order.index',['type' => 2]) }}">Paid Orders</a></li>
                    <li><a href="{{ route('admin.order.index',['type' => 3]) }}">Cancelled Orders</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Customized Enquiry <span class="label label-success pull-right">{{ \App\Models\CustomOrderEnquiry::where('coe_status',0)->count() }}</span><span class="label label-primary pull-right">{{ \App\Models\CustomOrderEnquiry::where('coe_status',1)->count() }}</span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.custom-enquiry.index',['status' => 0]) }}"><i class="fa fa-home"></i>New Enquiries<span class="label label-success pull-right">{{ \App\Models\CustomOrderEnquiry::where('coe_status',0)->count() }}</span></a></li>
                    <li><a href="{{ route('admin.custom-enquiry.index',['status' => 1]) }}"><i class="fa fa-home"></i>Old Enquiries<span class="label label-primary pull-right">{{ \App\Models\CustomOrderEnquiry::where('coe_status',1)->count() }}</span></a></li>                    
                  </ul>
                </li>
                <li><a href="{{ route('admin.enquiry.index',['type' => 101]) }}"><i class="fa fa-home"></i> Enquiries</a></li>
                <li><a><i class="fa fa-home"></i> Categories <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.category.index') }}">Category List</a></li>
                    <li><a href="{{ route('admin.category.create') }}">Add Category</a></li>
                    <li><a href="{{ route('admin.sub-category.index') }}">Sub Category List</a></li>
                    <li><a href="{{ route('admin.sub-category.create') }}">Add Sub Category</a></li>
                    <li><a href="{{ route('admin.child-category.index') }}">Child Category List</a></li>
                    <li><a href="{{ route('admin.child-category.create') }}">Add Child Category</a></li>
                    <li><a href="{{ route('admin.super-child-category.index') }}">Super Child Category List</a></li>
                    <li><a href="{{ route('admin.super-child-category.create') }}">Add Super Child Category</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Flavors <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.flavor.index') }}">Flavor List</a></li>
                    <li><a href="{{ route('admin.flavor.create') }}">Add Flavor</a></li>                    
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Products <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.product.index') }}">Product List</a></li>
                    <li><a href="{{ route('admin.product.create') }}">Add Product</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Delivery Pincode <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.pincode.index') }}">Pincode List</a></li>
                    <li><a href="{{ route('admin.pincode.create') }}">Add Pincode</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Offers <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.offer.index') }}">Offer List</a></li>
                    <li><a href="{{ route('admin.offer.create') }}">Add Offer</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Banner <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.banner.index') }}">Banner List</a></li>
                    <li><a href="{{ route('admin.banner.create') }}">Add Banner</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Second Banner <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.second-banner.index') }}">Second Banner List</a></li>
                    <li><a href="{{ route('admin.second-banner.create') }}">Add Second Banner</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Testimonials <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.testimonial.index') }}">Testimonial List</a></li>
                    <li><a href="{{ route('admin.testimonial.create') }}">Add Testimonial</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Our Clients <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.client.index') }}">Client List</a></li>
                    <li><a href="{{ route('admin.client.create') }}">Add Client</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> FAQ's <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.faq.index') }}">FAQ List</a></li>
                    <li><a href="{{ route('admin.faq.create') }}">Add FAQ</a></li>
                  </ul>
                </li>
                <li><a href="{{ route('admin.web-detail.index') }}"><i class="fa fa-home"></i> {{ config('app.name') }} Details </a></li>     --}}            
              </ul>
            </div>
            

          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small hidden">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <nav>
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log Out <i class="fa fa-sign-out"></i> 
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>                
              </li>                      
            </ul>
          </nav>
        </div>
      </div>
        <!-- /top navigation -->