@extends('admin.layouts.master')

@section('title')
Orders
@endsection
@push('header')
<!-- Datatables -->
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = 'order';
@endphp
<script>
  var ID = '{{ $ID }}';
</script>
<script type="text/javascript">
  viewSingle = function(id, getUrl, fetchId, additional){     
    var editForm = 'edit'+id+'Form',      
    u = getUrl+'/'+fetchId+'/edit';
    $.get(u, function(data) {      
      additional(data);
    });    
  };

  additional = function(data){    
    $('#'+ID+'ModalLabel').html('View Order ');
    $('#'+ID+'Modal').modal('toggle');
    $('#modalData').html(data);
  };
</script>
@endpush

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Orders</h3>
      </div>
      {{-- <div class="title_right">
        <button class="btn btn-primary pull-right" onclick="CRUD.add('{{ $ID }}','{{ route('admin.'.$ID.'.store') }}');">Add</button>
      </div> --}}
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">            
            <table id="{{ $ID }}Table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Client</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Pincode</th>
                  <th>Quantity</th>
                  <th>Amount</th>
                  <th>Date</th>                  
                  <th>Status</th>                  
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($orders as $k => $o)
                <tr id = "tr-{{ $o->order_id }}">
                  <td>{{ ++$k }}</td>
                  <td>{{ $o->user->name }}</td>
                  <td>{{ $o->user->email }}</td>
                  <td>{{ $o->userDetail->ud_mobile }}</td>                  
                  <td>{{ $o->userDetail->ud_pincode }}</td>
                  <td>{{ $o->products()->count() }}</td>              
                  <td>{{ '₹ '.number_format($o->order_total) }}</td>
                  <td>{{ Carbon\Carbon::parse($o->order_created_at)->toDayDateTimeString() }}</td>
                  <td>{{ $o->order_delivery_type ? 'COD' : $o->status->os_title }}</td>

                  <td>                    
                    <a href="#" class="btn btn-info btn-xs" onclick = "viewSingle('{{ $ID }}', '{{ route('admin.'.$ID.'.index') }}' ,'{{ $o->order_id }}', additional);"><i class="fa fa-pencil"></i> View </a>
                    <a href="{{ route('admin.order-invoice',['id' => $o->order_id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Print </a>                    
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan = "10" class="text-center">No {{ $ID }} Found</td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="{{ $ID }}Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="{{ $ID }}ModalLabel">View {{ $ID }}</h4>
          </div>
          <div class="modal-body">
            <div class="form-horizontal">
              <div class="row" id = "modalData">
                {{-- @include('view.name', ['some' => 'data'])                 --}}
              </div>
              <div class="clearfix"></div>
            </div>
          </div>          
        </div>
      </div>
    </div>

    <div class="modal fade" id="{{ $ID }}DModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Are You Sure?</h4>
            <br>
            <div>
              <form id = "{{ $ID }}DForm">                
                <button type = "button" class="btn btn-danger" id = "yes">Yes</button>
                <button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
              </form>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="text/javascript">  
  var update = "{{ url('admin/'.$ID) }}",
  deleteU = "{{ url('admin/'.$ID) }}",
  store = "{{ route('admin.'.$ID.'.store') }}";

  Table.init(ID);

</script>
@endpush