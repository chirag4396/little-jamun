@extends('admin.layouts.master')

@section('title')
Sub Categories
@endsection
@push('header')
<!-- Datatables -->
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = 'sub-category';
@endphp
<script>
  var ID = '{{ $ID }}';
</script>
@endpush

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Categories</h3>
      </div>
      <div class="title_right">
        <button class="btn btn-primary pull-right" onclick="$(this).CRUD({url : '{{ route('admin.'.$ID.'.store') }}',id : '{{ $ID }}'},'addModal');">Add</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">            
            <table id="{{ $ID }}Table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($categories as $k => $c)
                <tr id = "tr-{{ $c->sc_id }}">
                  <td>{{ ++$k }}</td>
                  <td><img src="{{ asset($c->sc_icon_path) }}" class="pro-img"></td>                  
                  <td>{{ $c->sc_name }}</td>
                  <td>{{ $c->category->cat_name }}</td>
                  <td>
                    <a href="javascript:;" class="btn btn-info btn-xs" onclick = "$(this).CRUD({url : '{{ route('admin.'.$ID.'.index') }}',fetchId:'{{ $c->sc_id }}',id : '{{ $ID }}'}, 'edit')"><i class="fa fa-pencil"></i> Edit</a>
                    <a onclick = "CRUD.delete('{{ $ID }}',{{ $c->sc_id }});" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>                    
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan = "5" class="text-center">No {{ ucfirst($ID) }} Found <a class = "btn btn-primary" href="{{ route('admin.'.$ID.'.create') }}">Add some now</a></td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="{{ $ID }}Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="{{ $ID }}ModalLabel">Edit {{ $ID }}</h4>
          </div>
          <div class="modal-body">
            @include('admin.forms.sub_category_form')                        
          </div>          
        </div>
      </div>
    </div>

    <div class="modal fade" id="{{ $ID }}DModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Are You Sure?</h4>
            <br>
            <div>
              <form id = "{{ $ID }}DForm">                
                <button type = "button" class="btn btn-danger" id = "yes">Yes</button>
                <button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
              </form>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="text/javascript">  
  var update = "{{ url('admin/'.$ID) }}",
  deleteU = "{{ url('admin/'.$ID) }}",
  store = "{{ route('admin.'.$ID.'.store') }}";

  Table.init(ID);
  $('#{{ $ID }}Form').CRUD({
    url : '{{ route('admin.'.$ID.'.store') }}',
    processResponse : function (data) {
      if(data.msg == 'success'){
        $('#icon_pathPreview').attr('src', '{{ asset('images/no-image.png') }}');
      }
    }
  });
  // $(this).CRUD({url : 'http://little-jamun/admin/sub-category',fetchId:'12',id : 'sub-category'}, 'edit')
</script>
@endpush