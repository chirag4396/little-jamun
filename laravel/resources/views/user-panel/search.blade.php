@extends('user-panel.layouts.master')
@section('title')
	Search result for {{ $search }}
@endsection
@push('header')
<link href="{{ asset('css/w3.css') }}" rel="stylesheet">
<link href="{{ asset('css/style2.css') }}" rel="stylesheet">	
@endpush
@section('content')
<!--breadcrumbs-->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">{!! 'Searched for <b>"'.$search.'"</b>' !!}</li>
		</ol>
	</div>
</div>
<!--//breadcrumbs-->



<!--products-->
<div class="products">	 
	<div class="container">
		<div class="col-md-12 product-model-sec">
			@php
			$k = 2;
			@endphp
			@forelse (\App\Models\Product::where('pro_title','like',$search.'%')->get() as $pro)
			
			<div class="product-grids {{ ( $k % 3== 0) ? 'product-grids-mdl' : ''}} simpleCart_shelfItem wow fadeInUp animated" data-wow-delay=".5s">
				<div class="new-top">
					<div class="selectProduct w3-padding" data-title="Nexus6P" data-id="Nexus 6P" data-size="5.7&quot;" data-weight="178 g" data-processor="1.55 GHz, Octa Core, Qualcomm Snapdragon 810 " data-battery="3450 mAH">
						<a class="w3-btn-floating w3-light-grey addButtonCircular addToCompare">+</a>
						<img src="{{ asset(isset($pro->display) ? $pro->display->img_thumb_path : '') }}" alt="{{ $pro->pro_title }}" class="img-responsive product-img item_thumb" alt=""/>
						<span class="item_pageLink hidden">{{ route('shop.product',['id' => $pro->pro_id]) }}</span>							
					</div>
					<div class="new-text">
						<ul>
							<li><a href="{{ route('shop.product',['id' => $pro->pro_id]) }}">Quick View </a></li>
							<li><input type="number" class="item_quantity" min="1" value="1"></li>
							<li><a class="item_add" href="javascript:;"> Add to cart</a></li>
						</ul>
					</div>
				</div>
				<div class="new-bottom">
					<h5><a class="name item_name" title="{{ $pro->pro_title }}" href="{{ route('shop.product',['id' => $pro->pro_id]) }}">{{ str_limit($pro->pro_title, 20) }}</a></h5>					
					<div class="ofr">
						@php
						$r = $pro->pro_price;

						if ($pro->pro_discount){
							$r = $r - ($r * ($pro->pro_discount/100));
						}
						@endphp
						@if ($pro->pro_discount)						
						<p class="pric1"><del>&#8377; {{ number_format($pro->pro_price,2) }}</del></p>
						@endif
						<p><span class="item_price">&#8377; {{ number_format($r,2) }}</span></p>
						<div class="item_pro hidden">{{ $pro->pro_id }}</div>
					</div>
				</div>
			</div>
			@php
			$k++;
			@endphp
			@empty
			No Product Found
			@endforelse			
		</div>
		
		<div class="clearfix"> </div>
	</div>
</div>
<!--//products-->



<!--preview panel-->
<div class="w3-container  w3-center">
	<div class="w3-row w3-card-4 w3-grey w3-round-large w3-border comparePanle w3-margin-top">
		<div class="w3-row">
			<div class="w3-col l9 m8 s6 w3-margin-top">
				<h4>Added for comparison</h4>
			</div>
			<div class="w3-col l3 m4 s6 w3-margin-top">
				&nbsp;
				<button class="w3-btn w3-round-small w3-white w3-border notActive cmprBtn" disabled>Compare</button>
			</div>
		</div>
		<div class=" titleMargin w3-container comparePan">
		</div>
	</div>
</div>
<!--end of preview panel-->

<!-- comparision popup-->
<div id="id01" class="w3-animate-zoom w3-white w3-modal modPos">
	<div class="w3-container">
		<a onclick="document.getElementById('id01').style.display='none'" class="whiteFont w3-padding w3-closebtn closeBtn">&times;</a>
	</div>
	<div class="w3-row contentPop w3-margin-top">
	</div>

</div>
<!--end of comparision popup-->

<!--  warning model  -->
<div id="WarningModal" class="w3-modal">
	<div class="w3-modal-content warningModal">
		<header class="w3-container w3-teal">
			<h3><span>&#x26a0;</span>Error</h3>
		</header>
		<div class="w3-container">
			<h4>Maximum of Four products are allowed for comparision</h4>

		</div>
		<footer class="w3-container w3-right-align">
			<button id="warningModalClose" onclick="document.getElementById('id01').style.display='none'" class="w3-btn w3-hexagonBlue w3-margin-bottom  ">Ok</button>
		</footer>
	</div>
</div>
<!--  end of warning model  -->


@endsection

@push('footer')
<script src="{{ asset('js/compare.js') }}"></script>
@endpush