@extends('user-panel.layouts.master')
@section('title')
Checkout
@endsection
@push('header')
@isset ($MERCHANT_KEY)

<script>
	var hash = '{{ $hash }}';	
	function submitPayuForm() {
		if(hash == '') {
			return
		}else{
			$('#loader').show();		
		}
		var payuForm = document.forms.payuForm
		payuForm.submit()
	}
</script>
@endisset

@endpush
@php
$Log = 'login';
@endphp
@section('body')
onload="submitPayuForm();"
@endsection
@section('content')

<!-- checkout content section start -->
<div class="checkout-area">
	<div class="container">
		@guest()		
		<div class="row">
			<div class="col-md-12">
				<div class="coupon-accordion">
					<h3>Returning customer?  <span id="showcoupon3">Click here to login</span></h3>
					<div id="checkout_coupon3" class="coupon-checkout-content">
						<p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
						<div class="coupon-info top1">
							<form class="form-horizontal" id = "{{ $Log }}Form">
								<p class="checkout-coupon top">
									<label class="l-contact">
										Username or email 
										<em>*</em>
									</label>
									<input type="email" name = "email">
								</p>
								<p class="checkout-coupon top-down">
									<label class="l-contact">
										password
										<em>*</em>
									</label>
									<input type="password" name="password">
								</p>
								<div class="cop-left">
									<input class="button-primary" type="submit" value="login">
									<label class="inline">
										<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
									</label>
								</div>
								<p class="lost_password">
									<a href="{{ route('password.request') }}">Lost your password?</a>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endguest

		{{-- <div class="row">
			<div class="col-md-12">
				<div class="coupon-accordion res">
					<h3>Have a coupon? <span id="showcoupon">Click here to enter your code</span></h3>
					<div id="checkout_coupon" class="coupon-checkout-content tnm">
						<div class="coupon-info">
							<form action="#">
								<p class="checkout-coupon res">
									<input type="text" placeholder="Coupon code" />
									<input type="submit" value="Apply Coupon" />
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div> --}}
		<div class="row" {!! !Auth::guest() ? 'style="margin-top: 60px;"' : '' !!}>			
			<div class="col-md-5 col-sm-12">
				<div class="ro-checkout-summary">
					<div class="ro-title">
						<h3 class="checkbox9">ORDER SUMMARY</h3>
					</div>
					@for ($i = 1; $i <= $data['itemCount']; $i++)
					@php					
					$d = [];
					$a = explode(',',$data['item_options_'.$i]);
					foreach ($a as $key => $value) {
						$n = explode(':',$value);
						$d[trim($n[0])] = $n[1];

					}		
					@endphp
					@php						
					$pId = $d['pro'];
					$pro = App\Models\Product::find($pId);
					@endphp
					<div class="ro-body">						
						<div class="ro-item">
							<div class="ro-image">
								<a href="#">
									<img src="{{ asset($pro->display->img_path) }}" alt="">
								</a>
							</div>
							<div>
								<div class="tb-beg">
									<a href="#">{{ $pro->pro_title }}</a>
								</div>
							</div>
							<div>
								<div class="ro-price">
									<span class="amount">&#8377; {{ $data['item_price_'.$i] }}</span>
								</div>
								<div class="ro-quantity">
									<strong class="product-quantity">× {{ $data['item_quantity_'.$i] }}</strong>
								</div>
								<div class="product-total">
									<span class="amount"> &#8377; {{ $data['item_price_'.$i] * $data['item_quantity_'.$i] }}</span>
								</div>
							</div>
							@isset ($d['text'])
							    
							<div style="padding-left: 20px;width: 75%;">
								<span><b>Product Text:</b>{{ $d['text'] }}</span>
							</div>
							@endisset
							
						</div>
						
					</div>
					
					@endfor
					<div class="ro-footer">
						<div>
							<p>
								Subtotal
								<span>
									<span class="amount">{{ '₹'.number_format(preg_replace('/₹|\.00/', '', $data['grand']) - preg_replace('/₹|\.00/', '', $data['shipping']),2) }}</span>
								</span>
							</p>
							<div class="ro-divide"></div>
						</div>
						<div class="shipping">
							<p> Shipping </p>
							<div class="ro-shipping-method">
								<p>	
									{{ $data['shipping'] }}								
								</p>
							</div>
							<div class="clearfix"></div>
							<div class="ro-divide"></div>
						</div>
						<div class="order-total">
							<p>
								Total
								<span>
									<strong>
										<span class="amount">{{ '₹'.number_format(preg_replace('/₹|\.00/', '', $data['grand']),2) }}</span>
									</strong>
								</span>
							</p>
						</div>
						<div>
							<p>
								Payment {{ isset($payment) ? 'recieved' : 'due' }}
								<span>
									<strong>
										<span class="amount">{{ '₹'.number_format(preg_replace('/₹|\.00/', '', $data['grand']),2) }}</span>
									</strong>
								</span>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7 col-sm-12">
				<div class="text">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li id = "homeTab" role="presentation" class="ano {{ isset($MERCHANT_KEY) ? 'active' : '' }} {{ isset($payment) ? 'complete' : '' }}">
							<a href="#home" aria-controls="home" role="tab" data-toggle="tab"></a>
							<span>Address</span>
						</li>
						<li id = "paymentTab" role="presentation" class="ano  {{ isset($payment) ? 'complete' : '' }}">
							<a href="#" {{--  href="#profile" aria-controls="profile" role="tab" data-toggle="tab" --}}></a>
							<span>Payment</span>
						</li>
						<li id = "completeTab" role="presentation" class="ano la {{ isset($payment) ? 'active' :'' }}">
							<a href="#complete" aria-controls="message" role="tab" data-toggle="tab"></a>
							<span>Complete</span>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						@isset ($MERCHANT_KEY)									
						<div role="tabpanel" class="tab-pane {{ $MERCHANT_KEY ? 'active' : '' }}" id="home">
							<div class="row">
								
								<form action="{{ $action }}" method="post" name="payuForm">
									{{ csrf_field() }}
									<input type="hidden" name="key" value="{{ $MERCHANT_KEY }}" />
									<input type="hidden" name="hash" value="{{ $hash }}"/>
									<input type="hidden" name="txnid" value="{{ $txnid }}" />
									<input type="hidden" name="service_provider" value="payu_paisa" size="64" />
									<input required class="form-control"  name="amount" type="hidden"  value="{{ (empty($posted['amount'])) ? preg_replace('/₹|\.00/', '', $data['grand']): $posted['amount'] }}" />


									<input type = "hidden" name="productinfo" value="{{  (empty($posted['productinfo'])) ? 'Brownie' : $posted['productinfo'] }}" size="64" />

									<input type = "hidden" name="surl" value="{{ (empty($posted['surl'])) ? route('shop.orderStatus') : $posted['surl'] }}" size="64" />

									<input type = "hidden" name="furl" value="{{  (empty($posted['furl'])) ? route('shop.orderStatus') : $posted['furl']  }}" size="64" />

									<input type="hidden" name="all" value = "{{ http_build_query($data) }}">

									<div class="checkbox-form">
										<div class="col-md-12">
											<h3 class="checkbox9">SHIPPING ADDRESS DETAILS</h3>
										</div>
										<div class="col-md-12">
											<div class="di-na bs">
												<label class="l-contact">
													First Name 
													<em>*</em>
												</label>
												<input required class="form-control" name="firstname" type="text" id="firstname" value="{{ (empty($posted['firstname'])) ? (Auth::user() ? Auth::user()->name : '') : $posted['firstname'] }}" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="di-na bs">
												<label class="l-contact">
													Email Address 
													<em>*</em>
												</label>
												<input required class="form-control" name="email" type="email" autocomplete="off" id="email" value="{{ (empty($posted['email'])) ? (Auth::user() ? Auth::user()->email : '') : $posted['email'] }}" />
												
											</div>
										</div>
										<div class="col-md-6">
											<div class="di-na bs">
												<label class="l-contact">
													Phone 
													<em>*</em>
												</label>
												<input required id = "phone" class="form-control" type="number" name="phone" value="{{ (empty($posted['phone'])) ? '' : $posted['phone'] }}" />
												
											</div>
										</div>										
										<div class="col-md-12">
											<label class="l-contact">
												Address  
												<em>*</em>
											</label>
											<div class="di-na bs">
												<textarea class="form-control" name="address" required>{{ (Auth::user() ? (Auth::user()->details()->first() ?Auth::user()->details()->first()->ud_address : '') : '') }}</textarea>												
											</div>
										</div>										
										<div class="col-md-6">
											<label class="l-contact">
												Town / City  
												<em>*</em>
											</label>
											<div class="di-na bs">
												<input class="form-control" type="text"  name="city" value = "{{ (Auth::user() ? (Auth::user()->details()->first() ?Auth::user()->details()->first()->ud_city : '') : '') }}">
											</div>
										</div>										
										<div class="col-md-6">
											<div class="di-na bs">
												<label class="l-contact">
													Postcode / ZIP
													<em>*</em>
												</label>
												<input class="form-control" type="text" name="pincode" readonly value = "{{ $data['pincode'] }}">
											</div>
										</div>

										<input type="hidden" name="udf1" value="{{ (empty($posted['udf1'])) ? '' : $posted['udf1'] }}" />

										<input type="hidden" name="udf2" value="{{ (empty($posted['udf2'])) ? '' : $posted['udf2'] }}" />

										<input type="hidden" name="udf3" value="{{ (empty($posted['udf3'])) ? '' : $posted['udf3'] }}" />

										<input type="hidden" name="udf4" value="{{  (empty($posted['udf4'])) ? '' : $posted['udf4'] }}" />

										<input type="hidden" name="udf5" value="{{ (empty($posted['udf5'])) ? '' : $posted['udf5'] }}" />

										<input type="hidden" name="pg" value="{{(empty($posted['pg'])) ? '' : $posted['pg'] }}" type="hidden" />
									</div>
									<div class="col-md-12">
										<label class="l-contact">
											How you want to Pay?
											<em>*</em>
										</label>
										<div class="di-na bs">
											<input type="radio" name="delivery_type" value = "0"> Online
											<input type="radio" name="delivery_type" value = "1"> Cash on Delivery
										</div>
									</div>

									<div class="text-center" style="margin-bottom: 20px;">
										<input id = "proceedBtn" class="btn chk-btn" type="submit" value="Submit">
									</div>

								</form>

								

							</div>
							{{-- <div class="row">
								<div class="col-md-12">
									<h3 class="checkbox9">SHIP TO A DIFFERENT ADDRESS?</h3>
									<div id="showcoupon2">
										<input class="input-checkbox" type="checkbox">
									</div>
								</div>
								<div id="checkout_coupon2" class="coupon-checkout-content2">
									<div class="checkbox-form">
										<div class="col-md-6">
											<div class="di-na bs">
												<label class="l-contact">
													First Name 
													<em>*</em>
												</label>
												<input class="form-control" type="text"  name="name">
											</div>
										</div>
										<div class="col-md-6">
											<div class="di-na bs">
												<label class="l-contact">
													Last Name 
													<em>*</em>
												</label>
												<input class="form-control" type="text"  name="name">
											</div>
										</div>
										<div class="col-md-12">
											<div class="di-na bs">
												<label class="l-contact">
													Company Name
													<em>*</em>
												</label>
												<input class="form-control" type="text"  name="name">
											</div>
										</div>
										<div class="col-md-6">
											<div class="di-na bs">
												<label class="l-contact">
													Email Address 
													<em>*</em>
												</label>
												<input class="form-control" type="email"  name="name">
											</div>
										</div>
										<div class="col-md-6">
											<div class="di-na bs">
												<label class="l-contact">
													Phone 
													<em>*</em>
												</label>
												<input class="form-control" type="tel"  name="name">
											</div>
										</div>
										<div class="col-md-12">
											<div class="country-select">
												<label class="l-contact">
													Country 
													<em>*</em>
												</label>
												<select class="email s-email s-wid">
													<option>Bangladesh</option>
													<option>Albania</option>
													<option>Åland Islands</option>
													<option>Afghanistan</option>
													<option>Belgium</option>
												</select>
											</div>
										</div>
										<div class="col-md-12">
											<label class="l-contact">
												Address  
												<em>*</em>
											</label>
											<div class="di-na bs">
												<input class="form-control" type="text"  name="name"  placeholder="Street address">
											</div>
										</div>
										<div class="col-md-12">
											<div class="di-na bs tana">
												<input class="form-control" type="text"  name="name" placeholder="Apartment, suite, unit etc. (optional)">
											</div>
										</div>
										<div class="col-md-12">
											<label class="l-contact">
												Town / City  
												<em>*</em>
											</label>
											<div class="di-na bs">
												<input class="form-control" type="text"  name="name">
											</div>
										</div>
										<div class="col-md-6">
											<div class="country-select">
												<label class="l-contact">
													District 
													<em>*</em>
												</label>
												<select class="email s-email s-wid">
													<option>mymensingh</option>
													<option>dhaka</option>
													<option>khulna</option>
													<option>kumillah</option>
													<option>chadpur</option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="di-na bs">
												<label class="l-contact">
													Postcode / ZIP
													<em>*</em>
												</label>
												<input class="form-control" type="text"  name="name">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="di-na bs">
										<label class="l-contact">
											Order Notes
										</label>
										<textarea class="input-text " placeholder="Notes about your order, e.g. special notes for delivery." name="order_comments"></textarea>
									</div>
								</div>
								<div class="col-md-12 text-center">
									<p class="checkout-coupon">
										<input type="submit" value="CONTINUE">
									</p>
								</div>

							</div> --}}
						</div>
						@endisset
						{{-- {{ dd($payment) }} --}}
						{{-- <div role="tabpanel" class="tab-pane" id="profile">
							<div class="row">
								<div class="col-md-12">
									<div class="top-check-text">
										<div class="check-down">
											<h3 class="checkbox9">INFORMATION</h3>
											<span><a class="ro-edit-customer-info" href="#">Edit</a></span>
										</div>
										<div class="ro-content2">
											<div class="ro-info2">
												<p>
													<span>Email Address: </span>
													tasnimakter903@yahoo.com
												</p>
											</div>
											<div class="ro-info2">
												<p>
													<span>Country: </span>
													BD
												</p>
											</div>
										</div>
									</div>
									<form action="#" class="all-payment">
										<div class="all-paymet-border">
											<div class="payment-method">
												<div class="pay-top sin-payment">
													<input id="payment_method_1" class="input-radio" type="radio" value="cheque" checked="checked" name="payment_method">
													<label for="payment_method_1"> Direct Bank Transfer </label>
													<div class="payment_box payment_method_bacs">
														<p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
													</div>
												</div>
												<div class="pay-top sin-payment">
													<input id="payment_method_2" class="input-radio" type="radio" value="cheque" name="payment_method">
													<label for="payment_method_2"> Cheque Payment  </label>
													<div class="payment_box payment_method_bacs">
														<p>Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
													</div>
												</div>
												<div class="pay-top sin-payment">
													<input id="payment_method_3" class="input-radio" type="radio" value="cheque" name="payment_method">
													<label for="payment_method_3">PayPal <img alt="" src="img/icon-img/44.png"><a href="#">What is PayPal?</a></label>
													<div class="payment_box payment_method_bacs">
														<p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
													</div>
												</div>
											</div>
											<div class="form-row place-order">
												<input class="button alt" type="submit" value="Place order" >
											</div>
										</div>
									</form>
								</div>
							</div>
						</div> --}}

						@isset($payment)
						<div role="tabpanel" class="tab-pane mymsg {{ $payment ? 'active' : '' }}" id="complete">
							<div class="last-check">							
								@if ($payment['status'] == 'success')								
								<div class="alert">								 
									<strong>Thank you for your purchase!</strong>								
									<p>We recieved your payment successfully, we'll deliver your order to given address soon, for any query please contact us at {{ Config::get('app.landline') }} or {{ Config::get('app.support_email') }}</p>
								</div>
								@endif
								@if ($payment['status'] == 'failure')
								<div class="alert fail">								  
									<strong>Order Failure!</strong> Please Try again or we'll contact to you shortly or please feel free to contact us at <b>{{ config('app.landline') }}</b> or <b>{{ config('app.support_email') }}</b>
								</div>
								@endif
							</div>
						</div>
						@endisset
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- checkout  content section end --> 
@endsection

@push('footer')
<script type="text/javascript">
	CRUD.formSubmission({
		url : "{{ url($Log) }}", 
		type : 0,
		id : '{{ $Log }}Form',
		onLoad: 'check',
		proData : function(data){
			console.log(data);
			// if(data.msg == 'successLogin'){
			// 	location.reload();
			// }
		}
	});
	@isset($payment)
	simpleCart.empty();
	@endif
</script>
{{--add particular Jqueries or scripts tag here of particular page--}}
@endpush