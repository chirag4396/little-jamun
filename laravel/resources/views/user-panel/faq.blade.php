@extends('user-panel.layouts.master')
@section('title')
	Frequently asked Questions
@endsection

@section('content')
<!--breadcrumbs-->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">FAQ</li>
		</ol>
	</div>
</div>
<!--//breadcrumbs-->
<!--faq-->
<div class="faq-info">
	<div class="container">
		<div class="title-info">
			<h3 class="title">Frequently Asked<span> Questions</span></h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur </p>
		</div>
		<ul class="faq">
			@forelse ($faqs as $k => $faq)			
			<li class="item{{ ++$k }} wow fadeInDown animated" data-wow-delay=".5s"><a href="#">{{ $faq->faq_qus }}<span class="icon"> </span></a>
				<ul>
					<li class="subitem1"><p>{{ $faq->faq_ans }}</p></li>										
				</ul>
			</li>
			@empty
				
			@endforelse	
		</ul>
		<!-- script for tabs -->
		<script type="text/javascript">
			$(function() {
			
				var menu_ul = $('.faq > li > ul'),
					   menu_a  = $('.faq > li > a');
				
				menu_ul.hide();
			
				menu_a.click(function(e) {
					e.preventDefault();
					if(!$(this).hasClass('active')) {
						menu_a.removeClass('active');
						menu_ul.filter(':visible').slideUp('normal');
						$(this).addClass('active').next().stop(true,true).slideDown('normal');
					} else {
						$(this).removeClass('active');
						$(this).next().stop(true,true).slideUp('normal');
					}
				});
			
			});
		</script>
		<!-- script for tabs -->
	</div>			
</div>			
<!--//faq-->
@endsection

@push('footer')
<script src="{{ asset('js/compare.js') }}"></script>
@endpush