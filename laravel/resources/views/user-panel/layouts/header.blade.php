<!DOCTYPE html>
<html>
<head>
	<title>Little Jamun | @yield('title') </title>
	<!-- for-mobile-apps -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Modern Shoppe for Kids" />
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/fav.png') }}">	
	<script type="application/x-javascript"> 
		addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } 
	</script>
	<style type="text/css">
	.loader{
		display: none;
		color: white;  
		height: 1000px;
		font-family: initial;
		position: fixed;
		top: 0;
		font-size: 36px;
		z-index: 9999;
		background: rgba(0, 0, 0, 0.88);
		width: 100%;
		text-align: center;
		padding-top: 250px;
	}
	#loader2{
		display: block;            
		background: #2b0039 !important;
	}
</style>
<!--//for-mobile-apps -->
<!--Custom Theme files -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css" media="screen" />

<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

<!--//Custom Theme files -->
<!--js-->
<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('js/modernizr.custom.js') }}"></script>
<!--//js-->
<!--cart-->
<script src="{{ asset('js/simpleCart.min.js') }}"></script>
<!--cart-->
<!--web-fonts-->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'><link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Pompiere' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Fascinate' rel='stylesheet' type='text/css'>
<!--web-fonts-->
<!--animation-effect-->
<link href="{{ asset('css/animate.min.css') }}" rel="stylesheet"> 
<script src="{{ asset('js/wow.min.js') }}"></script>
<script>
	new WOW().init();
</script>
<!--//animation-effect-->
<!--start-smooth-scrolling-->
<script type="text/javascript" src="{{ asset('js/move-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/easing.js') }}"></script>	
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!--//end-smooth-scrolling-->
@stack('header')
</head>
<body @yield('body')>
	<div class="loader" id = "loader2">
		<img src="{{ asset('images/logo.png') }}" width = "300">
	</div>
	<div class="loader" id = "loader">
		<div>Taking you to Payment Portal, please wait.</div>
	</div>
	<!--header-->
	<div class="header">
		<div class="top-header navbar navbar-default"><!--header-one-->
			<div class="container">
				<div class="nav navbar-nav wow fadeInLeft animated" data-wow-delay=".5s">
					@if (Auth::guest())
					<p>Welcome to {{ config('app.name') }} <a href="{{ route('register') }}">Register </a> Or <a href="{{ route('login') }}">Sign In</a></p>

					@else
					<p>Welcome to {{ config('app.name') }} {{ Auth::user()->name }}
					<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log Out <i class="fa fa-sign-out"></i> 
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>     
					</p>
					@endif
				</div>
				<div class="nav navbar-nav navbar-right social-icons wow fadeInRight animated" data-wow-delay=".5s">
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#" class="in"> </a></li>
						<li><a href="#" class="you"> </a></li>						
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="header-two navbar navbar-default"><!--header-two-->
			<div class="container">
				<div class="nav navbar-nav header-two-left">
					<ul>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+9881100080</li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:littlejamun@gmail.com">littlejamun@gmail.com</a></li>			
					</ul>
				</div>
				<div class="nav navbar-nav logo wow zoomIn animated" data-wow-delay=".7s">
					<a href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" width="250px"></a>
				</div>
				<div class="nav navbar-nav navbar-right header-two-right">
					<div class="header-right my-account">
						<a href="{{ route('shop.contact') }}"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> CONTACT US</a>						
					</div>
					<div class="header-right cart">
						<a href="#"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a>
						<h4><a href="{{ route('shop.cart') }}">
							<span class="simpleCart_total"> $0.00 </span> (<span id="simpleCart_quantity" class="simpleCart_quantity"> 0 </span>) 
						</a></h4>
						<div class="cart-box">
							<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="top-nav navbar navbar-default"><!--header-three-->
			<div class="">
				<nav class="navbar" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<!--navbar-header-->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav top-nav-info">
							<li><a href="{{ route('home') }}" class="{{ $catId or 'active' }}">Home</a></li>
							@php
							$cats = App\Models\Category::get();
							@endphp
							@if ($cats->count())
							@forelse ($cats as $cat)
							@if ($cat->subCategory->count())

							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" class="{{ isset($catId) ? ($catId == $cat->cat_id ? 'active' : '') : '' }}" >{{ $cat->cat_name }}<b class="caret"></b></a>
								<ul class="dropdown-menu multi-column multi-column1">
									@forelse ($cat->subCategory()->get() as $scat)
									<li><a class="list" href="{{ route('shop.products',['cat' => $cat->cat_id, 'sub' => $scat->sc_id]) }}">{{ $scat->sc_name }}</a></li>
									@empty										
									@endforelse
								</ul>
							</li>	
							@else
							<li><a href="{{ route('shop.products',['cat' => $cat->cat_id]) }}"  class="{{ isset($catId) ? ($catId == $cat->cat_id ? 'active' : '') : '' }}">{{ $cat->cat_name }}</a></li>
							@endif
							@empty
							@endforelse
							
							@endif
							
							{{-- <li><a href="codes.html">Special Offers</a></li> --}}
						</ul> 
						<div class="clearfix"> </div>
						<!--//navbar-collapse-->
						<header class="cd-main-header">
							<ul class="cd-header-buttons">
								<li><a class="cd-search-trigger" href="#cd-search"> <span></span></a></li>
							</ul> <!-- cd-header-buttons -->
						</header>
					</div>
					<!--//navbar-header-->
				</nav>
				<div id="cd-search" class="cd-search">
					<form method="get" action="{{ route('shop.search') }}">
						<input type="search" placeholder="Search..." name="search">
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--//header-->