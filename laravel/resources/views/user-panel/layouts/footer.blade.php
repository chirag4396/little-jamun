<!--footer-->
<div class="footer">
	<div class="container">
		<div class="footer-info">
			<div class="col-md-4 footer-grids wow fadeInUp animated" data-wow-delay=".5s">
				<h4 class="footer-logo"><a href="index.html">Little <b>Jamun</b> <span class="tag">Everything for Kids world </span> </a></h4>
				<p>© 2018  Little Jamun Modern Shoppe for Kids . All rights reserved | Design by <a href="http://sungare.com" target="_blank">Sungare Technologies
				</p>
			</div>
			<div class="col-md-4 footer-grids wow fadeInUp animated" data-wow-delay=".7s">
				<h3>Popular</h3>
				<ul>
					<li><a href="{{ route('shop.about') }}">About Us</a></li>
					<li><a href="{{ route('shop.contact') }}">Contact Us</a></li>
					<li><a href="{{ route('shop.faq') }}">FAQ</a></li>
				</ul>
			</div>
			<div class="col-md-4 footer-grids wow fadeInUp animated" data-wow-delay=".9s">
				<h3>Subscribe</h3>
				<p>Sign Up Now For More Information <br> About Our Company </p>
				<form>
					<input type="text" placeholder="Enter Your Email" required="">
					<input type="submit" value="Go">
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--//footer-->		
<!--search jQuery-->
<script src="{{ asset('js/main.js') }}"></script>
<!--//search jQuery-->
<!--smooth-scrolling-of-move-up-->
<script type="text/javascript">
	$(document).ready(function() {
		var defaults = {
			containerID: 'toTop',
			containerHoverID: 'toTopHover',
			scrollSpeed: 1200,
			easingType: 'linear' 
		};

		$().UItoTop({ easingType: 'easeOutQuart' });

	});
	simpleCart.currency({
		code: "INR" ,
		symbol: "&#8377;" ,
		name: "Indian Rupees"
	});
	simpleCart.bind('afterAdd', function(item){
		$('html, body').animate({scrollTop : 0},600);    
		$('#cartBox').addClass('blink_me');
		window.setTimeout(function(){
			$('#cartBox').removeClass('blink_me');
		},2000);
	});
	$(window).on('load',function(){
		$('#loader2').addClass('hidden');
	});

	simpleCart.bind( 'update' , function(item){		

		var button = $('.simpleCart_checkout');
		button.hide();       

		var quantity = simpleCart.quantity();

		if(quantity > 0){
			button.show();
		}

		if(quantity == 0){
			button.hide();
		}
	}); 
	// simpleCart.bind( 'beforeAdd' , function( item ){
		// console.log(simpleCart.quantity());
		// if(simpleCart.quantity() > 10 ){
		// 	alert("The maximum number of items per order is 10. Please remove some items and try again.");
		// 	return false;
		// }
	// });
</script>
<!--//smooth-scrolling-of-move-up-->
<!--Bootstrap core JavaScript
	================================================== -->
	<!--Placed at the end of the document so the pages load faster -->
	<script src="{{ asset('js/bootstrap.js') }}"></script>
	<script src="{{ asset('admin-assets/js/custom/common.js') }}"></script>
	@stack('footer')
</body>
</html>