@extends('user-panel.layouts.master')
@section('title')
Home
@endsection
@push('header')

@endpush
@section('content')
<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1366px;height:450px;overflow:hidden;visibility:hidden;background: #000000b3;">
	<!-- Loading Screen -->
	<div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
		<img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
	</div>
	<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1366px;height:450px;overflow:hidden;">
		@forelse (\App\Models\Banner::get() as $b)								

		<div data-p="227.00">
			<img data-u="image" class = "jssor-img" src="{{ asset($b->ban_img_path) }}" />
		</div>
		@empty								
		@endforelse	        
	</div>
	<!-- Bullet Navigator -->
	<div data-u="navigator" class="jssorb053" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
		<div data-u="prototype" class="i" style="width:16px;height:16px;">
			<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
				<path class="b" d="M11400,13800H4600c-1320,0-2400-1080-2400-2400V4600c0-1320,1080-2400,2400-2400h6800 c1320,0,2400,1080,2400,2400v6800C13800,12720,12720,13800,11400,13800z"></path>
			</svg>
		</div>
	</div>
	<!-- Arrow Navigator -->
	<div data-u="arrowleft" class="jssora093" style="width:50px;height:50px;top:0px;left:30px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
		<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
			<circle class="c" cx="8000" cy="8000" r="5920"></circle>
			<polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
			<line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
		</svg>
	</div>
	<div data-u="arrowright" class="jssora093" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
		<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
			<circle class="c" cx="8000" cy="8000" r="5920"></circle>
			<polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
			<line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
		</svg>
	</div>
</div>
{{-- <!--banner-->
<div class="banner" style="background:url(../images/1.jpg) no-repeat 0px 0px;">
	<div class="container">
		<div class="banner-text">			
			<div class="col-sm-5 banner-left wow fadeInLeft animated" data-wow-delay=".5s" >
				

			</div>
			<div class="col-sm-7 banner-right wow fadeInRight animated" data-wow-delay=".5s">			
				<section class="slider grid">
					<div class="flexslider">
						<ul class="slides">
							@forelse (\App\Models\Banner::get() as $b)								
							<li>
								<h1>{{ $b->ban_title }}</h1>
								<h4>{{ $b->ban_sub_title }}</h4>
								<img src="{{ asset($b->ban_img_path) }}" alt="">
							</li>
							@empty								
							@endforelse							
						</ul>
					</div>
				</section>
				<!--FlexSlider-->
				<script defer src="{{ asset('js/jquery.flexslider.js') }}"></script>
				<script type="text/javascript">
					$(window).load(function(){
						$('.flexslider').flexslider({
							animation: "pagination",
							start: function(slider){
								$('body').removeClass('loading');
							}
						});
					});
				</script>
				<!--End-slider-script-->
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>			
<!--//banner--> --}}
<!--new-->
<div class="new">
	<div class="container">
		<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
			<h3 class="title">Popular <span>Categories</span></h3>
			{{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur </p> --}}
		</div>
		<div class="new-info">
			<!-- collection section start -->
			<div class="banner-area">
				<div class="container">
					<div class="section-padding1">
						<div class="row">
							@forelse (\App\Models\Category::get() as $k => $c)
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="banner-box banner-box-re">
									@if ($c->subCategory->count())
									<a href="{{ route('shop.our-menu',['catId' => $c->cat_id]) }}">
										@else
										<a href="{{ route('shop.products',['cat' => $c->cat_id]) }}">
											@endif
											<img alt="{{ $c->cat_name }}" src="{{ asset($c->cat_icon_path) }}">
											<div>
												<h2>
													{{ $c->cat_name }}
												</h2>
											</div>
										</a>
									</div>								
								</div>
								@empty
								
								@endforelse							
							</div>
						</div>
					</div>
				</div>
				<!-- collection section end -->
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>		
	<!--//new-->
	<!--gallery-->	
	<!--//gallery-->
	<!--trend-->
	<!--trend-->
	{{-- <div class="trend wow zoomIn animated" data-wow-delay=".5s">
		<div class="container">
			<div class="trend-info">
				<section class="slider grid">
					<div class="flexslider trend-slider">
						<ul class="slides">
							<li>
								<div class="col-md-5 trend-left">
									<img src="images/t1.png" alt=""/>
								</div>
								<div class="col-md-7 trend-right">
									<h4>TOP 10 TRENDS <span>FOR YOU</span></h4>
									<h5>Flat 20% OFF</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus, justo ac volutpat vestibulum, dolor massa pharetra nunc, nec facilisis lectus nulla a tortor. Duis ultrices nunc a nisi malesuada suscipit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam eu bibendum felis. Sed viverra dapibus tincidunt.</p>
								</div>
								<div class="clearfix"></div>
							</li>
							<li>
								<div class="col-md-5 trend-left">
									<img src="images/t2.png" alt=""/>
								</div>
								<div class="col-md-7 trend-right">
									<h4>TOP 10 TRENDS <span>FOR YOU</span></h4>
									<h5>Flat 20% OFF</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus, justo ac volutpat vestibulum, dolor massa pharetra nunc, nec facilisis lectus nulla a tortor. Duis ultrices nunc a nisi malesuada suscipit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam eu bibendum felis. Sed viverra dapibus tincidunt.</p>
								</div>
								<div class="clearfix"></div>
							</li>
							<li>
								<div class="col-md-5 trend-left">
									<img src="images/t3.png" alt=""/>
								</div>
								<div class="col-md-7 trend-right">
									<h4>TOP 10 TRENDS <span>FOR YOU</span></h4>
									<h5>Flat 20% OFF</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus, justo ac volutpat vestibulum, dolor massa pharetra nunc, nec facilisis lectus nulla a tortor. Duis ultrices nunc a nisi malesuada suscipit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam eu bibendum felis. Sed viverra dapibus tincidunt.</p>
								</div>
								<div class="clearfix"></div>
							</li>
							<li>
								<div class="col-md-5 trend-left">
									<img src="images/t4.png" alt=""/>
								</div>
								<div class="col-md-7 trend-right">
									<h4>TOP 10 TRENDS <span>FOR YOU</span></h4>
									<h5>Flat 20% OFF</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus, justo ac volutpat vestibulum, dolor massa pharetra nunc, nec facilisis lectus nulla a tortor. Duis ultrices nunc a nisi malesuada suscipit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam eu bibendum felis. Sed viverra dapibus tincidunt.</p>
								</div>
								<div class="clearfix"></div>
							</li>
						</ul>
					</div>
				</section>
			</div>
		</div>
	</div> --}}
	<!--//trend-->
	<div class="trend wow zoomIn animated" data-wow-delay=".5s">
		<div class="container">
			<div class="trend-info">
				<section class="slider grid">
					<div class="flexslider trend-slider">
						<ul class="slides">
							@forelse (\App\Models\SecondBanner::get() as $sb)
							<li>
								<div class="col-md-5 trend-left">
									<img src="{{ asset($sb->sb_img) }}" alt="{{ $sb->sb_title }}"/>
								</div>
								<div class="col-md-7 trend-right">
									<h4>{{ $sb->sb_title }}</h4>
									<h5>{{ $sb->sb_sub_title }}</h5>
									<p>{{ $sb->sb_description }}</p>
								</div>
								<div class="clearfix"></div>
							</li>
							@empty

							@endforelse						
						</ul>
					</div>
				</section>
			</div>
		</div>
	</div>
	<!--//trend-->
	@endsection

	@push('footer')
	<script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		jssor_1_slider_init = function() {

			var jssor_1_SlideshowTransitions = [
			{$Duration:500,$Delay:12,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2049,$Easing:$Jease$.$OutQuad},
			{$Duration:500,$Delay:40,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$SlideOut:true,$Easing:$Jease$.$OutQuad},
			{$Duration:1000,x:-0.2,$Delay:20,$Cols:16,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Assembly:260,$Easing:{$Left:$Jease$.$InOutExpo,$Opacity:$Jease$.$InOutQuad},$Opacity:2,$Outside:true,$Round:{$Top:0.5}},
			{$Duration:1600,y:-1,$Delay:40,$Cols:24,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:$Jease$.$OutJump,$Round:{$Top:1.5}},
			{$Duration:1200,x:0.2,y:-0.1,$Delay:16,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InWave,$Top:$Jease$.$InWave,$Clip:$Jease$.$OutQuad},$Round:{$Left:1.3,$Top:2.5}},
			{$Duration:1500,x:0.3,y:-0.3,$Delay:20,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$During:{$Left:[0.2,0.8],$Top:[0.2,0.8]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InJump,$Top:$Jease$.$InJump,$Clip:$Jease$.$OutQuad},$Round:{$Left:0.8,$Top:2.5}},
			{$Duration:1500,x:0.3,y:-0.3,$Delay:20,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$During:{$Left:[0.1,0.9],$Top:[0.1,0.9]},$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InJump,$Top:$Jease$.$InJump,$Clip:$Jease$.$OutQuad},$Round:{$Left:0.8,$Top:2.5}}
			];

			var jssor_1_options = {
				$AutoPlay: 1,
				$FillMode: 1,
				$SlideshowOptions: {
					$Class: $JssorSlideshowRunner$,
					$Transitions: jssor_1_SlideshowTransitions,
					$TransitionsOrder: 1
				},
				$ArrowNavigatorOptions: {
					$Class: $JssorArrowNavigator$
				},
				$BulletNavigatorOptions: {
					$Class: $JssorBulletNavigator$
				}
			};

			var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

			/*#region responsive code begin*/

			var MAX_WIDTH = 1366;

			function ScaleSlider() {
				var containerElement = jssor_1_slider.$Elmt.parentNode;
				var containerWidth = containerElement.clientWidth;

				if (containerWidth) {

					var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

					jssor_1_slider.$ScaleWidth(expectedWidth);
				}
				else {
					window.setTimeout(ScaleSlider, 30);
				}
			}

			ScaleSlider();

			$Jssor$.$AddEvent(window, "load", ScaleSlider);
			$Jssor$.$AddEvent(window, "resize", ScaleSlider);
			$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
			/*#endregion responsive code end*/
		};
	</script>
	<script type="text/javascript">jssor_1_slider_init();</script>
	<script defer src="{{ asset('js/jquery.flexslider.js') }}"></script>
	<script type="text/javascript">
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "pagination",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	</script>
	@endpush