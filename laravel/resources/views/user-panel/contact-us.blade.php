@extends('user-panel.layouts.master')
@section('title')
	Contact Us
@endsection
@push('header')
	@php
		$ID = "enquiry";
	@endphp
	<script type="text/javascript">
		var ID = '{{ $ID }}';
	</script>
@endpush
@section('content')
<!--breadcrumbs-->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">Contact Us</li>
		</ol>
	</div>
</div>
<!--//breadcrumbs-->
<!--contact-->
<div class="contact">
	<div class="container">
		<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
			<h3 class="title">How To <span> Find Us</span></h3>
		</div>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3783.8525568654604!2d73.85161691520125!3d18.49033667492332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c01b4c7f5f83%3A0xd15f6ec56a1be851!2sNescraft+Systems!5e0!3m2!1sen!2sin!4v1525895557204" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>	
</div>
<div class="address"><!--address-->
	<div class="container">
		<div class="address-row">
			<div class="col-md-6 address-left wow fadeInLeft animated" data-wow-delay=".5s">
				<div class="address-grid">
					<h4 class="wow fadeIndown animated" data-wow-delay=".5s">DROP US A LINE </h4>
					<form id = "{{ $ID }}Form">
						<input type="hidden" name="type" value="101">						
						<input name="name" id = "name" class="wow fadeIndown animated" data-wow-delay=".6s" type="text" placeholder="Name" required="">
						<input name="email" id = "email" class="wow fadeIndown animated" data-wow-delay=".7s" type="text" placeholder="Email" required="">
						<input name="mobile" id = "mobile" class="wow fadeIndown animated" data-wow-delay=".8s" type="text" placeholder="Mobile" required="">
						<textarea name="query" class="wow fadeIndown animated" data-wow-delay=".8s" placeholder="Message" required=""></textarea>
						<input class="wow fadeIndown animated" data-wow-delay=".9s" type="submit" value="SEND">
					</form>
				</div>
			</div>
			<div class="col-md-6 address-right">
				<div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
					<h4>ADDRESS</h4>
					<p>
						Nescraft Systems,
						48A, Parvati Industrial Estate,
						Opp. Adinath Society,
						Pune-Satara Road,
						Pune-411009.</p>
				</div>
				<div class="address-info address-mdl wow fadeInRight animated" data-wow-delay=".7s">
					<h4>PHONE </h4>
					<p>+91 9881100080</p>
				</div>
				<div class="address-info wow fadeInRight animated" data-wow-delay=".6s">
					<h4>MAIL</h4>
					<p><a href="mailto:littlejamun@gmail.com"> littlejamun@gmail.com</a></p>
				</div>
			</div>
		</div>	
	</div>	
</div>
<!--//contact-->	
@endsection

@push('footer')
	<script type="text/javascript">
		CRUD.formSubmission({
			url : '{{ route('shop.enquiry.store') }}',
			id : 'enquiryForm',
			type : 0,
			validate : {'mobile' : 'mobile' , 'alphaSpace' : 'name', 'email' : 'email'}
		});
		// 
	</script>
@endpush