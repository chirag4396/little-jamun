@extends('user-panel.layouts.master')
@section('title')
{{ \App\Models\Category::find($catId)->cat_name }} Products
@endsection
@push('header')
<link href="{{ asset('css/w3.css') }}" rel="stylesheet">
<link href="{{ asset('css/style2.css') }}" rel="stylesheet">	
@endpush
@section('content')
<!--breadcrumbs-->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">Products</li>
		</ol>
	</div>
</div>
<!--//breadcrumbs-->



<!--products-->
<div class="products">	 
	<div class="container">
		<div class="col-md-12 product-model-sec">
			@php
			$k = 2;
			@endphp
			@forelse (\App\Models\SubCategory::where('sc_cat_id', $catId)->get() as $sub)
			
			<div class="product-grids {{ ( $k % 3== 0) ? 'product-grids-mdl' : ''}} simpleCart_shelfItem wow fadeInUp animated" data-wow-delay=".5s">
				<div class="new-top">
					<div class="selectProduct w3-padding" data-title="Nexus6P" data-id="Nexus 6P" data-size="5.7&quot;" data-weight="178 g" data-processor="1.55 GHz, Octa Core, Qualcomm Snapdragon 810 " data-battery="3450 mAH">
						<a href="{{ route('shop.products', ['sub' => $sub->sc_id, 'cat' => $catId]) }}">
							<img src="{{ asset($sub->sc_icon_path) }}" alt="{{ $sub->sc_name }}" class="img-responsive product-img item_thumb"/>							
						</a>						
					</div>					
				</div>
				<div class="new-bottom">
					<h5><a class="name item_name" title="{{ $sub->sc_name }}" href="{{ route('shop.products', ['sub' => $sub->sc_id, 'cat' => $catId]) }}">{{ str_limit($sub->sc_name, 20) }}</a></h5>					
				</div>
			</div>
			@php
			$k++;
			@endphp
			@empty
			No Sub Categories Found
			@endforelse			
		</div>
		
		<div class="clearfix"> </div>
	</div>
</div>
<!--//products-->
@endsection

@push('footer')
<script src="{{ asset('js/compare.js') }}"></script>
@endpush