@extends('user-panel.layouts.master')
@section('title')
	Login
@endsection
@push('header')
<link href="{{ asset('css/w3.css') }}" rel="stylesheet">
<link href="{{ asset('css/style2.css') }}" rel="stylesheet">	
@endpush
@section('content')
<!--breadcrumbs-->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow fadeInUp" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">Sign In</li>
		</ol>
	</div>
</div>
<!--//breadcrumbs-->
<!--login-->
<div class="login-page">
	<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
		<h3 class="title">SignIn<span> Form</span></h3>
		{{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur </p> --}}
	</div>
	<div class="widget-shadow">
		<div class="login-top wow fadeInUp animated" data-wow-delay=".7s">
			<h4>Welcome back to {{ config('app.name') }} ! <br> Not a Member? <a href="{{ route('register') }}">  Register Now »</a> </h4>
		</div>
		<div class="login-body wow fadeInUp animated" data-wow-delay=".7s">
			<form method="POST" action="{{ route('login') }}">
				@csrf
				<input id="email" type="text" class="user{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Enter your email" >

				@if ($errors->has('email'))
				    <span class="invalid-feedback">
				        <strong>{{ $errors->first('email') }}</strong>
				    </span>
				@endif
				
				<input id="password" type="password" class="lock{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

				@if ($errors->has('password'))
				    <span class="invalid-feedback">
				        <strong>{{ $errors->first('password') }}</strong>
				    </span>
				@endif
				<input type="submit" name="Sign In" value="Sign In">
				<div class="forgot-grid">
					<label class="checkbox"><input type="checkbox" name="checkbox" {{ old('remember') ? 'checked' : '' }}><i></i>Remember me</label>
					<div class="forgot">
						<a href="{{ route('password.request') }}">Forgot Password?</a>
					</div>
					<div class="clearfix"> </div>
				</div>
			</form>
		</div>
	</div>
	{{-- <div class="login-page-bottom">
		<h5> - OR -</h5>
		<div class="social-btn"><a href="#"><i>Sign In with Facebook</i></a></div>
		<div class="social-btn sb-two"><a href="#"><i>Sign In with Twitter</i></a></div>
	</div> --}}
</div>
<!--//login-->
@endsection

@push('footer')
<script src="{{ asset('js/compare.js') }}"></script>
@endpush