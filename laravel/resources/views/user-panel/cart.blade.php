@extends('user-panel.layouts.master')
@section('title')
	Cart
@endsection
@section('content')
<!--breadcrumbs-->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">Check Out page</li>
		</ol>
	</div>
</div>
<!--//breadcrumbs-->
<!--cart-items-->
<div class="cart-items">
	<div class="container">
		<h3 class="wow fadeInUp animated" data-wow-delay=".5s">Shopping Cart Details</h3>
		<div class="simpleCart_items table-content table-responsive"></div>
		<div class="row">
			<div class="col-md-8 col-sm-7 col-xs-12">						
				<div class="coupon">
					<h3></h3>
					<p>Enter your pin code, for delivery.</p>
					<form id = "pinForm">								
						<input type="text" placeholder="Pin Code" autocomplete="off" autocomplete = "off" name = "pincode" id = "pincode" />
						<input type="submit" value="Check Availability" />
					</form>
					<div class="clearfix"></div>
					<p id = "pinMsg" class="hidden"></p>
					<p id = "pinStatus" class="hidden">0</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-5 col-xs-12">
				<div class="cart_totals">
					<h2>Cart Totals</h2>
					<div class="clearfix"></div>
					<table>
						<tbody>
							<tr class="cart-subtotal">
								<th>Subtotal</th>
								<td><span class="amount simpleCart_total"></span></td>
							</tr>
							<tr class="shipping">
								<th>Shipping</th>
								<td>												
									<span class="simpleCart_shipping"></span>
								</td>
							</tr>
							<tr class="order-total">
								<th>Total</th>
								<td>
									<strong><span class="amount simpleCart_grandTotal" id = "finalGrand">&#8377;</span></strong>
									{{-- <span class="simpleCart_grandTotal hidden" id = "grand"></span> --}}
								</td>
							</tr>											
						</tbody>
					</table>
					<div class="wc-proceed-to-checkout">
						<a href="javascript:;" class="simpleCart_checkout">Proceed to Checkout</a>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!--//cart-items-->	
@endsection

@push('footer')
<script type="text/javascript">		
	simpleCart({	    
		shippingFlatRate:0,
		cartStyle: "table",
		cartColumns: [
		{ view: function( item, column) {
			// console.log(column);
			return '<a href="'+item.get('pagelink')+'"><img src="'+item.get('thumb')+'" alt="" width = "100" class = "cart-img"/></a>';			
		}, label: 'Image', attr:'thumb' },		
		{ attr: "name", label: "Product Name" , view : function(item, column){
			return '<span class ="">'+item.get('name')+'</span>';
		}},
		{ label: 'Text' , view: function(item, column){			
			return (typeof item.get('text') !== "undefined") ? '"'+item.get('text')+'"' : "-" ;
		}},
		{ attr: "price" , label: "Price", view: function(item, column){			
			return '<span class="amount"> &#8377;'+item.get('price')+'</span>';
		}},
		{ attr: "quantity" , label: "Qty" , className : "qty-width", view: function(item, column){
			return '<a href="javascript:;" class="simpleCart_decrement"><i class = "fa fa-minus"></i></a><span class = "spanQty">'+item.get('quantity')+'</span><a href="javascript:;" class="simpleCart_increment"><i class = "fa fa-plus"></i></a>';

		}},
		{ attr: "total" , label: "Total" , view: function (item, column) {
			if(item.get('pro')){
				return '<span id = "tot-'+item.get('pro')+'">&#8377;'+parseFloat(item.get('total')).toFixed(2)+'</span>';
			}else{
				return '<span id = "tot-'+item.get('offer')+'">&#8377;'+parseFloat(item.get('total')).toFixed(2)+'</span>';				
			}
		}},
		{ label: 'Ordered QTY' , attr: 'size', view: function(item, column){
				return '<span id = "qty-'+item.get('pro')+'">'+item.get('quantity')+'</span>';			
		}},
		{ view: "remove" , text: "<i class='fa fa-times-circle wrong-circle'></i>" , label: 'Remove'}
		],
		checkout: {
			type: "SendForm",
			url: "{{ route('shop.checkout') }}",
			method: "POST",
			extra_data: {
				grand : $('#finalGrand').html(),
				_token : $('meta[name="csrf-token"]').attr('content')
			}
		} 
	});	


	simpleCart.bind( 'beforeCheckout' , function( data ){
		// if(simpleCart.quantity() > 0){

			if($('#pincode').val() == ''){
				$('#pincode').focus();
				$('#pinMsg').removeClass('hidden').addClass('red').html('Please select Pincode for delivery');			
				return false;
			}

			if($('#pinStatus').html() == '1'){
				data.grand = $('.simpleCart_grandTotal').html();
				data.shipping = $('.simpleCart_shipping').html();
				data.pincode = $('#pincode').val();
				data._token = $('meta[name="csrf-token"]').attr('content');
			}else{
				return false;
			}
		// }else{
		// 	$('.simpleCart_checkout').hide();
		// }
	});

	function checkPin(fd) {
		$.ajax({
			url : '{{ route('shop.find-pin') }}',
			type : 'post',
			data : fd,
			processData: false,
			contentType : false,
			success : function(data){					
				if(data.msg == 'success'){
					$('#pinMsg').removeClass('hidden red').addClass('green').html('We are available to delivery at this pincode.');

					var pinPrice = parseFloat(data.data.pin_price), 
					grand = parseFloat($('.simpleCart_total').html().replace('₹', '').replace(',',''));
					var total = grand + pinPrice;

					$('.simpleCart_shipping').html('&#8377;'+data.data.pin_price+'.00');
					$('#finalGrand').html('&#8377;'+parseFloat(total)+'.00');
					$('#pinStatus').html('1');
				}else{
					$('#pinMsg').removeClass('hidden green').addClass('red').html('Sorry!! we cannot provide deivery to this pincode.');
					$('.simpleCart_shipping').html('&#8377;00.00');					
					$('#pinStatus').html('0');						
					// window.setTimeout(function() {
					// 	$('#pinMsg').addClass('hidden').html('');
					// },2000);
				}

			}
		});
	}
	$('#pinForm').on({
		'submit' : function(e){
			e.preventDefault();
			var fd = new FormData(this);
			checkPin(fd);			
		}
	});

	$('#pincode').on({
		'keyup change' : function(e){
			e.preventDefault();
			var fd = new FormData();
			fd.append('pincode', this.value);
			checkPin(fd);
		}
	});	
</script>
@endpush