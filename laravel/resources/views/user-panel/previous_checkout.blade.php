@extends('user-panel.layouts.master')
@section('title')
	Payment Details
@endsection
@push('header')
@isset ($MERCHANT_KEY)

<script>
	var hash = '{{ $hash }}';	
	function submitPayuForm() {
		if(hash == '') {
			return
		}else{
			$('#loader').show();		
		}
		var payuForm = document.forms.payuForm
		payuForm.submit()
	}
</script>
@endisset

@endpush
@php
$Log = 'login';
@endphp
@section('content')
<!--breadcrumbs-->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">Order Details</li>
		</ol>
	</div>
</div>
<!--//breadcrumbs-->
<!-- checkout content section start -->
<div class="checkout-area">
	<div class="container">
		<div class="row" {!! !Auth::guest() ? 'style="margin-top: 60px;"' : '' !!}>			
			<div class="col-md-5 col-sm-12">
				<div class="ro-checkout-summary">
					<div class="ro-title">
						<h3 class="checkbox9">ORDER SUMMARY</h3>
					</div>
					@for ($i = 1; $i <= $data['itemCount']; $i++)
					@php					
					$d = [];
					$a = explode(',',$data['item_options_'.$i]);
					foreach ($a as $key => $value) {
						$n = explode(':',$value);
						$d[trim($n[0])] = $n[1];

					}		
					@endphp
					@if (isset($d['pro']))
					@php						
						$pId = $d['pro'];
						$pro = App\Models\Product::find($pId);
					@endphp
					<div class="ro-body">						
						<div class="ro-item">
							<div class="ro-image">
								<a href="#">
									<img src="{{ asset($pro->display->img_path) }}" alt="">
								</a>
							</div>
							<div>
								<div class="tb-beg">
									<a href="#">{{ $pro->pro_title }}</a>
								</div>
							</div>
							<div>
								<div class="ro-price">
									<span class="amount">&#8377; {{ $data['item_price_'.$i] }}</span>
								</div>
								<div class="ro-quantity">
									<strong class="product-quantity">× {{ $data['item_quantity_'.$i] }}</strong>
								</div>
								<div class="product-total">
									<span class="amount"> &#8377; {{ $data['item_price_'.$i] * $data['item_quantity_'.$i] }}</span>
								</div>
							</div>
						</div>
						
					</div>
					@else
					@php						
						$oId = $d['offer'];
						$offer = App\Models\Offer::find($oId);
					@endphp
					<div class="ro-body">						
						<div class="ro-item">
							<div class="ro-image">
								<a href="#">
									<img src="{{ asset($offer->offer_img_path) }}" alt="">
								</a>
							</div>
							<div>
								<div class="tb-beg">
									<a href="#">{{ $offer->offer_title }}</a>
								</div>
							</div>
							<div>
								<div class="ro-price">
									<span class="amount">&#8377; {{ $data['item_price_'.$i] }}</span>
								</div>
								<div class="ro-quantity">
									<strong class="product-quantity">× {{ $data['item_quantity_'.$i] }}</strong>
								</div>
								<div class="product-total">
									<span class="amount"> &#8377; {{ $data['item_price_'.$i] * $data['item_quantity_'.$i] }}</span>
								</div>
							</div>
						</div>
						
					</div>
					@endif	
					@endfor
					<div class="ro-footer">
						<div>
							<p>
								Subtotal
								<span>
									<span class="amount">{{ '₹'.number_format(preg_replace('/₹|\.00/', '', $data['grand']) - preg_replace('/₹|\.00/', '', $data['shipping']),2) }}</span>
								</span>
							</p>
							<div class="ro-divide"></div>
						</div>
						<div class="shipping">
							<p> Shipping </p>
							<div class="ro-shipping-method">
								<p>	
									{{ $data['shipping'] }}								
								</p>
							</div>
							<div class="clearfix"></div>
							<div class="ro-divide"></div>
						</div>
						<div class="order-total">
							<p>
								Total
								<span>
									<strong>
										<span class="amount">{{ '₹'.number_format(preg_replace('/₹|\.00/', '', $data['grand']),2) }}</span>
									</strong>
								</span>
							</p>
						</div>
						<div>
							<p>
								Payment {{ $status ? 'recieved' : 'due' }}
								<span>
									<strong>
										<span class="amount">{{ '₹'.number_format(preg_replace('/₹|\.00/', '', $data['grand']),2) }}</span>
									</strong>
								</span>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7 col-sm-12">
				<div class="text">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li id = "homeTab" role="presentation" class="ano complete">
							<a href="#home" aria-controls="home" role="tab" data-toggle="tab"></a>
							<span>Address</span>
						</li>
						<li id = "paymentTab" role="presentation" class="ano complete">
							<a href="#" {{--  href="#profile" aria-controls="profile" role="tab" data-toggle="tab" --}}></a>
							<span>Payment</span>
						</li>
						<li id = "completeTab" role="presentation" class="ano la active">
							<a href="#complete" aria-controls="message" role="tab" data-toggle="tab"></a>
							<span>Complete</span>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						
						<div role="tabpanel" class="tab-pane mymsg active" id="complete">
							<div class="last-check">
							{{-- {{ dd($status) }}						 --}}
								@if ($status)
								<div class="alert">								 
									<strong>Thank you for your purchase!</strong>								
									<p>We recieved your payment successfully, we'll deliver your order to given address soon, for any query please contact us at {{ Config::get('app.landline') }} or {{ Config::get('app.support_email') }}</p>
								</div>
								@else
								<div class="alert fail">								  
									<strong>Order Failure!</strong> Please Try again or we'll contact to you shortly or please feel free to contact us at <b>{{ config('app.landline') }}</b> or <b>{{ config('app.support_email') }}</b>
								</div>
								@endif
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- checkout  content section end --> 
@endsection

@push('footer')
<script type="text/javascript">
	CRUD.formSubmission({
		url : "{{ url($Log) }}", 
		type : 0,
		id : '{{ $Log }}Form',
		onLoad: 'check',
		proData : function(data){
			console.log(data);
			if(data.msg == 'successLogin'){
				location.reload();
			}
		}
	});
	@isset($status)
	simpleCart.empty();
	@endif
</script>
{{--add particular Jqueries or scripts tag here of particular page--}}
@endpush