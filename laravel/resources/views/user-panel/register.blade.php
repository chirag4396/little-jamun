@extends('user-panel.layouts.master')
@section('title')
	Register
@endsection
@push('header')
@endpush
@section('content')
<!--breadcrumbs-->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">Register</li>
		</ol>
	</div>
</div>
<!--//breadcrumbs-->
<!--login-->
<div class="login-page">
	<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
		<h3 class="title">Register<span> Form</span></h3>
		{{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur </p> --}}
	</div>
	<div class="widget-shadow">
		<div class="login-top wow fadeInUp animated" data-wow-delay=".7s">
			<h4>Already have an Account ?<a href="{{ route('login') }}">  Sign In »</a> </h4>
		</div>
		<div class="login-body">
			<form class="wow fadeInUp animated" data-wow-delay=".7s" method="POST" action="{{ route('register') }}">
				@csrf
				<input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Full Name">

				@if ($errors->has('name'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif

				<input id="email" type="text" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email Address" >

				@if ($errors->has('email'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif

				<input id="password" type="password" class="lock{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

				@if ($errors->has('password'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
				@endif

				<input id="password-confirm" type="password" name="password_confirmation" required placeholder="Confirm Password">

				<input type="submit" name="Register" value="Register">
			</form>
		</div>
	</div>
</div>
<!--//login-->
@endsection

@push('footer')
@endpush