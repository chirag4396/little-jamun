@extends('user-panel.layouts.master')
@section('title')
{{ $product->pro_title }}
@endsection
@push('header')
{{-- <link rel="stylesheet" href="{{ asset('css/flexslider1.css') }}" type="text/css" media="screen" />	 --}}
@endpush
@push('footer')
<!--flex slider-->
<script defer src="{{ asset('js/jquery.flexslider.js') }}"></script>
<script>
	// Can also be used with $(document).ready()
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			controlNav: "thumbnails"
		});
	});
</script>
<!--flex slider-->
<script src="{{ asset('js/imagezoom.js') }}"></script>
@endpush
@section('content')

<!--breadcrumbs-->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">{{ $product->category->cat_name }}</li>
			{!! $product->subCategory ? '<li class="active">'.$product->subCategory->sc_name.'</li>' : '' !!}
			<li class="active">{{ $product->pro_title }}</li>
		</ol>
	</div>
</div>
<!--//breadcrumbs-->
<!--single-page-->
<div class="single">
	<div class="container">





                






		<div class="single-info simpleCart_shelfItem">		
			<div class="col-md-6 single-top wow fadeInLeft animated" data-wow-delay=".5s">	
				<div class="flexslider">
					<ul class="slides">
						@forelse ($product->images()->get() as $img)
						{{-- {{ $img }} --}}
						<li data-thumb="{{ asset($img->img_path) }}">
							<div class="thumb-image"> <img src="{{ asset($img->img_path) }}" data-imagezoom="true" class="img-responsive" alt="{{ $img->img_title }}" > </div>
						</li>							
						@empty

						@endforelse
						<span class="item_thumb">{{ asset($product->display ? $product->display->img_path : '') }}</span>
					</ul>
				</div>
			</div>
			<div class="col-md-6 single-top-left wow fadeInRight animated" data-wow-delay=".5s">
				<h3 class="item_name">{{ $product->pro_title }}</h3>
				<h6 class="item_price">&#8377;{{ number_format($product->pro_price,2) }}</h6>			
				{!! $product->pro_sub_title !!}
				<div class="clearfix"></div>
				<span class="item_pageLink hidden">{{ route('shop.product',['id' => $product->pro_id]) }}</span>
				<div class="item_pro hidden">{{ $product->pro_id }}</div>
				<div id = "types">
					@forelse (\App\Models\Type::get() as $type)				

					@if ($product['pro_'.strtolower($type->type_title)])
					<div class="col-md-6 types-div">
						<h2>{{ $type->type_title }}</h2>
						<select class="form-control" name = "{{ $type->type_title }}" onchange="checkData();">
							<option value="-1">--select--</option>
							@forelse (\App\Models\TypeDetail::whereIn('td_id', explode(',', $product['pro_'.strtolower($type->type_title)]))->get() as $td)
							<option>{{ $td->td_title }}</option>
							@empty
							@endforelse
						</select>
					</div>
					@endif
					@empty

					@endforelse	
				</div>

				<div class="clearfix"> </div>	

				<div class="quantity col-md-4 no-padding" >
					<p class="qty"> Qty :</p><input min="1" type="number" value="1" class="item_quantity" max = "{{ $product->pro_qty }}" onchange="checkData();">
				</div>
				<div class="clearfix"> </div>	
				
				@if ($product->subCategory)
				@if ($product->subCategory->sc_custom)
				<div class="short-description">
					<label class="br-label">Text to write on Product</label>
					<input type="text" name="text" class="item_text form-control">
				</div>
				<div class="clearfix"> </div>
				@endif
				@else
				
				@if ($product->category->cat_custom)
				<div class="short-description">
					<label class="br-label">Text to write on Product</label>
					<input type="text" name="text" class="item_text form-control" onkeydown="checkData();">
				</div>
				<div class="clearfix"> </div>
				@endif
				
				@endif

				<div class="btn_form">
					<a href="javascript:;" onclick = "checkData();" class="add-cart">ADD TO CART</a>	
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<!--collapse-tabs-->
		<div class="collpse tabs">
			<div class="panel-group collpse" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default wow fadeInUp animated" data-wow-delay=".5s">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								Description
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							{!! $product->pro_description !!}
						</div>
					</div>
				</div>

			</div>
		</div>
		<!--//collapse -->
		<!--related-products-->
		<div class="related-products">
			<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
				<h3 class="title">Related<span> Products</span></h3>
				{{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur </p> --}}
			</div>
			<div class="related-products-info">				
				@forelse ($product->suggestion() as $pro)
				<div class="col-md-3 new-grid new-mdl1 simpleCart_shelfItem wow flipInY animated" data-wow-delay=".5s">
					<div class="new-top">
						<a href="{{ route('shop.product',['id' => $pro->pro_id]) }}"><img src="{{ asset(isset($pro->display) ? $pro->display->img_thumb_path : '') }}" class="img-responsive item_thumb" alt="{{ $pro->pro_title }}"/></a>
						<span class="item_pageLink hidden">{{ route('shop.product',['id' => $pro->pro_id]) }}</span>							

						<div class="new-text">
							<ul>
								<li><a class="item_add" href=""> Add to cart</a></li>
								<li><a href="{{ route('shop.product',['id' => $pro->pro_id]) }}">Quick View </a></li>								
							</ul>
						</div>
					</div>
					<div class="new-bottom">
						<h5><a class="name" href="{{ route('shop.product',['id' => $pro->pro_id]) }}">{{ str_limit($pro->pro_title, 20) }}</a></h5>
						<div class="ofr">
							@php
							$r = $pro->pro_price;

							if ($pro->pro_discount){
								$r = $r - ($r * ($pro->pro_discount/100));
							}
							@endphp
							@if ($pro->pro_discount)						
							<p class="pric1"><del>&#8377; {{ number_format($pro->pro_price,2) }}</del></p>
							@endif
							<p><span class="item_price">&#8377; {{ number_format($r,2) }}</span></p>
							<div class="item_pro hidden">{{ $pro->pro_id }}</div>
							
						</div>
					</div>
				</div>					
				@empty

				@endforelse
				<div class="clearfix"> </div>
			</div>
		</div>
		<!--//related-products-->
	</div>
</div>
<!--//single-page-->

@endsection

@push('footer')

<script type="text/javascript">

	function checkData(){
		var vals = [];
		$('#types select').each(function(index, el) {
			var name = $(el).attr('name'),
			warn = 'warn'+index;
			$('#'+warn).remove();
			
			if($(el).val() == '-1'){
				var p = $('<p/>', {
					html : 'Please select '+name,
					class : 'warn',
					id : warn
				});
				$(el).after(p).focus();
				vals.push(false);
			}
		});

		if($('.item_text').length){
			$('#warnT').remove();
			if($('.item_text').val() == ''){
				var p = $('<p/>', {
					html : 'Please Enter some text to display',
					class : 'warn',
					id : 'warnT'
				});
				$('.item_text').after(p).focus();
				vals.push(false);
			}
		}
		var maxQ = parseInt($('.item_quantity').attr('max')),
		valQ = parseInt($('.item_quantity').val());

		if(valQ > maxQ){
			$('#warnQ').remove();
			var p = $('<p/>', {
				html : 'Please select quantity lessthan '+maxQ,
				class : 'warn',
				id : 'warnQ'
			});
			$('.item_quantity').after(p).focus();
			vals.push(false);
		}

		if($.inArray(false, vals) >= 0){
			$('.add-cart').removeClass('item_add').attr('onclick','checkData();');			
		}else{
			$('.add-cart').addClass('item_add');
			$('.warn').html('');
		}	
	}
	simpleCart.bind( 'afterAdd' , function( item ){
		var maxQ = parseInt($('.item_quantity').attr('max')),
			valQ = parseInt($('.item_quantity').val());
		var newQ = maxQ-valQ;
		
		$('.item_quantity').val(1).attr('max', newQ);
		if(!newQ){
			$('.add-cart').removeClass('item_add').html('Sold Out').attr('href','javascript:;').attr('onclick', false);
			return false;
		}
		// if(simpleCart.quantity() > 10 ){
			// alert("The maximum number of items per order is 10. Please remove some items and try again.");
			// return false;
		// }
	});
	// function changeQty(){
	// 	var maxQ = parseInt($('.item_quantity').attr('max')),
	// 	valQ = parseInt($('.item_quantity').val());
	// 	var newQ = maxQ-valQ;
		
	// 	$('.item_quantity').attr('max', newQ);
		
	// 	// if(!newQ){
	// 	// 	$('.add-cart').removeClass('item_add').html('Sold Out').attr('href','javascript:;');
	// 	// }
	// }
</script>
@endpush