<?php

namespace App\Logic\Image;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;
use App\Models\Image;
use App\Http\Traits\GetData;

class ImageRepository
{
    use GetData;
    public function upload( $form_data )
    {


        $pid = $form_data['product'];
        $photo = $form_data['file'];

        $originalName = pathinfo($photo->getClientOriginalName(), PATHINFO_FILENAME);
        // $extension = $photo->getClientOriginalExtension();
        // $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);
        
        $title = $originalName;
        $exe = $photo->getClientOriginalExtension();
        
        $filename = $title.'.'.$exe;        
        $thumbFilename = $title.'-thumbnail.'.$exe;

        // $filename = $this->sanitize($originalNameWithoutExt);
        // $allowed_filename = $this->createUniqueFilename( $filename, $extension );

        $uploadSuccess1 = $this->original( $photo, $filename );

        $uploadSuccess2 = $this->icon( $photo, $thumbFilename );

        if( !$uploadSuccess1 || !$uploadSuccess2 ) {

            return Response::json([
                'error' => true,
                'message' => 'Server error while uploading',
                'code' => 500
            ], 500);

        }

        $sessionImage = new Image;
        $sessionImage->img_title  = $title;
        $sessionImage->img_path = Config::get('app.images').$filename;
        $sessionImage->img_thumb_path = Config::get('app.thumbnails').$thumbFilename;        
        $sessionImage->img_pro_id = $pid;        
        $sessionImage->save();

        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);

    }

    public function createUniqueFilename( $filename, $extension )
    {
        // $path = Config::get('app.images');
        // $thumbPath = Config::get('app.thumbnails');
        $full_size_dir = Config::get('app.images');
        $full_image_path = $full_size_dir . $filename . '.' . $extension;

        if ( File::exists( $full_image_path ) )
            {
            // Generate token for image
                $imageToken = substr(sha1(mt_rand()), 0, 5);
                return $filename . '-' . $imageToken . '.' . $extension;
            }

            return $filename . '.' . $extension;
        }

    /**
     * Optimize Original Image
     */
    public function original( $photo, $filename )
    {
        $manager = new ImageManager();
        $image = $manager->make( $photo )->save(Config::get('app.images'). $filename );

        return $image;
    }

    /**
     * Create Icon From Original
     */
    public function icon( $photo, $filename )
    {
        $manager = new ImageManager();
        $image = $manager->make( $photo )->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->save( Config::get('app.thumbnails'). $filename );

        return $image;
    }

    /**
     * Delete Image From Session folder, based on original filename
     */
    public function delete( $id){
        $img = Image::find($id);
        $this->removeFile($img->img_path);
        $this->removeFile($img->img_thumb_path);
        $img->delete();     
    }

    public function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
        mb_strtolower($clean, 'UTF-8') :
        strtolower($clean) :
        $clean;
    }
}