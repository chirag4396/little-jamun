<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuyOffer extends Model
{
	protected $primaryKey = 'bo_id';

	protected $fillable = ['bo_buy_product', 'bo_buy_qty', 'bo_get_product', 'bo_get_qty', 'bo_offer'];

	public $timestamps = false;

	public function offer()
	{
		return $this->belongsTo(\App\Models\Offer::class, 'bo_offer');
	}
}
