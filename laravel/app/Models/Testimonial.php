<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
	protected $primaryKey = 'test_id';
    protected $fillable = ['test_name', 'test_designation', 'test_picture', 'test_testimonial', 'test_created_at'];
	CONST CREATED_AT = 'test_created_at';
	CONST UPDATED_AT = null;
}
