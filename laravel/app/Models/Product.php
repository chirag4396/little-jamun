<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 04:45:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 * 
 * @property int $pro_id
 * @property string $pro_title
 * @property string $pro_sub_title
 * @property string $pro_description
 * @property int $pro_price
 * @property int $pro_category
 * @property int $pro_type
 * @property int $pro_sub_type
 * @property int $pro_sell_price
 * @property int $pro_size
 * @property int $pro_sleeves
 * @property int $pro_neck
 * @property int $pro_work_type
 * @property int $pro_fabric
 * @property int $pro_style
 * @property int $pro_length
 * @property int $pro_design
 * @property int $pro_material
 * @property \Carbon\Carbon $pro_created_at
 * @property \Carbon\Carbon $pro_updated_at
 * 
 * @property \App\Models\Category $category
 *
 * @package App\Models
 */
class Product extends Eloquent
{
	protected $primaryKey = 'pro_id';
	public $timestamps = false;
	

	protected $dates = [
		'pro_created_at',
		'pro_updated_at'
	];

	protected $fillable = [
		'pro_title', 'pro_sub_title', 'pro_description', 'pro_price', 'pro_qty', 'pro_category', 'pro_sub_category', 'pro_discount', 'pro_cod', 'pro_materials', 'pro_colors', 'pro_frames'
	];
	protected $guarded = [];
	// public function newQuery($excludeDeleted = true) {		
	// 	return parent::newQuery($excludeDeleted)->with('category')->where('cat_delete_status',0);
	// }
	public function category(){
		return $this->belongsTo(\App\Models\Category::class, 'pro_category');
	}
	public function subCategory()
	{
		return $this->belongsTo(\App\Models\SubCategory::class, 'pro_sub_category');
	}
	
	public function falvor()
	{
		return $this->belongsTo(\App\Models\Flavor::class, 'pro_flavor');
	}
	public function images()
	{
		return $this->hasMany(\App\Models\Image::class, 'img_pro_id');
	}
	public function display()
	{
		return $this->hasOne(\App\Models\Image::class, 'img_pro_id')->where('img_display_priority',1);
	}

	public function orderedProduct(){
		return $this->hasMany(App\Models\OrderProduct::class, 'op_product_id');
	}

	public function enquiries(){
		return $this->hasMany(\App\Models\CustomOrderEnquiry::class,'enq_pro_id');
	}

	public function customPrice(){
		return $this->hasMany(\App\Models\CustomQuote::class,'coe_pro_id');
	}

	public function suggestion(){		
		$data = $this->where('pro_id', '!=' ,$this->pro_id)->where(function($data){
			if(!empty($this->subCategory->sc_id)){
				$data->where('pro_sub_category',$this->subCategory->sc_id);
			}			
			$data->where('pro_category',$this->category->cat_id);
		})->limit(3)->get();
		return $data;
	}
}
