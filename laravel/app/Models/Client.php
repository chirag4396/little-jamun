<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $primaryKey = 'client_id';
	protected $fillable = ['client_company', 'client_img_path'];
	public $timestamps = false;
}
