<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
   protected $primaryKey = 'mat_id';
   
   public $timestamps = false;

   protected $fillable = ['mat_title'];
}
