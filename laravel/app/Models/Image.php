<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 04:45:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Image
 * 
 * @property int $img_id
 * @property string $img_title
 * @property string $img_path
 * @property int $img_pro_id
 *
 * @package App\Models
 */
class Image extends Eloquent
{
	protected $primaryKey = 'img_id';

	protected $fillable = [
		'img_title',
		'img_path',
		'img_thumb_path',
		'img_display_priority',
		'img_pro_id'
	];
	
	public $timestamps = false;		

	public function product(){
		return $this->belongsTo(\App\Models\Product::class, 'img_pro_id');
	}
}
