<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Occasion extends Model
{
    protected $primaryKey = 'occ_id';

    protected $fillable = ['occ_title'];

    public $timestamps = false;
}
