<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomQuote extends Model
{
	protected $primaryKey = 'custom_id';

	protected $fillable = ['custom_coe_id', 'custom_pro_id', 'custom_price', 'custom_query', 'custom_status'];

	CONST CREATED_AT = 'custom_created_at';
	CONST UPDATED_AT = 'custom_updated_at';

	public function product(){
		return $this->belongsTo(\App\Models\Product::class,'custom_pro_id');
	}
	
	public function enquiry(){
		return $this->belongsTo(\App\Models\CustomOrderEnquiry::class,'custom_coe_id');
	}
}
