<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebDetail extends Model
{
    protected $fillable = ['about', 'terms', 'privacy'];    

    public $timestamps =false;
}
