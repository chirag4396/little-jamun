<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $primaryKey = 'type_id';
    
    public $timestamps = false;

    protected $fillable = ['type_title'];

    public function details(){
    	$this->hasMany(App\TypeDetail::Class, 'td_type');
    }
}
