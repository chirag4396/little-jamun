<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecondBanner extends Model
{
    protected $primaryKey = 'sb_id';       

    protected $fillable = ['sb_title', 'sb_sub_title', 'sb_description', 'sb_img'];

    CONST CREATED_AT = 'sb_created_at';
    
    CONST UPDATED_AT = 'sb_updated_at';
}
