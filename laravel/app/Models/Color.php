<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $primaryKey = 'color_id';
    
    public $timestamps = false;

    protected $fillable = ['color_title'];
}
