<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PinCode extends Model
{
    protected $primaryKey = 'pin_id';
    public $timestamps = false;
    
    protected $fillable = [
    	'pin_area', 'pin_code', 'pin_price', 'pin_status'
    ];
}
