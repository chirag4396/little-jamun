<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomOrderEnquiry extends Model
{
    protected $primaryKey = 'coe_id';

    protected $fillable = [ 'coe_pro_id', 'coe_veg', 'coe_mobile', 'coe_weight', 'coe_email', 'coe_name', 'coe_query', 'coe_img_path', 'coe_status'];

    CONST CREATED_AT = 'coe_created_at';
    CONST UPDATED_AT = 'coe_updated_at';

    // public function order(){
    // 	return $this->belongsTo(\App\Models\OrderDetail::class,'coe_order_id');
    // }

    public function product(){
    	return $this->belongsTo(\App\Models\Product::class,'coe_pro_id');
    }
    public function customPrice(){
    	return $this->hasOne(\App\Models\CustomQuote::class, 'custom_coe_id');
    }
}
