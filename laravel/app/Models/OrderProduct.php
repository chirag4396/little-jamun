<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
	protected $primaryKey = 'op_id';
	public $timestamps = false;

	
	protected $fillable = ['op_order_id', 'op_product_id', 'op_offer_id', 'op_quantity', 'op_flavor', 'op_flavor_price', 'op_weight', 'op_piece', 'op_type', 'op_veg', 'op_sugar', 'op_price'];

	public function order(){
    	return $this->belongsTo(\App\Models\OrderDetail::class, 'op_order_id');
    }

    public function product(){
        // return $this->op_product_id;
        // if($this->)
    	return $this->belongsTo(\App\Models\Product::class, 'op_product_id');
    }

    public function offer(){
    	return $this->belongsTo(\App\Models\Offer::class, 'op_offer_id');
    }
}
