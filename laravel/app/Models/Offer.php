<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
	protected $primaryKey = 'offer_id';

	protected $fillable = ['offer_occasion', 'offer_title', 'offer_type', 'offer_products', 'offer_description', 'offer_cost', 'offer_img_path', 'offer_veg', 'offer_start', 'offer_end'];

	CONST CREATED_AT = 'offer_created_at';
	
	CONST UPDATED_AT = 'offer_updated_at';

	public function orderedOffer(){
		return $this->hasMany(App\Models\OrderProduct::class, 'op_offer_id');
	}	

	public function buyOffer()
	{
		return $this->hasOne(\App\Models\BuyOffer::class, 'bo_offer');
	}
}
