<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 04:45:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Type
 * 
 * @property int $type_id
 * @property string $type_name
 *
 * @package App\Models
 */
class Flavor extends Eloquent
{
	protected $primaryKey = 'flavor_id';
	public $timestamps = false;

	protected $fillable = [
		'flavor_name',
		'flavor_price',
		'flavor_type',
		'flavor_default'
	];	

	public function products()
	{
    	return $this->hasMany(\App\Models\Product::class, 'pro_flavor');		
	}
}
