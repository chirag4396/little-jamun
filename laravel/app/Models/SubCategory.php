<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 04:45:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Type
 * 
 * @property int $type_id
 * @property string $type_name
 *
 * @package App\Models
 */
class SubCategory extends Eloquent
{
	protected $primaryKey = 'sc_id';
	public $timestamps = false;

	protected $fillable = [
		'sc_name',
		'sc_description',
		'sc_cat_id',
		'sc_types',
		'sc_icon_path',
		'sc_custom'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class, 'sc_cat_id');
	}
	// public function products($id = null)
	// {
		
	// 	$data = $this->hasMany(\App\Models\Product::class, 'pro_sub_category')->where(function($data) use ($id){
	// 		if(!is_null($id)){
	// 			$data->where('pro_id','!=',$id);
	// 		}
	// 	});
	// 	return $data;
	// }
	// public function childCategory()
	// {
	// 	return $this->hasMany(\App\Models\ChildCategory::class, 'cc_sc_id');
	// }

	// public function superChildCategory()
	// {
	// 	return $this->hasMany(\App\Models\SuperChildCategory::class, 'scc_sc_id');
	// }
}
