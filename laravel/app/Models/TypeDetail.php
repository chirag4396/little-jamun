<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeDetail extends Model
{
    protected $primaryKey = 'td_id';
    
    public $timestamps = false;

    protected $fillable = ['td_title', 'td_type'];

    public function type(){
    	$this->belongsTo(App\Type::Class, 'td_type');
    }
}
