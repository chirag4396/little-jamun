<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $primaryKey = 'enq_id';

	CONST CREATED_AT = 'enq_created_at';
	CONST UPDATED_AT = null;

    protected $fillable = ['enq_id', 'enq_name', 'enq_mobile', 'enq_email', 'enq_query','enq_created_at'];
        
}