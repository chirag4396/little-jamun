<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	protected $primaryKey = 'ban_id';
	public $timestamps = false;

    protected $fillable = ['ban_title', 'ban_sub_title', 'ban_img_path'];
}
