<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
	protected $primaryKey = 'faq_id';
	protected $fillable = ['faq_qus', 'faq_ans'];
	CONST CREATED_AT = 'faq_created_at';
	CONST UPDATED_AT = 'faq_updated_at';
}
