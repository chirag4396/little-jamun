<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 04:45:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Type
 * 
 * @property int $type_id
 * @property string $type_name
 *
 * @package App\Models
 */
class ChildCategory extends Eloquent
{
	protected $primaryKey = 'cc_id';
	public $timestamps = false;

	protected $fillable = [
		'cc_name',
		'cc_description',
		'cc_sc_id',
		'cc_icon_path'		
	];
	
	public function subCategory()
	{
		return $this->belongsTo(\App\Models\SubCategory::class, 'cc_sc_id');
	}
	
	public function products()
	{
		return $this->hasMany(\App\Models\Product::class, 'pro_child_category');
	}

	public function superChildCategory()
	{
		return $this->hasMany(\App\Models\SuperChildCategory::class, 'scc_cc_id');
	}
}
