<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 04:45:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Category
 * 
 * @property int $cat_id
 * @property string $cat_name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Category extends Eloquent
{
	protected $primaryKey = 'cat_id';
	public $timestamps = false;

	protected $fillable = [
		'cat_name',
		'cat_icon_path',
		'cat_icon_thumb_path',
		'cat_description',
		'cat_custom',
		'cat_delete_status',
		'cat_types',
		'cat_sub'	
	];
	// public function newQuery($excludeDeleted = true) {		
	// 	return parent::newQuery($excludeDeleted)->where('cat_delete_status',0);		
	// }
	public function products(){
		return $this->hasMany(\App\Models\Product::class, 'pro_category');
	}

	public function subCategory(){
		return $this->hasMany(\App\Models\SubCategory::class, 'sc_cat_id');
	}
}
