<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $primaryKey = 'ud_id';
	public $timestamps = false;
	
	protected $fillable = ['ud_address', 'ud_pincode', 'ud_city', 'ud_mobile', 'ud_user_id'];

	public function user()
	{
		return $this->belongsTo(\App\User::class, 'ud_user_id');
	}

	public function order()
    {
        return $this->hasOne(\App\Models\OrderDetail::class, 'order_user_detail');
    }
}
