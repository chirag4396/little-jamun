<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuperChildCategory extends Model
{
    protected $primaryKey = 'scc_id';

    public $timestamps = false;

    protected $fillable = [
    	'scc_name',
    	'scc_description',
    	'scc_cc_id',
    	'scc_sc_id',
    	'scc_icon_path'		
    ];
    
    public function childCategory()
    {
    	return $this->belongsTo(\App\Models\ChildCategory::class, 'scc_cc_id');
    }
    
    public function subCategory()
    {
    	return $this->belongsTo(\App\Models\SubCategory::class, 'scc_sc_id');
    }

    public function products()
    {
    	return $this->hasMany(\App\Models\Product::class, 'pro_super_child_category');
    }
}
