<?php 
namespace App\Helper;
use Carbon\Carbon;
use App\User;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManager;

class Helper{	

	public static function resizeImage($image, $quality = 75){
	// return $image;   
		$sourceImage = $image;
		$targetImage = $image;

		list($maxWidth, $maxHeight, $type, $attr) = getimagesize($image);
		
		if (!$image = @imagecreatefromjpeg($sourceImage)){
			return false;
		}

		list($origWidth, $origHeight) = getimagesize($sourceImage);

		if ($maxWidth == 0){
			$maxWidth  = $origWidth;
		}

		if ($maxHeight == 0){
			$maxHeight = $origHeight;
		}

		$widthRatio = $maxWidth / $origWidth;
		$heightRatio = $maxHeight / $origHeight;

		$ratio = min($widthRatio, $heightRatio);

		$dataewWidth  = (int)$origWidth  * $ratio;
		$dataewHeight = (int)$origHeight * $ratio;

		$dataewImage = imagecreatetruecolor($dataewWidth, $dataewHeight);
		imagecopyresampled($dataewImage, $image, 0, 0, 0, 0, $dataewWidth, $dataewHeight, $origWidth, $origHeight);
		imageinterlace($image, 1);
		imagejpeg($dataewImage, $targetImage, $quality);

		imagedestroy($image);
		imagedestroy($dataewImage);
	}

	public static function changeKeys($rep, $arr = []){
		$dataew = [];
		foreach ($arr as $key => $value) {			
			$dataew[$rep.$key] = $value;
		}		
		return $dataew;
	}

	public static function dateParser($date){
		return Carbon::parse($date)->format('Y-m-d h:i:s');
	}

	public static function removePrefix($data){
		foreach ($data as $k => $v) {
			if(is_array($v)){
				$data[$k] = $this->removePrefix($v);
			}

			$e = explode('_', $k);
						
			if(count($e) > 1){
				unset($e[0]);				
				$d = implode('_', $e);
				$data[$d] = $v;
				unset($data[$k]);
			}
		}
		return $data;        
	}
	
	public static function randomPassword($length) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$password = substr( str_shuffle( $chars ), 0, $length );
		return $password;
	}   

	public static function username(){     
		return 'myfab'.(User::max('id')+1);
	}

	public static function addUser($req){		
		$user = User::orWhere('email', $req['email'])                
		->first();

		$pass = $this->randomPassword(8);

		if(!$user){
			$newUser = User::create([				
				'name' => $req['firstname'],				
				'mobile' => $req['phone'],
				'email' => $req['email'],     
				'password' => bcrypt($pass),
				'v_password' => $pass,				
				'type' => 102,				
			]);
			$this->guard()->login($newUser);
			return $newUser->id;
		}else{
			$this->guard()->login($user);			
			return $user->id;   
		}
	}

	public static function removeFile($path){
		if(File::exists($path)){
			unlink($path);
		}
	}

	public static function uploadFiles($data, $title, $file, $paths = [], $remove = []){
		// return $paths;
		$postfix = ['big','thumbnail'];
		$da = [];

		$title = str_replace(' ','-',trim($title));
		$exe = $data->file($file)->getClientOriginalExtension();
		$manager = new ImageManager();
		
		$photo = $data->file($file)->getRealPath();
		
		foreach ($paths as $key => $value) {
			if(count($remove) > 0){
				$this->removeFile($remove[$key]);
			}
			$filename = $title.'-'.$postfix[$key].'.'.$exe;
			$full = $paths[$key].$filename;            

			$manager->make($photo)->resize(200, null, function ($constraint) {
				$constraint->aspectRatio();
			})->save($full);

			$this->resizeImage($full);
			$da[] = $full;
		}
		return $da;
	}

	public static function sendSMS($mobile,$msg){

		$url = 'http://trans.smsfresh.co/api/sendmsg.php';

		$fields = array(
			'user' => 'magarsham', 
			'pass' => 'festivito5555', 
			'sender' => 'FESTVT' ,
			'phone' => $mobile, 
			'text' => $msg, 
			'priority' => 'ndnd', 
			'stype' => 'normal'        
		);


		$ch = curl_init();


		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));


		$res = curl_exec($ch);


		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_close($ch);

		return $res;
	}

	protected function guard()
	{
		return Auth::guard();
	}


	public static function sendEmail($email, $name, $data, $blade){

		// return view('admin.email.'.$blade)->with($data);
		
		// $order = OrderDetail::find($orderId);
		return Mail::send('admin.email.'.$blade, $data, function ($message) use ($email, $name) {
			$message->from('info@browniepointpune.com', 'Brownie Point Pune');
			$message->sender('browniepointpune@gmail.com', 'Brownie Point Pune');
			$message->to($email, $name);			    
			
			$message->subject('Brownie Point Pune - Order confirmation');
			
			$message->priority(1);				   
		});
			// return $link;
	}
}