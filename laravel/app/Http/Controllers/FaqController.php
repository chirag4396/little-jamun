<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faq;
use App\Http\Traits\GetData;

class FaqController extends Controller
{
    use GetData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_faqs')->with(['faqs' => Faq::get()]);
    }

    public function all(){
        return view('user-panel.faq')->with(['faqs' => Faq::get()]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_faq');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        // $data [];
        $n = $this->changeKeys('faq_', $r->all());         
        $data['msg'] = (Faq::create($n) ? 'success' : 'error');        

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->removePrefix(Faq::find($id)->toArray());                
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {    
        $data = [];

        unset($r['_method']);
        $n = $this->changeKeys('faq_', $r->all());
        unset($n['faq_sid']);
        $faq = Faq::where('faq_id', $r->sid);
        $c = $faq->update($n);        
            
        $data['msg'] = 'successU';
        $data['data'] = $faq;
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['msg'] = 'error';
        $faq = Faq::find($id);

        $data['msg'] = $faq->delete() ? 'delete' : 'error';

        return $data;
    }
}
