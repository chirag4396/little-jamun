<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PayUMoneyController extends Controller
{
	protected $MERCHANT_KEY = "TXMkTSIC";
	protected $SALT = "QZPcki3WVO";
	protected $PAYU_BASE_URL = "https://test.payu.in";
	protected $action = '';
	protected $hash = '';
	protected $txnid = '';

	protected $formError = 0;

	public function getOrder(Request $r){
		list($posted, $this->txnid) = $this->listd($r);

		return view('user-panel.pay')->with(['hash' => $this->hash, 'formError' => $this->formError, 'action' => url('placeorder'), 'MERCHANT_KEY' => $this->MERCHANT_KEY, 'txnid' => $this->txnid]);
	}

	public function getPay(Request $r){ 

		$posted = array();
		if(!empty($r)) {

			foreach($r->all() as $key => $value) {    
				$posted[$key] = $value; 

			}
		}
		// return $posted;
		if(empty($posted['txnid'])) {            
			$this->txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		} else {
			// $n =$this->addUser($posted);
			// $u = $posted['productinfo'].'-'.$n.'-'.$posted['domain_name'];
			// $r->session()->put('productinfo', $u);
			$this->txnid = $posted['txnid'];
		}          
		$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		if(empty($posted['hash']) && sizeof($posted) > 0) {
			if(
				empty($posted['key'])
				|| empty($posted['txnid'])
				|| empty($posted['amount'])
				|| empty($posted['firstname'])
				|| empty($posted['email'])
				|| empty($posted['phone'])
				|| empty($posted['productinfo'])
				|| empty($posted['surl'])
				|| empty($posted['furl'])
				|| empty($posted['service_provider'])
			) {
				$this->formError = 1;
			} else {
				$hashVarsSeq = explode('|', $hashSequence);
				$hash_string = '';  
				foreach($hashVarsSeq as $hash_var) {
					$hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
					$hash_string .= '|';
				}

				$hash_string .= $this->SALT;


				$this->hash = strtolower(hash('sha512', $hash_string));
				$this->action = $this->PAYU_BASE_URL . '/_payment';
			}
		} elseif(!empty($posted['hash'])) {     
			$this->hash = $posted['hash'];
			$this->action = $this->PAYU_BASE_URL . '/_payment';
		}
		// return $this->action;
		return view('user-panel.pay')->with([
			'hash' => $this->hash, 
			'formError' => $this->formError, 
			'action' => $this->action, 
			'MERCHANT_KEY' => $this->MERCHANT_KEY, 
			'txnid' => $this->txnid,
			'posted' => $posted,
			
		]);
	}

	public function orderStatus (Request $r){    
		$status=$r['status'];
		$firstname=$r["firstname"];
		$amount=$r["amount"];
		$txnid=$r["txnid"];
		$posted_hash=$r["hash"];
		$key=$r["key"];
		$productinfo=$r["productinfo"];
		$email=$r["email"];
		$salt=$this->SALT;

		if (isset($r["additionalCharges"])) {
			$additionalCharges=$r["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

		}
		else {    

			$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

		}
		$hash = hash("sha512", $retHashSeq);

		if ($hash != $posted_hash) {
			return "Invalid Transaction. Please try again";
		}else {			
			return 'Success';
		}
	}

	public function listd(Request $r){

		$posted = array();
		if(!empty($r)) {			
			foreach($r->all() as $key => $value) {    
				$posted[$key] = $value; 

			}
		}

		if(empty($posted['txnid'])) {
			$this->txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		} else {
			$this->txnid = $posted['txnid'];
		}

		return [$posted,$this->txnid];
	}
}