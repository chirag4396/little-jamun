<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PinCode;
use App\Http\Traits\GetData;

class PinCodeController extends Controller
{
    use GetData;
    public function finPincode(Request $r)
    {

        $pin = PinCode::where('pin_status',1)->where('pin_code',$r->pincode)->first();
        if($pin){
            return ['msg' => 'success', 'data' => $pin];
        }
        return ['msg' => 'error'];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_pincodes')->with(['pincodes' => PinCode::get()]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_pincode');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        if(!PinCode::where('pin_code',$r->code)->first()){
            $n = $this->changeKeys('pin_', $r->all());
            $n['pin_area'] = ucfirst($n['pin_area']);

            $data['msg'] = PinCode::create($n) ? 'success' : 'error';

        }else{
            $data['msg'] = 'exist';
        }   
        return $data; 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->removePrefix(PinCode::find($id)->toArray());        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $data = [];

        unset($r['_method']);
        // return $r->all();
        if (isset($r->status)) {
            $p = PinCode::find($id)->update(['pin_status' => $r->status]);                    
            return redirect()->back();
        }
        $n = $this->changeKeys('pin_', $r->all());
        unset($n['pin_sid']);
        $pin = PinCode::find($r->sid);
        
        $p = PinCode::where('pin_id', $r->sid)->update($n);        

        $data['msg'] = 'successU';
        $data['data'] = $pin;

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['msg'] = 'error';
        $pin = PinCode::find($id);

        if($pin->delete()){            
            $data['msg'] =  'delete';
        }

        return $data;
    }
}
