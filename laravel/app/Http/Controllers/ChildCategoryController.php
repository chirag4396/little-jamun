<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ChildCategory;
use App\Http\Traits\GetData;

class ChildCategoryController extends Controller
{
    use GetData;
    protected $path = 'images/child-categories/';

    public function index()
    {
        return view('admin.view_child_categories')->with(['categories' => ChildCategory::get()]);
    }

    public function create()
    {
        return view('admin.create_child_category');
    }

    public function store(Request $r)
    {
        if(!ChildCategory::where('cc_name',$r->name)->first()){
            $n = $this->changeKeys('cc_', $r->all());  
            $n['cc_name'] = ucfirst($n['cc_name']);         
            $n['cc_icon_path'] = $this->uploadFiles($r, $n['cc_name'], 'picture', [$this->path])[0];

            $data['msg'] = (ChildCategory::create($n) ? 'success' : 'error');
        }else{
            $data['msg'] = 'exist';
        }   
        return $data; 
    }

    public function edit($id)
    {
        return $this->removePrefix(ChildCategory::find($id)->toArray());        
    }

    public function update(Request $r, $id)
    {

        $data = [];

        unset($r['_method']);
        $n = $this->changeKeys('cc_', $r->all());
        unset($n['cc_sid']);
        $child = ChildCategory::find($r->sid);        
        
        if ($r->hasFile('picture')) {
            $n['cc_icon_path'] = $this->uploadFiles($r, $n['cc_name'], 'picture', [$this->path], [$child->cc_icon_path])[0];            
            unset($n['cc_picture']);
        }

        $c = ChildCategory::where('cc_id', $r->sid)->update($n);        
        $data['msg'] = 'successU';
        $data['data'] = $child;

        return $data;
    }

    public function destroy($id)
    {
        $data['msg'] = 'error';
        $child = ChildCategory::find($id);
        $this->removeFile($child->cc_picture);

        $data['msg'] = $child->delete() ? 'delete' : 'error';

        return $data;

    }

    public function getChildCategories($id = null)
    {        
        return response()->json(ChildCategory::select(['cc_name as name','cc_id as id'])->where('cc_sc_id',$id)->get());
    }
}
