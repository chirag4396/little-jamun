<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\WebDetail;
class HomeController extends Controller
{
	public function index(){
		
		return view('user-panel.index');
		// return view('user-panel.index');
	}

	public function webDetails($type){
		$web = WebDetail::first();    	
		return view('user-panel.'.$type)->with(['data' => $web[$type]]);
	}
	public function view(){		
		return view('admin.create_web_details')->with(['data' => WebDetail::first()]);
	}
	public function update(Request $r){
		
		$data = ['msg'=>'successU'];
		$web = WebDetail::find(1)->update([$r->field => $r->data]);
		return $data;    	
	}	
}
