<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Flavor;
use App\Http\Traits\GetData;

class FlavorController extends Controller
{
    use GetData;
    protected $pre = 'flavor_';
    public function index()
    {
        return view('admin.view_flavors')->with(['flavors' => Flavor::orderBy($this->pre.'name', 'asc')->get()]);
    }

    public function create()
    {
        return view('admin.create_flavor');
    }

    public function store(Request $r)
    {
        if(!Flavor::where($this->pre.'name',$r->name)->first()){
            $n = $this->changeKeys($this->pre, $r->all()); 
            $n[$this->pre.'name'] = ucfirst($n[$this->pre.'name']);
            $data['msg'] = (Flavor::create($n) ? 'success' : 'error');
        }else{
            $data['msg'] = 'exist';
        }   
        return $data; 
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return $this->removePrefix(Flavor::find($id)->toArray());        
    }
    
    public function update(Request $r, $id)
    {

        $data = [];

        unset($r['_method']);
        $n = $this->changeKeys($this->pre, $r->all());        
        unset($n[$this->pre.'sid']);        
        $flavor = Flavor::find($r->sid);      

        $c = Flavor::where($this->pre.'id', $r->sid)->update($n);
        $data['msg'] = 'successU';
        array_push($data, $flavor);

        return $data;
    }

    public function destroy($id)
    {
        $data['msg'] = 'error';
        $data['msg'] = Flavor::find($id)->delete() ? 'delete' : 'error';

        return $data;

    }
}
