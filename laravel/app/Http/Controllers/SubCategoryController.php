<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SubCategory;
use App\Http\Traits\GetData;

class SubCategoryController extends Controller
{
    use GetData;
    protected $path = 'images/sub-categories/';

    public function index()
    {
        return view('admin.view_sub_categories')->with(['categories' => SubCategory::orderBy('sc_name', 'asc')->get()]);
    }

    public function create()
    {
        return view('admin.create_sub_category');
    }

    public function store(Request $r)
    {
        // return $r->all();

        if(!SubCategory::where('sc_name',$r->name)->first()){
            $n = $this->changeKeys('sc_', $r->all()); 
            $n['sc_types'] = implode(',', $r->types);
            $n['sc_name'] = ucfirst($n['sc_name']);
            // return $n;
            if($r->hasFile('icon_path')){

                $n['sc_icon_path'] = $this->uploadFiles($r, $n['sc_name'], 'icon_path', [$this->path])[0];
            }

            $data['msg'] = (SubCategory::create($n) ? 'success' : 'error');
        }else{
            $data['msg'] = 'exist';
        }   
        return $data; 
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return $this->removePrefix(SubCategory::find($id)->toArray());        
    }
    
    public function update(Request $r, $id)
    {        
        $data = [];

        unset($r['_method']);
        $n = $this->changeKeys('sc_', $r->all()); 
        $n['sc_types'] = $r->types ? implode(',', $r->types) : null;
        $n['sc_name'] = ucfirst($n['sc_name']);

        unset($n['sc_sid']);
        
        if($r->hasFile('icon_path')){

            $n['sc_icon_path'] = $this->uploadFiles($r, $n['sc_name'], 'icon_path', [$this->path])[0];
        }

        // $n = $this->changeKeys('sc_', $r->all());
        // $n['sc_types'] = implode(',', $r->types);        
        // $sub = SubCategory::find($r->id);        
        
        // if ($r->hasFile('icon_path')) {
        //     $n['sc_icon_path'] = $this->uploadFiles($r, $n['sc_name'], 'icon_path', [$this->path], [$sub->sc_icon_path])[0];
        // }
        $sub = SubCategory::where('sc_id', $r->id);
        $c = $sub->update($n);        
        $data['msg'] = 'successU';
        $data['data'] = $sub;

        return $data;
    }

    public function destroy($id)
    {
        $data['msg'] = 'error';        
        $sub = SubCategory::find($id);
        
        $this->removeFile($sub->icon_path);

        $data['msg'] = $sub->delete() ? 'delete' : 'error';

        return $data;

    }

    public function getSubCategories($id = null)
    {        
        return response()->json(SubCategory::select(['sc_name as name','sc_id as id'])->where('sc_cat_id',$id)->get());
    }
}
