<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Http\Traits\GetData;
use Intervention\Image\ImageManager;

class ClientController extends Controller
{
    use GetData;
    public function index()
    {
        return view('admin.view_clients')->with(['clients' => Client::get()]);
    }

    public function create()
    {
        return view('admin.create_client');
    }

    public function store(Request $r)
    {
        $path = 'images/clients/';

        if(!Client::where(['client_company' => $r->name])->first()){
            $n = $this->changeKeys('client_', $r->all()); 
            $n['client_company'] = ucfirst($n['client_company']);
            
            $title = str_replace(' ','-',trim($n['client_company']));
            $exe = $r->file('picture')->getClientOriginalExtension();
            $filename = $title.'.'.$exe;
            
            $manager = new ImageManager();
            $photo = $r->file('picture')->getRealPath();

            $thumbnail = $path.$filename;            

            $manager->make($photo)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbnail);
            
            $this->resizeImage($thumbnail);
            $n['client_img_path'] = $thumbnail;

            $data['msg'] = (Client::create($n) ? 'success' : 'error');
        }else{
            $data['msg'] = 'exist';
        }   
        return $data; 
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return $this->removePrefix(Client::find($id)->toArray());        
    }
    
    public function update(Request $r, $id)
    {

        $data = [];

        unset($r['_method']);
        $n = $this->changeKeys('client_', $r->all());
        unset($n['client_sid']);
        // return $id;
        $client = Client::find($r->sid);
        
        // if($client->client_company != $n['client_company']){

        $path = 'images/clients/';
        $n['client_company'] = ucfirst($n['client_company']);

        if ($r->hasFile('picture')) {
            if(File::exists($client->client_img_path)){
                unlink($client->client_img_path);
            }

            $title = str_replace(' ','-',trim($n['client_company']));
            $exe = $r->file('picture')->getClientOriginalExtension();
            $filename = $title.'.'.$exe;

            $manager = new ImageManager();
            $photo = $r->file('picture')->getRealPath();

            $thumbnail = $path.$filename;            

            $manager->make($photo)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbnail);

            $this->resizeImage($thumbnail);
            $n['client_img_path'] = $thumbnail;
        }
        $c = Client::where('client_id', $r->sid)->update($n);        
        $data['msg'] = 'successU';
        $data['data'] = $client;
        return $data;
    }

    public function destroy($id)
    {
        $data['msg'] = 'error';
        $client = Client::find($id);
        $this->removeFile($client->client_img_path);        
                
        if($client->delete()){            
            $data['msg'] =  'delete';
        }

        return $data;
    }

}
