<?php

namespace App\Http\Controllers;

use App\Models\SuperChildCategory;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;

class SuperChildCategoryController extends Controller
{
    use GetData;
    protected $path = 'images/super-child-categories/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_super_child_categories')->with(['categories' => SuperChildCategory::get()]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_super_child_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        if(!SuperChildCategory::where(['scc_name' => $r->name, 'scc_cc_id' => $r->cc_id])->first()){
            $n = $this->changeKeys('scc_', $r->all());  
            $n['scc_name'] = ucfirst($n['scc_name']);         
            $n['scc_icon_path'] = $this->uploadFiles($r, $n['scc_name'], 'picture', [$this->path])[0];

            $data['msg'] = (SuperChildCategory::create($n) ? 'success' : 'error');
        }else{
            $data['msg'] = 'exist';
        }   
        return $data; 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SuperChildCategory  $superChildCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SuperChildCategory $superChildCategory)
    {
        return $this->removePrefix(SuperChildCategory::find($id)->toArray());        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SuperChildCategory  $superChildCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SuperChildCategory $superChildCategory)
    {
        return $this->removePrefix($superChildCategory->toArray());        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SuperChildCategory  $superChildCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, SuperChildCategory $superChildCategory)
    {
        $data = [];

        unset($r['_method']);
        $n = $this->changeKeys('scc_', $r->all());
        // unset($n['scc_sid']);
        // $child = ChildCategory::find($r->sid);        
        // return $superChildCategory;
        if ($r->hasFile('picture')) {
            $n['scc_icon_path'] = $this->uploadFiles($r, $n['scc_name'], 'picture', [$this->path], [$child->cc_icon_path])[0];            
            unset($n['cc_picture']);
        }

        $c = $superChildCategory->update($n);        
        $data['msg'] = 'successU';
        $data['data'] = $superChildCategory;

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SuperChildCategory  $superChildCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SuperChildCategory $superChildCategory)
    {
        $data['msg'] = 'error';
        // $child = ChildCategory::find($id);
        $this->removeFile($superChildCategory->scc_icon_path);

        $data['msg'] = $superChildCategory->delete() ? 'delete' : 'error';

        return $data;
    }

    public function getSuperChildCategories($id)
    {        
        return response()->json(SuperChildCategory::select(['scc_name as name','scc_id as id'])->where('scc_cc_id',$id)->get());
    }
}
