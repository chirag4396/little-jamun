<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Products;
use App\Models\Image;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class CategoryController extends Controller
{
    use GetData;
    protected $path = 'images/categories/';
    protected $pathThumb = 'images/categories/thumbnails/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_categories')->with(['categories' => Category::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_category');
    }
    
    public function store(Request $r)
    {    
    // return $r->all();    
        try {
            if(!Category::where('cat_name',$r->name)->first()){
                $n = $this->changeKeys('cat_', $r->all());
                $n['cat_name'] = ucfirst($n['cat_name']);            
                
                if(isset($n['cat_types'])){
                    $n['cat_types'] = implode(',',$n['cat_types']);
                }
                // return $n;
                if ($r->hasFile('icon_path')) {
                    list($regular, $thumb) = $this->uploadFiles($r, $n['cat_name'], 'icon_path', [$this->path, $this->pathThumb]);
                    $n['cat_icon_path'] = $regular;
                    $n['cat_icon_thumb_path'] = $thumb;
                }

                $data['msg'] = Category::create($n) ? 'success' : 'error';

            }else{
                $data['msg'] = 'exist';
            }
        } catch (QueryException $e) {
            $data['msg'] = 'error';        
        }
        return $data;     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->removePrefix(Category::find($id)->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        // return $r->all();
        $data = [];

        unset($r['_method']);
        $n = $this->changeKeys('cat_', $r->all());
        if(isset($n['cat_types'])){
            $n['cat_types'] = implode(',',$n['cat_types']);
        }
        if($n['cat_sub']){
            $n['cat_types'] = null;
        }

        unset($n['cat_id']);
        // return $n;
        $cat = Category::find($r->id);        
        if ($r->hasFile('icon_path')) {
            list($regular, $thumb) = $this->uploadFiles($r, $n['cat_name'], 'icon_path', [$this->path, $this->pathThumb],[$cat->cat_icon_path,$cat->cat_icon_thumb_path]);
            $n['cat_icon_path'] = $regular;
            $n['cat_icon_thumb_path'] = $thumb;
        }  


        $c = Category::where('cat_id', $r->id)->update($n);        

        $data['msg'] = 'successU';
        $data['data'] = $cat;

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['msg'] = 'error';
        $cat = Category::find($id);
        $pros = $cat->products();
        $subs = $cat->subCategory();

        if($subs->count()){
            foreach ($subs->get() as $sk => $sv) {                
                $this->removeFile($sv->sc_icon_path);
            }
            $subs->delete();
        }

        if ($pros->count()) {            
            foreach ($pros->get() as $pk => $pv) {
                $imgs = $pv->images();
                if($imgs->count()){
                    foreach ($imgs->get() as $ik => $iv) {
                        $this->removeFile($iv->img_thumb_path);
                        $this->removeFile($iv->img_path);
                    }
                    $imgs->delete();
                    
                }
            }

            $pros->delete();
        }

        $this->removeFile($cat->cat_icon_path);
        $this->removeFile($cat->cat_icon_thumb_path);
        if($cat->delete()){
            $data['msg'] =  'delete';
        }

        return $data;

    }
}
