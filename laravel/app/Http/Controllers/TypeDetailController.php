<?php

namespace App\Http\Controllers;

use App\Models\TypeDetail;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;

class TypeDetailController extends Controller
{
    use GetData;

    protected $response = ['msg' => 'error'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $q = $this->changeKeys('td_' , $r->all());
        // return $q;
        
        $t = TypeDetail::create($q);
        
        if ($t) {
            $this->response = ['msg' => 'success', 'd' => $this->removePrefix($t->toArray())];
        }

        return $this->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeDetail  $typeDetail
     * @return \Illuminate\Http\Response
     */
    public function show(TypeDetail $typeDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeDetail  $typeDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeDetail $typeDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeDetail  $typeDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeDetail $typeDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeDetail  $typeDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeDetail $typeDetail)
    {
        //
    }
}
