<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SecondBanner;
use App\Http\Traits\GetData;
use Intervention\Image\ImageManager;
use Illuminate\Database\QueryException;

class SecondBannerController extends Controller
{
    use GetData;
    
    protected $path = 'images/sub-banner/';

    protected $response = ['msg' => 'error'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_second_banners')->with(['banners' => SecondBanner::get()]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_second_banner');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $n = $this->changeKeys('sb_', $r->all());      

        $imgTitle = str_replace(' ','-',trim($n['sb_title'])).(SecondBanner::max('sb_id')+1);

        if ($r->hasFile('banner')) {
            list($big) = $this->uploadFiles($r, $imgTitle, 'banner', [$this->path], [], [300]);
            $n['sb_img'] = $big;
        }

        try{
            SecondBanner::create($n);
            
            $this->response['msg'] = 'success';

        } catch (QueryException $e) {            
            // return $this->response;
            return $e;
        }
        return $this->response; 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SecondBanner  $secondBanner
     * @return \Illuminate\Http\Response
     */
    public function show(SecondBanner $secondBanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SecondBanner  $secondBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(SecondBanner $secondBanner)
    {
        return $this->removePrefix($secondBanner->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SecondBanner  $secondBanner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, SecondBanner $secondBanner)
    {               

        // return $r->all();
        try{
            $n = $this->changeKeys('sb_', $r->all());      

            $imgTitle = str_replace(' ','-',trim($n['sb_title'])).(SecondBanner::max('sb_id')+1);
            
            if ($r->hasFile('img')) {
                list($big) = $this->uploadFiles($r, $imgTitle, 'img', [$this->path]);
                $n['sb_img'] = $big;
            }
            $secondBanner->update($n);
            $this->response['msg'] = 'successU';
            
        } catch (QueryException $e) {
            $this->response['msg'] = 'errorU';
        }
        return $this->response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SecondBanner  $secondBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(SecondBanner $secondBanner)
    {
        $this->removeFile($secondBanner->sb_img);

        $this->response['msg'] = $secondBanner->delete() ? 'delete' : 'error';

        return $this->response;
    }
}
