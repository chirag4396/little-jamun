<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CustomOrderEnquiry;
use App\Models\CustomQuote;
use App\Http\Traits\GetData;
use Intervention\Image\ImageManager;

class CustomEnquiryController extends Controller
{
    use GetData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {            
        $e = CustomOrderEnquiry::where('coe_status',$r->status)->get();
        return view('admin.view_custom_enquiries')->with(['enquiries' => $e]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $n = $this->changeKeys('coe_', $r->all());
        $path = 'images/customized-enquiry/';
        if ($r->hasFile('img_path')) {

            $title = str_replace(' ','-',trim($n['coe_mobile'].rand(9999,4)));
            $exe = $r->file('img_path')->getClientOriginalExtension();
            $filename = $title.'.'.$exe;

            $manager = new ImageManager();
            $photo = $r->file('img_path')->getRealPath();

            $thumbnail = $path.$filename;   

            $manager->make($photo)->save($thumbnail);

            $this->resizeImage($thumbnail);
            $n['coe_img_path'] = $thumbnail;

        }
        
        $data['msg'] = CustomOrderEnquiry::create($n) ? 'successEnquiry' : 'error';
        
        return $data; 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $enq = CustomOrderEnquiry::join('products', 'products.pro_id', '=', 'custom_order_enquiries.coe_pro_id')->join('images', 'images.img_pro_id', '=', 'products.pro_id')->where('images.img_display_priority',1)->find($id);
        return $enq;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function demo()
    {
        return view('admin.email.custom')->with(['cus' => CustomQuote::find(10)]);        
    }

    public function send(Request $r)
    {
//        return $r->all();
        $data['msg'] ='error';
        CustomOrderEnquiry::find($r->coe_id)->update(['coe_status', 1]);
        $n = $this->changeKeys('custom_', $r->all());
        $cq = CustomQuote::create($n);        
        
        if($cq){
            $this->sendEmail($cq->enquiry->coe_email, $cq->enquiry->coe_name, ['cus' => $cq], 'custom');
            $data['msg'] = 'successSend';
        }
        
        return $data;
        
    }
}
