<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Http\Traits\GetData;
use Intervention\Image\ImageManager;

class BannerController extends Controller
{
	use GetData;

	protected $path = 'images/banners/';
    
    protected $pathSub = 'images/banners/sub/';

	public function index()
	{
		return view('admin.view_banners')->with(['banners' => Banner::get()]);
	}

	public function create()
	{
		return view('admin.create_banner');
	}

	public function store(Request $r)
	{
		// return $r->all();
		$path = 'images/banners/';

		$n = $this->changeKeys('ban_', $r->all()); 		

		$imgTitle = str_replace(' ','-',trim($n['ban_title'])).(Banner::max('ban_id')+1);
		
		if ($r->hasFile('img_path')) {
		    list($big) = $this->uploadFiles($r, $imgTitle, 'img_path', [$this->path]);
		    $n['ban_img_path'] = $big;		    
		}
		// if ($r->hasFile('banner2')) {		
		//     list($small) = $this->uploadFiles($r, $imgTitle, 'banner2', [$this->pathSub]);		    
		//     $n['ban_sub_img_path'] = $small;
		// }
		
		// $title = str_replace(' ','-',trim($n['ban_title'])).(Banner::max('ban_id')+1);
		// $exe = $r->file('banner_img')->getClientOriginalExtension();
		// $filename = $title.'.'.$exe;

		// $manager = new ImageManager();
		// $photo = $r->file('banner_img')->getRealPath();

		// $thumbnail = $path.$filename;   

		// $manager->make($photo)->save($thumbnail);

		// $this->resizeImage($thumbnail);
		// $n['ban_img_path'] = $thumbnail;

		// return $n;
		$data['msg'] = (Banner::create($n) ? 'success' : 'error');

		return $data; 
	}

	public function show($id)
	{
        //
	}

	public function edit($id)
	{
		return $this->removePrefix(Banner::find($id)->toArray());        
	}

	public function update(Request $r, $id)
	{
		
		$path = 'images/banners/';
		
		unset($r['_method']);
		$n = $this->changeKeys('ban_', $r->all()); 
		$n['ban_title'] = ucfirst($n['ban_title']);

		unset($n['sc_sid']);
		
		if($r->hasFile('img_path')){

		    $n['ban_img_path'] = $this->uploadFiles($r, $n['ban_title'], 'img_path', [$this->path])[0];
		}


		$data['msg'] = (Banner::find($id)->update($n) ? 'successU' : 'error');

		return $data; 
	}

	public function destroy($id)
	{
		$data['msg'] = 'error';
		$ban = Banner::find($id);

		$this->removeFile($ban->ban_img_path);
		
		if($ban->delete()){            
			$data['msg'] =  'delete';
		}

		return $data;

	}    
}
