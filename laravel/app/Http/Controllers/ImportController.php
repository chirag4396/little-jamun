<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\TypeDetail;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Image;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;
use Excel;
use DB;


class ImportController extends Controller
{
	public function fethTypes($values, $type)
	{
		$d = [];
		if(!empty($values)){
			$vals = explode(',',$values);            
			foreach ($vals as $key => $value) {
				$tds = TypeDetail::where(['td_title' => $value])->first();

				if($tds){                
					$d[] = $tds->td_id;
				}else{
					$t = TypeDetail::create(['td_title' => $value,'td_type' => $type]);
					$d[] = $t->td_id;
				}
			}
		}

		return $d;
	}

	public function fetchCategory($value)
	{
		$d = 0;
		$cat = Category::where(['cat_name' => $value])->first();
		return ($cat) ? $cat->cat_id : Category::create(['cat_name' => $value])->cat_id;
	}

	public function fetchSubCategory($value, $cat)
	{
		$d = 0;
		$sc = SubCategory::where(['sc_name' => $value, 'sc_cat_id' => $cat])->first();
		return ($sc) ? $sc->sc_id : SubCategory::create(['sc_name' => $value, 'sc_cat_id' => $cat])->sc_id;
	}

	public function import()
	{
		return view('admin.import');
	}
	public function uploadFile(Request $r){
		$title = uniqid();
		$exe = $r->file('excel')->getClientOriginalExtension();
		$m = 'excels';
		$filename = $title.'.'.$exe;
		$r->file('excel')->move($m,$filename);
		return ['msg' => 'success', 'file' => $filename];
	}
	public function importData(Request $r)
	{
		// return $r->all();
		$path = 'excels/'.$r->excelFile;

		$data = Excel::load($path, function($reader) {})->get();

		if(!empty($data) && $data->count()){

			foreach ($data as $key => $value) {

				$materials = $this->fethTypes($value->materials,1);
				$colors = $this->fethTypes($value->colors,2);
				$frames = $this->fethTypes($value->frames,3);

				$category = $this->fetchCategory($value->category);
				$subCategory = $this->fetchSubCategory($value->sub_category, $category);                
				$pro = Product::create([
					'pro_title' => $value->title,
					'pro_sub_title' => $value->sub_title,
					'pro_description' => $value->description,
                    'pro_price' => $value->price,
                    'pro_discount' => $value->discount,
					'pro_qty' => $value->qty,
					'pro_cod' => (empty($value->cod) ? 1 : 0), 
					'pro_materials' => implode(',',$materials),
					'pro_colors' => implode(',',$colors),
					'pro_frames' => implode(',',$frames),
					'pro_category' => $category,
					'pro_sub_category' => $subCategory
				]);
				
				if($value->display_picture){                
					Image::where('img_title', (string)$value->display_picture)->update([
						'img_pro_id' => $pro->pro_id,
						'img_display_priority' => 1
					]);
				}

				if($value->images){
					// foreach (explode(',',$value->images) as $kin => $vin) {
					Image::whereIn('img_title', explode(',',$value->images))->update(['img_pro_id' => $pro->pro_id,'img_display_priority' => 0]);

					// }		
				}
			}
			return redirect()->back()->with(['status' => 'done']);            

		} 
	}


	public function images(){

		return Image::all();
	}
}
