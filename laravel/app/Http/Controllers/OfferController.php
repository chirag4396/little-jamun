<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use App\Models\BuyOffer;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Intervention\Image\ImageManager;

class OfferController extends Controller
{
    use GetData;

    protected $response = ['msg' => 'error'];
    
    protected $path = 'images/offers/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_offers')->with(['offers' => Offer::get()]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_offer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $buy = [];
        $newData = $this->changeKeys('offer_', $r->all());

        if ($newData['offer_type'] == 101 ) {
            $newData['offer_cost'] = $newData['offer_discount'];
            $newData['offer_products'] = $newData['offer_product'];            
        }else if ($newData['offer_type'] == 103 ) {            
            $buy = [
                'bo_buy_product' => $r->buy_product,
                'bo_buy_qty' => $r->buy_qty,
                'bo_get_product' => $r->get_product,
                'bo_get_qty' => $r->get_qty,                
            ];            
        }else{
            $newData['offer_products'] = implode(',', $newData['offer_products']);
        }
        unset($newData['offer_product']);       
        unset($newData['offer_discount']);
        // return $newData;
        
        if ($r->hasFile('banner_img')) {
            $title = strtolower(str_replace(' ','-',trim($newData['offer_title'])));
            $exe = $r->file('banner_img')->getClientOriginalExtension();
            $filename = $title.'.'.$exe;

            $manager = new ImageManager();
            $photo = $r->file('banner_img')->getRealPath();

            $thumbnail = $this->path.$filename;   

            $manager->make($photo)->save($thumbnail);

            $this->resizeImage($thumbnail);
            $newData['offer_img_path'] = $thumbnail;
        }
        // return $newData['offer_type'];
        $offer = Offer::create($newData);

        if ($newData['offer_type'] == 103 ){

            $buy['bo_offer'] = $offer->offer_id;            
            // return $buy;
            BuyOffer::create($buy);
        }
        $this->response['msg'] = ($offer ? 'success' : 'error');

        return $this->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        return view('admin.edit_offer')->with(['offer' => $offer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Offer $offer)
    {
        $buy = [];        
        // return $r->all();
        $newData = $this->changeKeys('offer_', $r->all());

        if ($newData['offer_type'] == 101 ) {
            $newData['offer_cost'] = $newData['offer_discount'];
            $newData['offer_products'] = $newData['offer_product']; 
        }else if ($newData['offer_type'] == 103 ) {            
            $buy = [
                'bo_buy_product' => $r->buy_product,
                'bo_buy_qty' => $r->buy_qty,
                'bo_get_product' => $r->get_product,
                'bo_get_qty' => $r->get_qty,                
            ];           
        }else{
            $newData['offer_products'] = implode(',', $newData['offer_products']);
        }
        unset($newData['offer_product']);       
        unset($newData['offer_discount']);
        // return $newData;
        if ($r->hasFile('banner_img')) {
            $this->removeFile($offer->offer_img_path);

            $title = strtolower(str_replace(' ','-',trim($newData['offer_title'])));
            $exe = $r->file('banner_img')->getClientOriginalExtension();
            $filename = $title.'.'.$exe;

            $manager = new ImageManager();
            $photo = $r->file('banner_img')->getRealPath();

            $thumbnail = $this->path.$filename;   

            $manager->make($photo)->save($thumbnail);

            $this->resizeImage($thumbnail);
            $newData['offer_img_path'] = $thumbnail;
        }
        // $offer = Offer::create($newData);

        if ($newData['offer_type'] == 103 ){

            // $buy['bo_offer'] = $offer->offer_id;            
            // return $buy;
            BuyOffer::find($offer->offer_id)->update($buy);
        }
        $this->response['msg'] = ($offer->update($newData) ? 'successU' : 'error');
        return $this->response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        $this->removeFile($offer->offer_img_path);

        $this->response['msg'] = $offer->delete() ? 'delete' : 'error';

        return $this->response;
    }
}
