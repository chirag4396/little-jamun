<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Enquiry;
use App\Models\CustomQuote;
use App\Models\CustomOrderEnquiry;
use App\Http\Traits\GetData;
class EnquiryController extends Controller
{
    use GetData;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        $e = Enquiry::where('enq_type',$r->type)->get();
        return view('admin.view_enquiries')->with(['enquiries' => $e,'type' => $r->type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {        
        // return $r->all();
        $n = $this->changeKeys('enq_', $r->all());
        
        $data['msg'] = Enquiry::create($n) ? 'successEnquiry' : 'error';
        
        return $data; 
    }
    // public function storeCustom(Request $r)
    // {        
    //     // return $r->all();
    //     $n = $this->changeKeys('coe_', $r->all());
        
    //     $data['msg'] = CustomOrderEnquiry::create($n) ? 'success' : 'error';
        
    //     return $data; 
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $r){
        $enq = Enquiry::where('enq_type',$r->type);
        if($r->type == 102){
            $enq = $enq->join('products', 'products.pro_id', '=', 'enquiries.enq_pro_id')->join('images', 'images.img_pro_id', '=', 'products.pro_id')->join('flavors', 'flavors.flavor_id', '=', 'products.pro_flavor')->where('images.img_display_priority',1);
        }
        $enq = $enq->find($id);
        return $enq;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function send(Request $r)
    {        
        // return $r->all();
        $data['msg'] ='error';

        $n = $this->changeKeys('custom_', $r->all());
        $cq = CustomQuote::create($n);
        
        if($cq){
            return $this->sendEmail($r->email,$r->firstname,$orderId, $msg, 'notification');
            $data['msg'] = 'successSend';
        }
        
        return $data;
        
    }
}
