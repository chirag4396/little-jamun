<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\Testimonial;
use App\Http\Traits\GetData;
use Intervention\Image\ImageManager;

class TestimonialController extends Controller
{
    use GetData;
    public function index()
    {
        return view('admin.view_testimonials')->with(['testimonials' => Testimonial::get()]);
    }

    public function create()
    {
        return view('admin.create_testimonial');
    }

    public function store(Request $r)
    {
        $path = 'images/testimonials/';

        if(!Testimonial::where(['test_name' => $r->name,'test_designation' => $r->designation])->first()){
            $n = $this->changeKeys('test_', $r->all()); 
            $n['test_name'] = ucfirst($n['test_name']);
            
            $title = str_replace(' ','-',trim($n['test_name']));
            $exe = $r->file('picture')->getClientOriginalExtension();
            $filename = $title.'.'.$exe;
            
            $manager = new ImageManager();
            $photo = $r->file('picture')->getRealPath();

            $thumbnail = $path.$filename;            

            $manager->make($photo)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbnail);
            
            $this->resizeImage($thumbnail);
            $n['test_picture'] = $thumbnail;

            $data['msg'] = (Testimonial::create($n) ? 'success' : 'error');
        }else{
            $data['msg'] = 'exist';
        }   
        return $data; 
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return $this->removePrefix(Testimonial::find($id)->toArray());        
    }
    
    public function update(Request $r, $id)
    {

        $data = [];

        unset($r['_method']);
        $n = $this->changeKeys('test_', $r->all());
        unset($n['test_sid']);
        // return $id;
        $test = Testimonial::find($r->sid);
        
        // if($test->test_name != $n['test_name']){

        $path = 'images/testimonials/';
        $n['test_name'] = ucfirst($n['test_name']);

        if ($r->hasFile('picture')) {
            if(File::exists($test->test_picture)){
                unlink($test->test_picture);
            }

            $title = str_replace(' ','-',trim($n['test_name']));
            $exe = $r->file('picture')->getClientOriginalExtension();
            $filename = $title.'.'.$exe;

            $manager = new ImageManager();
            $photo = $r->file('picture')->getRealPath();

            $thumbnail = $path.$filename;            

            $manager->make($photo)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbnail);

            $this->resizeImage($thumbnail);
            $n['test_picture'] = $thumbnail;
        }
            // return $n;
        $c = Testimonial::where('test_id', $r->sid)->update($n);        

            // if($c){
        $data['msg'] = 'successU';
        $data['data'] = $test;

            // }else{                
            //     $data['msg'] = 'error';

            // }
        // }else{
        //     $data['msg'] = 'same';
        // }
        return $data;
    }

    public function destroy($id)
    {
        $data['msg'] = 'error';
        
        $test = Testimonial::find($id);
        
        $this->removeFile($test->test_picture);        
        
        if($test->delete()){            
            $data['msg'] =  'delete';
        }                

        return $data;

    }

}
