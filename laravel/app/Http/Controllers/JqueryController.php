<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logic\UploadHandler;
use Illuminate\Support\Facades\Input;

class JqueryController extends Controller
{
	
	// function __construct(UploadHandler $u)
	// {
	// 	$this->upload = $u;
	// }

	public function index()
	{
		return view('admin.file-upload');
	}

	public function upload(Request $r)
	{
		return $r->all();

	}

	public function store()
	{
		$file = Input::file('file');
		return Repsonse::json($file);
		// $upload = new Upload;


		try {
			$upload->process($file);
		} catch(Exception $exception){
          // Something went wrong. Log it.
			Log::error($exception);
			$error = array(
				'name' => $file->getClientOriginalName(),
				'size' => $file->getSize(),
				'error' => $exception->getMessage(),
			);
          // Return error
			return Response::json($error, 400);
		}

      // If it now has an id, it should have been successful.
		if ( $upload->id ) {
			$newurl = URL::asset($upload->publicpath().$upload->filename);

          // this creates the response structure for jquery file upload
			$success = new stdClass();
			$success->name = $upload->filename;
			$success->size = $upload->size;
			$success->url = $newurl;
			$success->thumbnailUrl = $newurl;
			$success->deleteUrl = action('UploadController@delete', $upload->id);
			$success->deleteType = 'DELETE';
			$success->fileID = $upload->id;

			return Response::json(array( 'files'=> array($success)), 200);
		} else {
			return Response::json('Error', 400);
		}
	}
	public function edit($id)
	{
	    $institution = Institution::find($id);
	    $fileids = json_decode($institution->logo);

	    $files = array();

	    foreach ($fileids as $fileid) {
	        $upload = Upload::find($fileid);
	        if (isset($upload)){
	            $newurl = URL::asset($upload->publicpath().$upload->filename);

	            $success = new stdClass();
	            $success->name = $upload->filename;
	            $success->size = $upload->size;
	            $success->url = $newurl;
	            $success->thumbnailUrl = $newurl;
	            $success->deleteUrl = action('UploadController@delete', $upload->id);
	            $success->deleteType = 'DELETE';
	            $success->fileID = $upload->id;
	            $files[] = $success;
	        }
	    }

	    $json = json_encode(array('files'=> $files));

	    return View::make('institutions.edit', compact('institution','json'));
	}
	public function delete($id)
	{
		$upload = Upload::find($id);
		$upload->delete();

		$success = new stdClass();
		$success->{$upload->filename} = true;

		return Response::json(array('files'=> array($success)), 200);
	}

	public function update($id)
	{
		$institution = Institution::findOrFail($id);

    //get form data
		$data = Input::only('title', 'body', 'topcolor', 'topfontcolor','currentdi','extracomments');
		$data['logo'] = json_encode(Input::get('fileid'));

    // validation rules
		$rules = array(
			'title'      => 'required',
        //'body'     => '',
        //'topfontcolor'  =>'required|between:4,8',
        //'topcolor'  => 'required|between:4,8',
			'logo' => 'required',
        //'currentdi' => '',
        //'extracomments' => '',
		);
		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$institution->update($data);

		return Redirect::to_action('InstitutionsController@show', array($id));
	}
}