<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use App\Models\Product;
use App\Models\Image;
use App\Models\Type;
use App\Models\TypeDetail;
use App\Models\Category;
use App\Models\SubCategory;
// use App\Models\Material;
// use App\Models\Frame;
// use App\Models\Color;
use App\Models\Offer;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;
use Excel;
use DB;

class ProductController extends Controller
{
    use GetData;
    public function allProducts($cat, $sub = null)
    {
        // return $sub;
        $pro = Product::where(function($pro) use ($sub)
        {            
            if($sub != null){
                $pro->where('pro_sub_category',$sub);
            }

        })->where('pro_category',$cat)->get();

        return view('user-panel.products')->with(['products' => $pro,'catId' => $cat,'subId' => $sub]);
    }
    

    public function index()
    {
        return view('admin.view_products')->with(['products' => Product::get()]);        
    }
    
    public function create()
    {
        return view('admin.create_product')->with(['categories' => Category::get()]);
    }
    
    public function store(Request $r)
    {        
        // return $r->all();
        $data = ['msg' => 'success'];

        $path = Config::get('app.images');
        $thumbPath = Config::get('app.thumbnails');

        // $n['pro_sugar'] = 'off';
        
        $n = $this->changeKeys('pro_', $r->all());
        
        // $n['pro_sugar'] = ($r->sugar == 'on') ? 1 : 0;
        // return $n;
        foreach ($n as $k => $v) {
            if(is_array($v)){                
                $n[$k] = implode(',',$v);
            }
        }
        unset($n['pro_display_picture']);
        
        (isset($n['pro_sub_category'])) ? ($n['pro_sub_category'] = ($n['pro_sub_category'] == '-1') ? 0 : $n['pro_sub_category']) : '';

        foreach (Type::get() as $key => $value) {
            $t = strtolower($value->type_title);
            $n['pro_'.$t] = $r[$t] ? implode(',',$r[$t]) : null;
        }
        
        // return $n;
        try {            

            $pro = Product::create($n);

            if($pro){
                if ($r->hasFile('display_picture')) {  
                    $preImg = Image::where('img_pro_id', $pro->pro_id)->orderBy('img_pro_id', 'desc')->get()->count();
                    $title = str_replace(' ','-',trim($n['pro_title'])).'-'.(++$preImg);
                    $exe = $r->file('display_picture')->getClientOriginalExtension();
                    $filename = $title.'.'.$exe;
                    $thumbFilename = $title.'-thumbnail.'.$exe;
                    $manager = new ImageManager();
                    $photo = $r->file('display_picture')->getRealPath();

                    $thumbnail = $thumbPath.$thumbFilename;
                    $imgPath = $path.$filename;

                    $manager->make($photo)->resize(200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($thumbnail);

                    $r->file('display_picture')->move($path, $filename);           

                    $this->resizeImage($imgPath);
                    $this->resizeImage($thumbnail);

                    Image::create([
                        'img_title' => $title,
                        'img_path' => $imgPath,
                        'img_thumb_path' => $thumbnail,
                        'img_display_priority' => 1,
                        'img_pro_id' => $pro->pro_id
                    ]);

                }
                $data['pid'] = $pro->pro_id;
            }else{
                $data['msg'] = 'error';
            }
        } catch (QueryException $e) {
            $data['msg'] = 'error';        
        }
        return  $data;            

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        return view('user-panel.single-product')->with(['product' => Product::find($id),'id' => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $r)
    {        
        if($r->ajax()){
            return $this->removePrefix(Product::find($id)->toArray());
        }         

        return view('admin.edit_product')->with(['pro'=>Product::find($id),'categories' => Category::get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {

        
        if($r->new_quantity){
            Product::find($id)->update(['pro_qty' => $r->new_quantity]);
            return ['msg' => 'successU'];
        }
        $data = ['msg' => 'successU'];
        $product = Product::find($id);
        $path = Config::get('app.images');
        $thumbPath = Config::get('app.thumbnails');

        // $n['pro_sugar'] = 'off';
        
        $n = $this->changeKeys('pro_', $r->all());
        
        // $n['pro_sugar'] = ($r->sugar == 'on') ? 1 : 0;
        // return $n;
        foreach ($n as $k => $v) {
            if(is_array($v)){                
                $n[$k] = implode(',',$v);
            }
        }
        unset($n['pro_display_picture']);
        
        (isset($n['pro_sub_category'])) ? ($n['pro_sub_category'] = ($n['pro_sub_category'] == '-1') ? 0 : $n['pro_sub_category']) : '';

        foreach (Type::get() as $key => $value) {
            $t = strtolower($value->type_title);
            $n['pro_'.$t] = $r[$t] ? implode(',',$r[$t]) : null;
        }
        
        try {            

            $product->update($n);        

            if ($r->hasFile('display_picture')) {  
                $img = Image::where('img_pro_id',$id)->first();

                $preImg = Image::where('img_pro_id', $id)->orderBy('img_pro_id', 'desc')->get()->count();


                $title = str_replace(' ','-',trim($n['pro_title'])).'-'.(++$preImg);
                $exe = $r->file('display_picture')->getClientOriginalExtension();
                $filename = $title.'.'.$exe;
                $thumbFilename = $title.'-thumbnail.'.$exe;
                $manager = new ImageManager();
                $photo = $r->file('display_picture')->getRealPath();

                $thumbnail = $thumbPath.$thumbFilename;
                $imgPath = $path.$filename;

                $manager->make($photo)->resize(200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbnail);

                $r->file('display_picture')->move($path, $filename);           

                // $this->resizeImage($imgPath);
                // $this->resizeImage($thumbnail);

                // Image::where('img_pro_id', $product->pro_id)->update([
                //     'img_title' => $title,
                //     'img_path' => $imgPath,
                //     'img_thumb_path' => $thumbnail
                // ]);

                if($img){                

                    $this->removeFile($img->img_path);
                    $this->removeFile($img->img_thumb_path);


                    Image::where('img_pro_id',$id)->update([
                        'img_title' => $title,
                        'img_path' => $imgPath,
                        'img_thumb_path' => $thumbnail
                    ]);
                }else{
                    Image::create([
                        'img_title' => $title,
                        'img_path' => $imgPath,
                        'img_thumb_path' => $thumbnail,
                        'img_display_priority' => 1,
                        'img_pro_id' => $id
                    ]);
                }

            }
            $data['pid'] = $product->pro_id;
            
        } catch (QueryException $e) {
            $data['msg'] = 'errorU';        
        }
        return  $data;         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pro = Product::find($id);
        $imgs = Image::where('img_pro_id', $pro->pro_id)->get();

        foreach ($imgs as $img){
            $this->removeFile($img->img_path);
            $this->removeFile($img->img_thumb_path);
            $img->delete();
        }

        $data['msg'] = $pro->delete() ? 'delete' : 'error';
        return $data;
    }

    public function search(Request $r){
        return view('user-panel.search')->with(['search' => $r->search]);
    }

    public function getProduct(Request $r){
        return $this->removePrefix(Product::find($r->id)->toArray());
    }

    public function offers(){
        $now = Carbon::now('Asia/Kolkata');
        $offers = Offer::where('offer_end', '>=', $now)->where('offer_start', '<=', $now)->get();
        return view('user-panel.offer')->with(['offers' => $offers]);
    }

    public function images($id){
        // return $id;
        return Image::where('img_pro_id',$id)->where('img_display_priority',0)->get();
    }
    
    public function stock()
    {    
        $products = Product::get();
        return view('admin.view_stock')->with(['products' => $products]);
    }    
}
