<?php
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'shop','as' => 'shop.'],function(){
		
	Route::get('/sub-categories/{id}', function ($id) {	
		return view('user-panel.sub-categories')->with(['catId' => $id]);
	})->name('our-menu');

	// Route::get('/sub-categories/{id}/{catId}', function ($id, $catId) {
	// 	return view('user-panel.sub-categories')->with(['subId' => $id, 'catId' => $catId]);
	// })->name('sub-menu');
	
	// Route::get('/child-categories/{id}/{subId}/{catId}', function ($id, $subId, $catId) {
	// 	return view('user-panel.child-categories')->with(['childId' => $id, 'subId' => $subId, 'catId' => $catId]);
	// })->name('child-menu');
	
	Route::get('/products/{cat}/{sub?}', 'ProductController@allProducts')->name('products');

	Route::get('/product/{id}','ProductController@show')->name('product');
	
	Route::get('/faq','FaqController@all')->name('faq');

	Route::get('/cart', function () {
		return view('user-panel.cart');
	})->name('cart');

	Route::get('/contact-us', function () {
		return view('user-panel.contact-us');
	})->name('contact');
	// Route::post('/contact', 'HomeController@contact')->name('add.contact');
	Route::get('/detail/{type}', 'HomeController@webDetails')->name('web-detail');

	Route::get('/our-story', function () {
		return view('user-panel.about');
	})->name('about');

	// Route::get('/terms', function () {
	// 	return view('user-panel.terms');
	// });

	Route::get('/search', 'ProductController@search')->name('search');
	
	Route::get('/offers', 'ProductController@offers')->name('offers');

	Route::post('/checkout','OrderController@checkout')->name('checkout');

	Route::post('/placeorder','OrderController@getPay')->name('placeorder');	

	Route::post('/paystatus','OrderController@orderStatus')->name('orderStatus');

	Route::resource('enquiry', 'EnquiryController');
	


	Route::post('fin-pin','PinCodeController@finPincode')->name('find-pin');

	Route::get('custom/{id}','OrderController@custom')->name('custom');
	
	Route::post('custom-enquiry','CustomEnquiryController@store')->name('store.custom');

	Route::get('/booked/order/{id}/{status?}', 'OrderController@previous')->name('previous');

});

Route::get('/admin/login', function() {
	return view('admin.login');
});

Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'as' => 'admin.'], function(){
	
	Route::get('/', function() {
		return view('admin.dashboard');
	})->name('home');
	
	Route::get('/form', function() {
		return view('admin.form');
	});
	Route::get('/table', function() {
		return view('admin.table');
	});

	Route::get('fetch-images/{id}','ProductController@images')->name('images');
	Route::resource('product', 'ProductController');
	Route::get('/detail', 'HomeController@view')->name('web-detail.index');
	Route::post('/detail', 'HomeController@update')->name('web-detail');
	
	// Route::resource('flavor', 'FlavorController');
	Route::resource('faq', 'FaqController');
	Route::resource('category', 'CategoryController');
	Route::resource('sub-category', 'SubCategoryController');
	Route::resource('material', 'MaterialController');
	Route::resource('type-detail', 'TypeDetailController');
	Route::resource('color', 'ColorController');
	// Route::resource('child-category', 'ChildCategoryController');
	// Route::resource('super-child-category', 'SuperChildCategoryController');
	Route::resource('pincode','PinCodeController');	
	// Route::resource('testimonial', 'TestimonialController');
	// Route::resource('client', 'ClientController');
	Route::resource('banner', 'BannerController');
	Route::resource('second-banner', 'SecondBannerController');
	// Route::resource('offer', 'OfferController');
	// Route::resource('occasion', 'OccasionController');
	Route::resource('order', 'OrderController');
	Route::resource('enquiry', 'EnquiryController');
	// Route::resource('custom-enquiry', 'CustomEnquiryController');
	Route::post('send-enquiry', 'EnquiryController@send')->name('enquiry.send');
	// Route::post('send-custom-enquiry', 'CustomEnquiryController@send')->name('custom-enquiry.send');
	
	// Route::get('demo', 'CustomEnquiryController@demo');
	// Route::post('get-product', 'ProductController@getProduct')->name('get-product');
	
	Route::get('sub-categories/{id?}', 'SubCategoryController@getSubCategories')->name('getSubCategories');
	Route::post('fetch-types', 'TypeController@fetchTypes')->name('fetch-types');	
	Route::get('invoice/{id}', 'OrderController@invoice')->name('order-invoice');
	
	Route::get('fetch-all-images','ImportController@images')->name('all-images');	
	Route::get('import/', 'ImportController@import')->name('import-product');
	Route::post('upload-excel/', 'ImportController@uploadFile')->name('upload-excel');
	Route::post('import/', 'ImportController@importData')->name('import');
	Route::get('stock', 'ProductController@stock')->name('stock');
	
});



Route::get('/drop', ['as' => 'upload', 'uses' => 'ImageController@getUpload']);

Route::get('/drop2', function(){
	return view('admin.drop2');
});

Route::get('file-upload', 'JqueryController@index');
Route::post('file-upload', 'JqueryController@store')->name('file-upload');

Route::post('upload', ['as' => 'upload-post', 'uses' =>'ImageController@postUpload']);
Route::post('upload/delete', ['as' => 'upload-remove', 'uses' =>'ImageController@deleteUpload']);
